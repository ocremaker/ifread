#!/bin/sh
#
# Interactive Fictions for Restricted Environments All-in-one Designer
# Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# Create assets dir
rm -rf public/assets && mkdir -p public/assets

cp -r assets/static/* public/
cp -r assets/fonts/ public/assets/
cp -r assets/images/ public/assets/
./node_modules/.bin/svgo -f ./assets/icons -o ./public/assets/icons
