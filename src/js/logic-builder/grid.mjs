/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Layer, Line } from "konva"
import { Zone, ZonePerBuilderDelegate } from "./zones/"

export class Grid extends Layer {
    constructor({ stage, offset = 10, ...options }) {
        super({
            draggable: false,
            name: "Grid",
            ...options
        })
        this.stage = stage
        // Grid info
        this.offset = offset
        this.snapOffsetMult = 5
        this.snap = offset * this.snapOffsetMult
        this.stage.on("dragend", (e) => {
            if(e.target === this.stage)
                this.redrawGrid()
        })
        this.redrawGrid()
    }

    /**
     * Rounds a position to its closest square's top-left on the grid.
     * @param {{x: number, y: number}} position
     * @returns {{x: number, y: number}}
     */
    roundToNearestSquare(position) {
        return {
            x: Math.floor(position.x / this.snap) * this.snap,
            y: Math.floor(position.y / this.snap) * this.snap
        }
    }

    /**
     * Generates a list of squares between two positions (excluding both)
     * if one were to pass through a direct line.
     * @param {{x: number, y: number}} pos1
     * @param {{x: number, y: number}} pos2
     * @returns {Generator<{x: number, y: number}, void, *>}
     */
    *squaresBetween(pos1, pos2) {
        let sq1 = this.roundToNearestSquare(pos1)
        let sq2 = this.roundToNearestSquare(pos2)
        let distance = Math.sqrt(Math.pow(pos1.x - pos2.x, 2) + Math.pow(pos1.y - pos2.y, 2))
        if(distance > 50) {
            let vect = { x: (pos2.x - pos1.x) / distance, y: (pos2.y - pos1.y) / distance }
            let last = sq1
            for(let i = 50; i < distance; i += 10) {
                let pos = {
                    x: vect.x * i + pos1.x,
                    y: vect.y * i + pos1.y
                }
                let sqb = this.roundToNearestSquare(pos)
                if((sqb.x !== last.x || sqb.y !== last.y) && (sqb.x !== sq2.x || sqb.y !== sq2.y)) {
                    yield pos
                    last = sqb
                }
            }
        }
    }

    redrawGrid() {
        this.clear()
        let zones = this.getChildren().filter(c => c instanceof ZonePerBuilderDelegate)
        this.destroyChildren()
        let ww = window.innerWidth / this.stage.scaleX()
        let wh = window.innerHeight / this.stage.scaleY()
        let width = this.stage.width() / this.stage.scaleX()
        let height = this.stage.height() / this.stage.scaleY()
        let startX = -ww - this.stage.x() / this.stage.scaleX()
        let startY = -wh - this.stage.y() / this.stage.scaleY()
        let endX = width + startX + 2 * ww
        let endY = height + startY + 2 * wh
        let startGridX = startX - (startX % this.offset)
        let startGridY = startY - (startY % this.offset)
        // Adding lines
        for(let x = startGridX; x < endX; x += this.offset)
            this.add(new Line({
                x,
                y: startY,
                points: [0, 0, 0, endY - startY],
                stroke: (x / this.offset % this.snapOffsetMult === 0) ? "#00000055" : "#00000011",
                listening: false,
                strokeWidth: 1
            }))
        for(let y = startGridY; y < endY; y += this.offset)
            this.add(new Line({
                x: startX,
                y,
                points: [0, 0, endX - startX, 0],
                stroke: (y / this.offset % this.snapOffsetMult === 0) ? "#00000055" : "#00000011",
                listening: false,
                strokeWidth: 1
            }))
        this.batchDraw()
    }
}
