/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Stage, Layer, Node } from "konva"
import { containsFlag } from "#utils"
import { Grid } from "./grid"
import { Socket } from "./shapes/sockets"
import Tools from "./tools/"

const ZOOM_FACTOR = 1.1

/**
 * Socket renderer for the logic graphs.
 */
export class LogicBuilder extends Stage {
    constructor({ uuid, ...options }) {
        super({
            draggable: true,
            ...options
        })
        this._currentCursor = "grab"
        this._cursorPriorities = []
        this._ctxMenu = null
        this.currentFocusedElement = null
        this.currentTool = Tools.move
        this.uuid = uuid
        this.defaultZone = null

        // Adding layers
        this.grid = new Grid({
            stage: this,
            offset: 20
        })
        this.mainLayer = new Layer({ draggable: false })
        this.zonesLayer = new Layer({ draggable: false })

        // Adding zone delegate
        /** @type {Zone} */
        let zone
        for(zone of Object.values(window.globals.zones))
            this.addZoneDelegate(zone.createDelegate(this))

        this.add(this.zonesLayer)
        this.add(this.grid)
        this.add(this.mainLayer)

        this.on("dragstart", (e) => {
            let isSocket = e.target instanceof Socket && containsFlag(e.target.ioType, Socket.IO.SOURCE)
            this.setCursorWithPriority(isSocket ? "crosshair" : "grabbing", 50)
        })
        this.on("dragend", (e) => this.removeCursorWithPriority(50))
        this.on("contextmenu", this.createContextMenu)
        this.on("toolChanged", (e) => this.selectTool(e.tool))
        this.on("wheel", this.zoom)
        // Send focus events
        this.on("pointerclick", (e) => {
            if(e.target !== this.currentFocusedElement && this.currentTool === Tools.move) {
                if(e.target === this) {
                    this.focus(this.defaultZone?.delegates.get(this.uuid))
                } else {
                    this.focus(e.target)
                }
            }
        })
    }

    _updateCursor() {
        let newCursor = Object.values(this._cursorPriorities).at(-1) ?? this.currentTool.defaultCursor
        if(newCursor !== this._currentCursor) {
            this.container().classList.remove(`cursor-${this._currentCursor}`)
            this.container().classList.add(`cursor-${newCursor}`)
            this._currentCursor = newCursor
        }
    }

    /**
     * Selects a new tool.
     * @param newTool
     */
    selectTool(newTool) {
        this.currentTool.onUnselected(this)
        this.currentTool = Tools[newTool]
        this._updateCursor()
        this.currentTool.onSelected(this)
    }

    /**
     * Focuses a new element.
     * @param {Node|null} target
     */
    focus(target) {
        if(this.currentFocusedElement !== null)
            this.currentFocusedElement.fire("unfocus")
        if(target != null) {
            target = target.findAncestor(".FocusCaptor", true)
            target?.fire("focus")
            target?.moveToTop()
            this.currentFocusedElement = target ?? null
        }
    }

    /**
     * Adds the current cursor to the stack
     * @param {string} cursorName
     * @param {number} priority - The cursor with the greatest priority is the one who gets to be displayed.
     */
    setCursorWithPriority(cursorName, priority) {
        this._cursorPriorities[priority] = cursorName
        this._updateCursor()
    }

    /**
     * Removes the cursor from the stack with a given priority
     */
    removeCursorWithPriority(priority) {
        delete this._cursorPriorities[priority]
        this._updateCursor()
    }

    /**
     * Removes all currently assigned cursor priorities.
     */
    resetCursors() {
        this._cursorPriorities = []
        this._updateCursor()
    }

    /**
     * Called when the stage is right-clicked.
     * @param e
     */
    createContextMenu(e) {
        e.evt.preventDefault()
        if(this._ctxMenu !== null) {
            this._ctxMenu.remove()
            this._ctxMenu = null
        }
        if(e.target !== null && e.target !== this) {
            /** @type {BlockContextMenu} */
            let target = e.target.findAncestor(".FocusCaptor", true)
            if(target !== null) {
                this._ctxMenu = document.createElement("block-context-menu")
                this._ctxMenu.associatedFocusCaptor = target
                let rect = this.container().getBoundingClientRect()
                this._ctxMenu.style.top = rect.top + this.getPointerPosition().y + 4 + "px"
                this._ctxMenu.style.left = rect.left + this.getPointerPosition().x + 4 + "px"
                document.body.appendChild(this._ctxMenu)
            }
        }
    }

    /**
     * Returns the current pointer position, offset by the x and y value of the stage.
     * @returns {{x: number, y: number}}
     */
    getOffsetPointerPosition() {
        const scale = this.scaleX()
        const pos = this.getPointerPosition()
        return {
            x: (pos.x - this.x()) / scale,
            y: (pos.y - this.y()) / scale
        }
    }

    /**
     * Adapted from https://konvajs.org/docs/sandbox/Zooming_Relative_To_Pointer.html
     * @param e
     */
    zoom(e) {
        e.evt.preventDefault() // Prevent scrolling.
        const absMousePosition = this.getPointerPosition()
        const offsetMousePosition = this.getOffsetPointerPosition()
        const direction = e.evt.deltaY > 0 ? 1 : -1

        const zoomLevel = this.scaleX()
        const newZoomLevel = Math.clamp(
            0.25,
            2,
            direction > 0 ? zoomLevel / ZOOM_FACTOR : zoomLevel * ZOOM_FACTOR
        )

        this.scale({ x: newZoomLevel, y: newZoomLevel })
        this.container().style.backgroundSize = (newZoomLevel * this.grid.snap) + "px"

        this.x(absMousePosition.x - offsetMousePosition.x * newZoomLevel)
        this.y(absMousePosition.y - offsetMousePosition.y * newZoomLevel)
        this.grid.redrawGrid()
    }

    /**
     * Returns a list of all focus captors (Blocks, Notes, and Links) on the canvas.
     * @returns {(Group|FocusCaptorDelegate)[]}
     */
    listFocusCaptors() {
        return this.find(".FocusCaptor").sort((a, b) => a._id - b._id)
    }

    /**
     * Returns all blocks present on the canvas.
     * @returns {BaseBlock[]}
     */
    listBlocks() {
        return this.mainLayer.find(".Block")
    }

    /**
     * Returns all notes present on the canvas.
     * @returns {Note[]}
     */
    listNotes() {
        return this.mainLayer.find(".Note")
    }

    /**
     * Returns all links present on the canvas.
     * @returns {Link[]}
     */
    listLinks() {
        return this.mainLayer.find(".Link")
    }

    /**
     *
     * @param {ZonePerBuilderDelegate} zoneDelegate
     */
    addZoneDelegate(zoneDelegate) {
        this.zonesLayer.add(zoneDelegate)
    }

    /**
     * Adds a link between two sockets on the canvas
     * @param {Link} link
     */
    addLink(link) {
        this.mainLayer.add(link)
        link.fromSocket.addLink(link)
        link.toSocket.addLink(link)
        this.fire("linkAdded", { link })
    }

    /**
     * Removes a link from the canvas.
     * @param {Link} link
     */
    removeLink(link) {
        link.remove()
        link.fromSocket.removeLink(link)
        link.toSocket.removeLink(link)
        this.fire("linkRemoved", { link })
        link.destroy()
    }

    /**
     * Adds a block to the canvas.
     * @param {BaseBlock|Note} block
     * @returns {BaseBlock|Note}
     */
    addBlock(block) {
        this.mainLayer.add(block)
        if(block.updateZone)
            block.updateZone()
        this.fire("blockAdded", { block })
        return block
    }

    /**
     * Adds a new block to the canvas.
     * @param {typeof BaseBlock} blockType
     * @param {{x: number, y: number}} pos
     * @returns {BaseBlock}
     */
    addNewBlock(blockType, pos) {
        let block = new blockType({ x: 0, y: 0 })
        block.x(pos.x - block.width() / 2)
        block.y(pos.y - block.height() / 2)
        return this.addBlock(block)
    }

    /**
     * Removes a block from the canvas
     * @param {Group} block
     */
    removeBlock(block) {
        block.remove()
        this.fire("blockRemoved", { block })
        block.destroy()
    }

    /**
     * Called when a zone has been painted on a rectangle.
     * @param {Zone} newZone
     * @param {number} x1
     * @param {number} y1
     * @param {number} x2
     * @param {number} y2
     */
    zoneUpdated(newZone, { x1, y1, x2, y2 }) {
        this.mainLayer.getChildren(b => {
            if(b.hasName("Block")) {
                const x = b.x() + b.width() / 2
                const y = b.y() + b.height() / 2
                if(x >= x1 && x <= x2 && y >= y1 && y <= y2) {
                    // Block is within square
                    b.fire("zoneChanged", { newZone })
                }
            }
            return false
        })
    }


    updateDefaultZoneStyle(zone) {
        const color = zone.blockData.color
        const container = this.container()
        container.style.backgroundColor = color
    }


    /**
     * Sets this builder's default zone.
     * @param {Zone} zone
     */
    setDefaultZone(zone) {
        console.log("Setting default zone", this.uuid, zone?.blockData.title)
        if(zone) {
            zone.on(`colorChanged.builder-${this.uuid}`, (e) => {
                this.updateDefaultZoneStyle(zone)
            })
            this.updateDefaultZoneStyle(zone)
        }
        this.defaultZone?.off(`colorChanged.builder-${this.uuid}`)
        this.defaultZone = zone
        this.mainLayer.getChildren(b => {
            if(b.hasName("Block") && b.currentZoneSquare(this) === undefined) {
                b.fire("zoneChanged", { newZone: zone })
            }
            return false
        })
    }
}