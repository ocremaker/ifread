/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Delta from "quill-delta"
import { BaseBlock } from "./base"
import { Socket, Button, SocketModel } from "#logic-builder/shapes"
import { openTextEditor } from "#ui"
import { Anchor } from "#logic-builder/utils"
import { Types } from "#abstract"
import { TextBlockData } from "#abstract/structures"
import { generateUUID } from "../../utils/index.mjs"


export class TextBlock extends BaseBlock {
    static displayName = "Text"

    /**
     * Object containing all the block's properties.
     * @type {TextBlockData}
     */
    accessor blockData


    static socketModels = [
        new SocketModel({
            ref: "textIn",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.LEFT,
            linkType: Types.Link.TEXT
        }),
        new SocketModel({
            ref: "textOut",
            ioType: Socket.IO.SOURCE,
            anchor: Anchor.RIGHT,
            linkType: Types.Link.TEXT
        })
    ]

    constructor({ blockData, ...options }) {
        let colors = {
            main: "#9DB6F6",
            background: "#DAE8FC"
        }
        super({
            width: 140,
            height: 100,
            type: "Text",
            removable: true,
            colors,
            blockData: blockData ?? new TextBlockData({}),
            ...options
        })
        // Create text contents if it doesn't exist
        if(!Array.isArray(window.globals.texts[this.blockData.contentsUUID]))
            window.globals.texts[this.blockData.contentsUUID] = []

        this.container.add(...[
            this.editTextButton = new Button({
                text: "Edit text",
                background: colors.background,
                width: 180,
                height: 30
            })
        ])

        this.editTextButton.on("pointerclick", () => {
            console.log("Opening text dialog")
            openTextEditor(`Edit text of ${this.titleNode.edit.text()}`, this.blockData.contentsUUID).then(() => {
                console.log("Saving", window.globals.texts[this.blockData.contentsUUID])
            })
        })
    }

    destroy() {
        super.destroy()
        delete window.globals.texts[this.blockData.contentsUUID]
    }

    /**
     * Duplicates the text.
     * @returns {TextBlock}
     */
    duplicate() {
        /** @type {TextBlock} */
        const duplicata = super.duplicate()
        window.globals.texts[duplicata.blockData.contentsUUID] = JSON.parse(JSON.stringify(window.globals.texts[this.blockData.contentsUUID]))
        return duplicata
    }
}