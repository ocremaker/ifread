/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Box, Socket } from "#logic-builder/shapes"
import { confirm } from "#ui"
import { containsFlag, generateUUID, MenuItem } from "#utils"
import { Types } from "#abstract"
import { BaseBlockData } from "#abstract/structures"
import { Anchor, attemptRectSnap } from "#logic-builder/utils"

export class BaseBlock extends Box {
    static displayName = "Block"

    /**
     * List of SocketModels that will be declared when the object is created.
     * Available statically to allow "Create" menus to access them.
     * @type {SocketModel[]}
     */
    static socketModels = []

    /**
     * Object containing all the block's properties.
     * @type {BaseBlockData}
     */
    blockData

    constructor({
        uuid,
        type,
        x, y,
        width, height,
        colors = { main: "#000000", background: "#AAAAAA" },
        justTitle = false,
        removable = false,
        blockData,
        ...options
    }) {
        super({
            name: `Block ${type}`,
            // Center block
            x: x - width / 2,
            y: y - height / 2,
            width, height,
            titleBackground: justTitle ? "transparent" : colors.background,
            titleHeight: justTitle ? height : 40,
            background: justTitle ? colors.background : "#FFFFFF",
            border: colors.main,
            ...options
        })
        this.uuid = uuid ?? generateUUID()
        this.type = type
        this.removable = removable
        this.savedZone = null

        // Setting block data
        if(!(blockData instanceof BaseBlockData))
            throw new Error("Cannot create block without block data.")
        this.blockData = blockData
        this.setTitle(blockData.title?.length > 0 ? blockData.title : this.constructor.displayName)

        // Sockets
        let sockets = this.constructor.socketModels.map(opts => new Socket(opts))
        if(sockets.length > 0)
            this.add(...sockets)
        this.sockets = sockets

        // Update socket positions
        Anchor.updatePositions(this.getChildren())
        this.on("widthChange", () => Anchor.updatePositions(this.getChildren()))
        this.on("heightChange", () => Anchor.updatePositions(this.getChildren()))
        this.on("dragstart", () => this.saveZone())
        this.on("dragmove", () => this.attemptSnap(10))
        this.on("dragend", () => this.updateZone())

        // Make sure title gets updated when edited directly on canvas.
        this.titleNode.edit.on("preTextEdit", (e) => {
            this.setTitle(e.text)
        })
        this.titleNode.edit.on("textEdited", (e) => {
            this.blockData.title = e.text
            this.fire("titleChanged", { newValue: e.text })
        })

        // Update properties
        this.on("titleChanged", (e) => { this.setTitle(e.newValue) })
    }

    /**
     * Sets the title of the block.
     * @param {string} val
     */
    setTitle(val = undefined) {
        // Setting the value with the display name as subtitle.
        let display = this.constructor.displayName
        val = val.split("\n")[0]
        if(val !== display && val !== "")
            this.titleNode.edit.text(`${val}\n(${display})`)
        else
            this.titleNode.edit.text(display)
    }

    remove() {
        this.off()
        for(let socket of this.find(".Socket"))
            socket.removeLinks()
        super.remove()
    }

    /**
     * Shows a prompt preparing for the deletion of the block.
     */
    prepareDeletion() {
        let msg = "This action will completely remove this block from the logic diagram. THIS OPERATION IS NOT REVERSIBLE! Continue?"
        confirm("delete-block", "Delete Block?", msg).then(() => {
            this.getStage().removeBlock(this)
            this.destroy()
        })
    }

    /**
     * Saving the current zone before drag.
     */
    saveZone() {
        this.savedZone = this.zone()
    }

    /**
     * Fires the zone changed event if the block's zone has changed since the last save.
     */
    updateZone() {
        const newZone = this.zone()
        if(newZone !== this.savedZone) {
            this.fire("zoneChanged", {newZone})
        }
        this.savedZone = null
    }

    /**
     * Attempts to snap the current block on the grid.
     * @param snapMargin
     * @returns {BaseBlock}
     */
    attemptSnap(snapMargin) {
        const thisRect = this.background.getClientRect()
        const snapRect = { // Remove sockets, but container height is somehow a little smaller.
            x: thisRect.x,
            y: thisRect.y,
            width: thisRect.width,
            height: thisRect.height - this.background.strokeWidth() // Remove stroke padding
        }
        attemptRectSnap(this, snapRect, this.getStage().grid.snap, snapMargin)
        return this
    }

    /**
     * Returns the blocks linked to this one
     * @returns {BaseBlock[]}
     */
    previousBlocks() {
        let socket = this.sockets.find(s => s.linkType === Types.Link.TEXT && containsFlag(s.ioType, Socket.IO.DESTINATION))
        return socket?.links.map(l => l.fromSocket.getParent()) ?? []
    }

    /**
     * Returns the branch block in the antecedents.
     * @returns {BranchBlock|null}
     */
    currentBranch() {
        return this.previousBlocks()[0]?.currentBranch() ?? null
    }

    /**
     * Returns the zone square in which this block is.
     * @param {LogicBuilder} stage
     * @return {ZoneSquare|undefined}
     */
    currentZoneSquare(stage) {
        const squarePos = stage.grid.roundToNearestSquare({
            x: this.x() + this.width()/2,
            y: this.y() + this.height()/2,
        })
        return stage.zonesLayer
                    .find(sq => sq.hasName("ZoneSquare") && sq.x() === squarePos.x && sq.y() === squarePos.y)[0]
    }

    /**
     * Returns the zone in which this block is, and null if it isn't in any.
     * @return {Zone|null}
     */
    zone() {
        const stage = this.getStage()
        const square = this.currentZoneSquare(stage)
        return square?.findAncestor(".ZoneDelegate").zone ?? stage.defaultZone
    }

    /**
     * Buttons of the context menu.
     * @returns {MenuItem[]}
     */
    get contextMenu() {
        let buttons = [
            new MenuItem({
                text: "&Rename",
                icon: "edit",
                shortcut: "F2",
                callback: () => {
                    this.titleNode.edit.startEdit()
                }
            }),
            new MenuItem({
                text: "&Help",
                icon: "manual",
                shortcut: "F1",
                callback: () => {
                }
            })
        ]
        if(this.removable === true) {
            buttons.push(new MenuItem({
                text: "&Remove",
                icon: "trash",
                shortcut: "Delete",
                callback: () => this.prepareDeletion()
            }))
            buttons.push(new MenuItem({
                text: "&Duplicate",
                icon: "duplicate",
                shortcut: "Ctrl+D",
                callback: () => this.selfDuplicate()
            }))
        }
        return buttons
    }

    /**
     * Duplicates the current block onto the canvas.
     */
    selfDuplicate() {
        let duplicate = this.duplicate()
        duplicate.x(this.x() + this.width() / 2)
        duplicate.y(this.y() + this.height() / 2)
        this.getStage().addBlock(duplicate)
    }

    /**
     * Duplicates the current block
     * @returns {BaseBlock}
     */
    duplicate() {
        const duplicatedData = this.blockData.duplicate()
        const initialization = {
            x: Math.round(this.x()),
            y: Math.round(this.y()),
            blockData: duplicatedData
        }
        const duplicata = new this.constructor(initialization)
        // Enforcing pos, because for some reason, creating it gets it changed.
        duplicata.x(initialization.x)
        duplicata.y(initialization.y)
        return duplicata
    }

    applyUnselectedStyle() {
        super.applyUnselectedStyle()
        this.sockets.forEach(s => {
            if(containsFlag(s.anchor, Anchor.RIGHT))
                s.offsetX(Math.abs(this.offsetX()))
            else
                s.offsetX(-Math.abs(this.offsetX()))
            s.offsetY(-this.offsetY())
        })
    }

    applySelectedStyle() {
        super.applySelectedStyle()
        this.sockets.forEach(s => {
            if(containsFlag(s.anchor, Anchor.RIGHT))
                s.offsetX(Math.abs(this.offsetX()))
            else
                s.offsetX(-Math.abs(this.offsetX()))
            s.offsetY(-this.offsetY())
        })
    }
}