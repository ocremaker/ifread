/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseBlock } from "./base"
import { Socket, SocketModel } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"
import { Types } from "#abstract"
import { StartBlockData } from "#abstract/structures"

export class StartBlock extends BaseBlock {
    static displayName = "Start"

    static socketModels = [
        new SocketModel({
            ref: "originOut",
            ioType: Socket.IO.SOURCE,
            anchor: Anchor.RIGHT,
            linkType: Types.Link.TEXT,
        })
    ]

    /**
     * Object containing all the block's properties.
     * @type {StartBlockData}
     */
    accessor blockData

    /**
     *
     * @param {StartBlockData} blockData
     * @param options
     */
    constructor({ blockData, ...options }) {
        super({
            width: 100,
            height: 50,
            type: "Start",
            justTitle: true,
            colors: {
                main: "#55AA55",
                background: "#BBFFBB"
            },
            blockData: blockData ?? new StartBlockData({}),
            ...options
        })
    }
}