/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseBlock } from "./base"
import { Socket, SocketModel } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"
import { Types } from "#abstract"
import { LocationBlockData } from "#abstract/structures"

export class LocationBlock extends BaseBlock {
    static displayName = "Location"

    static socketModels = [
        new SocketModel({
            ref: "locationOut",
            ioType: Socket.IO.SOURCE,
            anchor: Anchor.RIGHT,
            linkType: Types.Link.TEXT
        })
    ]

    /**
     * Object containing all the block's properties.
     * @type {LocationBlockData}
     */
    accessor blockData

    constructor({ blockData, ...options }) {
        super({
            width: 150,
            height: 50,
            type: "Location",
            justTitle: true,
            removable: true,
            colors: {
                main: "#FF7A76",
                background: "#FFCFCD"
            },
            blockData: blockData ?? new LocationBlockData({}),
            ...options
        })
    }
}