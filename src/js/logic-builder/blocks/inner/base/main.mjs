/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseBlock } from "../../base"
import { Socket, SocketModel } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"
import { Types } from "#abstract"
import { RegularChoiceMainBlockData } from "#abstract/structures"


export class MainChoiceBlock extends BaseBlock {
    static displayName = "Choice"

    static socketModels = [
        new SocketModel({
            ref: "branches",
            ioType: Socket.IO.SOURCE,
            anchor: Anchor.RIGHT,
            linkType: Types.Link.BRANCH,
            allowMultiple: true
        }),
        new SocketModel({
            ref: "linkInTop",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.TOP,
            linkType: Types.Link.LINK,
        }),
        new SocketModel({
            ref: "linkInBottom",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.BOTTOM,
            linkType: Types.Link.LINK,
        }),
    ]

    /**
     * Object containing all the block's properties.
     * @type {RegularChoiceMainBlockData}
     */
    accessor blockData

    constructor({ parentChoice, blockData, ...options }) {
        super({
            width: 160,
            height: 50,
            type: "Choice",
            justTitle: true,
            colors: {
                main: "#C8914E",
                background: "#FAD7AC"
            },
            blockData: blockData ?? new RegularChoiceMainBlockData({}),
            ...options
        })

        this.parentChoice = parentChoice
    }
}