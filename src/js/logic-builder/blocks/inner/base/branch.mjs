/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseBlock } from "../../base"
import { Socket, SocketModel } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"
import { Types } from "#abstract"
import { BranchBlockData } from "#abstract/structures"

export class BranchBlock extends BaseBlock {
    static displayName = "Branch"

    static socketModels = [
        new SocketModel({
            ref: "associatedChoiceIn",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.LEFT,
            linkType: Types.Link.BRANCH
        }),
        new SocketModel({
            ref: "textOut",
            ioType: Socket.IO.SOURCE,
            anchor: Anchor.RIGHT,
            linkType: Types.Link.TEXT
        })
    ]

    /**
     * Object containing all the block's properties.
     * @type {BranchBlockData}
     */
    accessor blockData

    constructor({ blockData, ...options }) {
        super({
            width: 180,
            height: 50,
            type: "Branch",
            justTitle: true,
            removable: true,
            colors: {
                main: "#FF7A76",
                background: "#FFCFCD"
            },
            blockData: blockData ?? new BranchBlockData({}),
            ...options
        })
    }

    /**
     * Returns the current branch.
     * @returns {BranchBlock}
     */
    currentBranch() {
        return this
    }
}