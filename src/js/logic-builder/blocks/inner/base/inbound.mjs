/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SocketContainer, Socket } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"

const TEXT_OFFSET = "   "

/**
 * Inline element for choices which include a socket container matching an outbound block.
 */
export class Inbound extends SocketContainer {

    /**
     * @param {OutboundBlock} outbound
     * @param options
     */
    constructor({outbound, ...options}) {
        super({
            name: "Inbound",
            align: "right",
            sockets: [
                new Socket({
                    ref: `Inbound-${outbound.uuid}`,
                    ioType: Socket.IO.SOURCE,
                    anchor: Anchor.RIGHT,
                    linkType: outbound.linkType,
                })
            ],
            ...options
        })
        this.linkedOutbound = outbound
        this.currentBranch = null
        this.linkedOutbound.on("titleChanged", () => this.refreshText())
        this.refresh()
    }

    /**
     * Refreshes the element, including the corresponding currentBranch and text.
     */
    refresh() {
        let currentBranch = this.linkedOutbound.currentBranch()
        if(currentBranch !== this.currentBranch) {
            if(this.currentBranch)
                this.currentBranch.off(`titleChanged.${this.linkedOutbound.uuid}))`)
            currentBranch?.on(`titleChanged.${this.linkedOutbound.uuid}))`, () => this.refreshText())
            this.currentBranch = currentBranch
        }
        this.sockets[0].links[0]?.regenerate()
        this.refreshText()
    }

    /**
     * Refreshes the text to match the outbound's title or the associatied branch's.
     */
    refreshText() {
        let title = this.linkedOutbound.blockData.title
        let useBranchTitle = title === this.linkedOutbound.constructor.displayName || title === ""
        this.text(useBranchTitle ? (this.currentBranch?.blockData.title ?? title) : title)
    }


    /**
     * Returns the branch block in the antecedents.
     * @returns {BranchBlock|null}
     */
    currentBranch() {
        return this.linkedOutbound.currentBranch()
    }

    /**
     * Returns the blocks linked to this one
     * @returns {BaseBlock[]}
     */
    previousBlocks() {
        return this.linkedOutbound.previousBlocks()
    }

    text(value) {
        if(value === undefined) {
            let text = this.textNode.text()
            return text.substring(0, text.length-TEXT_OFFSET.length)
        }
        else
            return this.textNode.text(value+TEXT_OFFSET)
    }

    destroy() {
        this.off()
        super.destroy()
    }
}
