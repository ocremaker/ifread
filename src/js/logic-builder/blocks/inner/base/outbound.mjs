/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseBlock } from "../../base"
import { Socket, SocketModel } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"
import { Types } from "#abstract"
import { OutboundLinkBlockData, OutboundTextBlockData } from "#abstract/structures"


export class OutboundBlock extends BaseBlock {
    /**
     * Object containing all the block's properties.
     * @type {OutboundBlockData}
     */
    accessor blockData

    /**
     * @param {OutboundBlockData?} blockData
     * @param {string} linkType
     * @param {string} outboundName
     * @param options
     */
    constructor({ linkType, outboundName, ...options }) {
        super({
            width: 160,
            height: 50,
            type: `Outbound Outbound-${outboundName}`,
            justTitle: true,
            removable: true,
            colors: {
                main: "#C176DC",
                background: "#EBC1FA"
            },
            ...options
        })
        this.linkType = linkType
    }
}

export class OutboundLink extends OutboundBlock {
    static displayName = "Outbound Link"

    static socketModels = [
        new SocketModel({
            ref: "toOutbound",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.LEFT,
            linkType: Types.Link.LINK,
        })
    ]

    constructor({ blockData, ...options }) {
        super({
            linkType: Types.Link.LINK,
            outboundName: "Link",
            blockData: blockData ?? new OutboundLinkBlockData({}),
            ...options
        })
    }
}

export class OutboundText extends OutboundBlock {
    static displayName = "Outbound Text"

    static socketModels = [
        new SocketModel({
            ref: "toOutbound",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.LEFT,
            linkType: Types.Link.TEXT,
        })
    ]

    constructor({ blockData, ...options }) {
        super({
            linkType: Types.Link.TEXT,
            outboundName: "Text",
            blockData: blockData ?? new OutboundTextBlockData({}),
            ...options
        })
    }
}