/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Button, Checkbox, Socket, SocketModel } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"
import { Types } from "#abstract"
import { CoreBlockData } from "#abstract/structures"
import { BranchBlock } from "../base/"
import { openTextEditor } from "../../../../ui/index.mjs"

export class Core extends BranchBlock {
    static displayName = "Core"

    static socketModels = [
        new SocketModel({
            ref: "top-left",
            ioType: Socket.IO.SOURCE | Socket.IO.DESTINATION,
            anchor: Anchor.TOP | Anchor.LEFT,
            linkType: Types.Link.BRANCH
        }),
        new SocketModel({
            ref: "top",
            ioType: Socket.IO.SOURCE | Socket.IO.DESTINATION,
            anchor: Anchor.TOP,
            linkType: Types.Link.BRANCH
        }),
        new SocketModel({
            ref: "top-right",
            ioType: Socket.IO.SOURCE | Socket.IO.DESTINATION,
            anchor: Anchor.TOP | Anchor.RIGHT,
            linkType: Types.Link.BRANCH
        }),
        new SocketModel({
            ref: "left",
            ioType: Socket.IO.SOURCE | Socket.IO.DESTINATION,
            anchor: Anchor.LEFT,
            linkType: Types.Link.BRANCH
        }),
        new SocketModel({
            ref: "right",
            ioType: Socket.IO.SOURCE | Socket.IO.DESTINATION,
            anchor: Anchor.RIGHT,
            linkType: Types.Link.BRANCH
        }),
        new SocketModel({
            ref: "bottom-left",
            ioType: Socket.IO.SOURCE | Socket.IO.DESTINATION,
            anchor: Anchor.BOTTOM | Anchor.LEFT,
            linkType: Types.Link.BRANCH
        }),
        new SocketModel({
            ref: "bottom",
            ioType: Socket.IO.SOURCE | Socket.IO.DESTINATION,
            anchor: Anchor.BOTTOM,
            linkType: Types.Link.BRANCH
        }),
        new SocketModel({
            ref: "bottom-right",
            ioType: Socket.IO.SOURCE | Socket.IO.DESTINATION,
            anchor: Anchor.BOTTOM | Anchor.RIGHT,
            linkType: Types.Link.BRANCH
        })
    ]

    /**
     * Object containing all the block's properties.
     * @type {CoreBlockData}
     */
    accessor blockData

    constructor({ blockData, ...options }) {
        const colors = {
            main: "#99E6FA",
            background: "#E1F6FF"
        }
        super({
            width: 160,
            height: 50,
            type: "Core",
            justTitle: false,
            colors,
            blockData: blockData ?? new CoreBlockData({}),
            ...options
        })

        // Create text contents if it doesn't exist
        if(!Array.isArray(window.globals.texts[this.blockData.contentsUUID]))
            window.globals.texts[this.blockData.contentsUUID] = []

        this.container.add(...[
            this.hasTrickCheckbox = new Checkbox({
                "text": "Has trick",
                checked: this.blockData.hasTrick,
                width: 200,
                height: 30
            })
        ])

        this.button = new Button({
            text: "Edit trick",
            background: colors.background,
            width: 180,
            height: 30
        })
        if(this.blockData.hasTrick)
            this.container.add(this.button)

        // Events
        this.hasTrickCheckbox.on("checkChanged", (e) => {
            this.blockData.hasTrick = e.checked
            this.fire("hasTrickChanged", { newValue: e.checked })
        })

        this.button.on("pointerclick", () => {
            openTextEditor(`Edit text of ${this.titleNode.edit.text()}`, this.blockData.contentsUUID).then(() => {
                console.log("Saving", window.globals.texts[this.contentsUUID])
            })
        })

        this.on("hasTrickChanged", (e) => {
            if(e.newValue)
                this.container.add(this.button)
            else
                this.button.remove()
            this.hasTrickCheckbox.checked(e.newValue)
            this.container.height(this.container.calculateImplicitHeight() + 5)
        })
    }

    destroy() {
        super.destroy()
        delete window.globals.texts[this.blockData.contentsUUID]
    }

    /**
     * Duplicates the trick contents on top of the block.
     * @returns {Core}
     */
    duplicate() {
        /** @type {Core} */
        const duplicata = super.duplicate()
        window.globals.texts[duplicata.blockData.contentsUUID] = JSON.parse(JSON.stringify(window.globals.texts[this.blockData.contentsUUID]))
        return duplicata
    }
}