/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Note } from "./note"
import { TextBlock } from "./text"
import { MergeBlock } from "./merge"
import { LocationBlock } from "./location"
import { StartBlock } from "./start"
import { RegularChoice, GhostWorld } from "./choice/"
import { BranchBlock, MainChoiceBlock, OutboundLink, OutboundText } from "./inner/base/"
import { Core, FirstCore, PLACABLE as GhostWorldBlocks } from "./inner/ghost/"
import { PLACABLE as RegularChoiceBlocks } from "./inner/regular"

import {
    TextBlockData,
    LocationBlockData,
    MergeBlockData,
    NoteData,
    StartBlockData,
    OutboundLinkBlockData,
    OutboundTextBlockData,
    RegularChoiceBlockData,
    RegularChoiceMainBlockData,
    BranchBlockData,
    GhostWorldBlockData,
    CoreBlockData,
    FirstCoreBlockData
} from "#abstract/structures/"

export const BLOCK_CATEGORIES = {
    "main": [
        Note,
        TextBlock,
        LocationBlock,
        MergeBlock,
        RegularChoice,
        GhostWorld
    ],
    "RegularChoice": [
        Note,
        TextBlock,
        MergeBlock,
        ...RegularChoiceBlocks
    ],
    "GhostWorld": [
        Note,
        ...GhostWorldBlocks
    ]
}

/**
 *
 * @type {Map<typeof AbstractBlockData, typeof BaseBlock>}
 */
export const DATA_TO_BLOCKTYPE = new Map([
    [NoteData, Note],
    [StartBlockData, StartBlock],
    [TextBlockData, TextBlock],
    [MergeBlockData, MergeBlock],
    [LocationBlockData, LocationBlock],
    [RegularChoiceBlockData, RegularChoice],
    [BranchBlockData, BranchBlock],
    [RegularChoiceMainBlockData, MainChoiceBlock],
    [OutboundLinkBlockData, OutboundLink],
    [OutboundTextBlockData, OutboundText],
    [GhostWorldBlockData, GhostWorld],
    [CoreBlockData, Core],
    [FirstCoreBlockData, FirstCore],
])