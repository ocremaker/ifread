/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Rect, Transformer } from "konva"
import { EditText } from "#logic-builder/shapes"
import { confirm } from "#ui"
import { generateUUID, MenuItem } from "#utils"
import { NoteData } from "#abstract/structures"
import { Anchor, attemptRectSnap, FocusGroup } from "#logic-builder/utils"

const MIN_WIDTH = 100
const MIN_HEIGHT = 100
const TEXT_PADDING = 10

export class Note extends FocusGroup {
    static displayName = "Note"

    /**
     * Object containing all the note's properties.
     * @type {NoteData}
     */
    accessor blockData

    constructor({
        width = 200, height = 200,
        blockData = null,
        ...options
    }) {
        super({
            width, height,
            draggable: true,
            name: "Note",
            ...options
        })
        this.removable = true
        this.uuid = generateUUID()
        this.blockData = blockData ?? new NoteData({})
        // Adding child nodes
        this.add(...[
            this.bgNode = new Rect({
                width, height,
                x: 0, y: 0,
                fill: this.blockData.color
            }),
            this.textNode = new EditText({
                x: TEXT_PADDING,
                y: TEXT_PADDING,
                text: this.blockData.contents,
                width: width - TEXT_PADDING * 2,
                height: height - TEXT_PADDING * 2,
                align: "center",
                verticalAlign: "middle",
                name: "Note-Text"
            })
        ])

        // Transformer events
        this.transformer = new Transformer({
            nodes: [this.bgNode],
            padding: 5,
            flipEnabled: false,
            rotateEnabled: false,
        })
        this.bgNode.on('transform', () => {
            let width = Math.max(this.bgNode.width() * this.bgNode.scaleX(), MIN_WIDTH)
            let height = Math.max(this.bgNode.height() * this.bgNode.scaleY(), MIN_HEIGHT)
            let x = this.x() + this.bgNode.x()
            let y = this.y() + this.bgNode.y()
            this.bgNode.setAttrs({
                width, height,
                x: 0,
                y: 0,
                scaleX: 1,
                scaleY: 1,
            })
            this.setAttrs({
                x, y, width, height,
                scaleX: 1,
                scaleY: 1,
            })
            this.textNode.setAttrs({
                width: width - TEXT_PADDING * 2,
                height: height - TEXT_PADDING * 2
            })
        })

        // Allow move snapping
        this.on("dragstart", () => {
            this.textNode.validateEdit()
        })
        this.on("dragmove", () => this.attemptSnap(5))

        // Update anchors
        Anchor.updatePositions(this.getChildren())
        this.on("widthChange", () => Anchor.updatePositions(this.getChildren()))
        this.on("heightChange", () => Anchor.updatePositions(this.getChildren()))

        // Update properties
        this.textNode.on("textEdited", (e) => {
            this.fire("contentsChanged", { newValue: e.text })
        })
        this.on("contentsChanged", (e) => {
            this.textNode.text(e.newValue)
            this.blockData.contents = e.newValue
        })
        this.on("colorChanged", (e) => this.bgNode.fill(e.newValue))
    }

    attemptSnap(snapMargin) {
        attemptRectSnap(this, this.bgNode.getClientRect(), this.getStage().grid.snap, snapMargin)
        return this
    }

    /**
     * Buttons of the context menu.
     * @returns {MenuItem[]}
     */
    get contextMenu() {
        return [
            new MenuItem({
                text: "&Edit Note",
                icon: "edit",
                shortcut: "F2",
                callback: () => {
                    this.textNode.startEdit()
                }
            }),
            new MenuItem({
                text: "&Help",
                icon: "manual",
                shortcut: "F1",
                callback: () => {
                }
            }),
            new MenuItem({
                text: "&Remove",
                icon: "trash",
                shortcut: "Delete",
                callback: () => {
                    let msg = "This action will completely remove this block from the logic diagram. IT WILL NOT BE RECOVERABLE! Continue anyway?"
                    confirm("delete-element", "Delete element?", msg).then(() => {
                        this.getStage().resetCursors()
                        this.destroy()
                    })
                }
            })
        ]
    }

    /**
     * Duplicates the current note
     * @returns {Note}
     */
    duplicate() {
        const duplicatedData = this.blockData.duplicate()
        const initialization = {
            x: Math.round(this.x()), y: Math.round(this.y()),
            width: this.width(), height: this.height(),
            blockData: duplicatedData
        }
        return new this.constructor(initialization)
    }

    applySelectedStyle() {
        this.add(this.transformer)
    }

    applyUnselectedStyle() {
        this.transformer.remove()
        // Get data from text if editing.
        this.textNode.validateEdit()
    }
}