/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Rect } from "konva"
import { BaseChoice } from "./base"
import { Inbound } from "../inner/base/"
import { FirstCore } from "../inner/ghost/"
import { GhostWorldBlockData } from "#abstract/structures"

export class GhostWorld extends BaseChoice {

    static displayName = "Ghost World"

    /**
     * Object containing all the block's properties.
     * @type {GhostWorldBlockData}
     */
    accessor blockData

    constructor({ blockData, ...options }) {
        super({
            width: 200,
            height: 100,
            type: "GhostWorld",
            removable: true,
            defaultMainBlockType: FirstCore,
            defaultBranchBlockType: null,
            blockData: blockData ?? new GhostWorldBlockData({}),
            ...options
        });
    }

    /**
     * Adds an inbound element to the choice.
     * @param {Inbound} inbound
     */
    addInbound(inbound) {
        if(this.container.getChildren().length === 2) // Only title and background?
            this.container.add(new Rect({
                width: 100,
                height: 1,
                fill: this.border
            }))
        super.addInbound(inbound)
    }
}