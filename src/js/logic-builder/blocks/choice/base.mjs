/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Button, Socket, SocketModel } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"
import { openInnerDiagram } from "#ui"
import { Types } from "#abstract"
import { BaseBlock } from "../base"
import { LogicBuilder } from "../../builder"
import { Inbound, OutboundBlock } from "../inner/base/"


export class BaseChoice extends BaseBlock {

    static socketModels = [
        new SocketModel({
            ref: "textIn",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.LEFT,
            linkType: Types.Link.TEXT
        }),
        new SocketModel({
            ref: "linkInTop",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.TOP,
            linkType: Types.Link.LINK
        }),
        new SocketModel({
            ref: "linkInBottom",
            ioType: Socket.IO.DESTINATION,
            anchor: Anchor.BOTTOM,
            linkType: Types.Link.LINK
        })
    ]

    constructor({ defaultMainBlockType, defaultBranchBlockType, ...options }) {
        const colors = {
            main: "#C8914E",
            background: "#FAD7AC"
        }
        super({
            colors,
            ...options
        })
        this.container.add(...[
            this.openButton = new Button({
                text: "Edit inner diagram",
                background: colors.background,
                width: 200,
                height: 30
            })
        ])

        // Initializing inner diagram.
        this.defaultMainBlockType = defaultMainBlockType
        this.defaultBranchBlockType = defaultBranchBlockType
        this.initializeInnerDiagram()

        this.openButton.on("pointerclick", () => {
            this.openInnerDiagram()
        })

        this.on("zoneChanged", (e) => this.innerBuilder.setDefaultZone(e.newZone))
    }

    destroy() {
        // Destroy inner diagram.
        this.innerBuilder.destroy()
        delete window.globals.builders[this.innerBuilderId]
        return super.destroy()
    }

    get innerBuilderId() {
        return `${this.constructor.name}-${this.uuid}`
    }

    /**
     * Adds an inbound element to the choice.
     * @param {Inbound} inbound
     */
    addInbound(inbound) {
        this.container.add(inbound)
    }

    /**
     * Refreshes all outbound blocks within the diagram, and adds/removes the ones who need to be done so.
     */
    refreshOutbounds() {
        console.log("Refreshing outbounds.")
        /** @type {OutboundBlock[]} */
        let outboundsToCheck = Array.from(
            this.innerBuilder.find(".Outbound")
                .filter(o => o.currentBranch() !== null)
        )
        /** @type {Inbound} */
        let inbound
        for(inbound of this.container.find(".Inbound")) {
            let aob = inbound.linkedOutbound
            if(outboundsToCheck.includes(aob)) {
                // Remove outbound from checking
                outboundsToCheck.splice(outboundsToCheck.indexOf(aob), 1)
                inbound.refresh()
            } else {
                inbound.destroy() // No longer associated with any outbound block.
                this.container.refreshLayout()
            }
        }
        for(let outbound of outboundsToCheck)
            this.addInbound(new Inbound({
                width: this.width(),
                height: 20,
                outbound
            }))
    }

    /**
     * Opens a popup with this choice's inner diagram
     */
    openInnerDiagram() {
        const blockType = this.constructor.name
        const display = this.constructor.displayName
        const currentName = this.blockData.title
        const title = `Edit inner diagram of ${display === currentName ? display : `${currentName} (${display})`}`
        openInnerDiagram(title, this.innerBuilderId, blockType)
    }

    /**
     * Initializes the inner diagram (can be overridden to add default blocks for subclasses)
     * @returns {LogicBuilder}
     */
    initializeInnerDiagram() {
        let builder = window.globals.builders[this.innerBuilderId]
        if(!builder) {
            // Create new builder.
            this.innerBuilder = builder = new LogicBuilder({
                container: document.createElement("div"),
                uuid: this.innerBuilderId,
                width: 1,
                height: 1
            })
            builder.addBlock(this.mainBlock = new this.defaultMainBlockType({
                x: 0, y: 0,
                parentChoice: this
            }))
            window.globals.builders[this.innerBuilderId] = builder
        } else {
            // Load existing builder
            this.innerBuilder = builder
            this.mainBlock = builder.listBlocks().find(b => b instanceof this.defaultMainBlockType)
            this.mainBlock.parentChoice = this
            this.refreshOutbounds()
        }
        // Auto link branch blocks
        if(this.defaultBranchBlockType !== null)
            builder.on("blockAdded", (e) => {
                if(e.block instanceof this.defaultBranchBlockType) {
                    /** @type {Socket} */
                    let start = this.mainBlock.findOne(".Socket-[branches]")
                    /** @type {Socket} */
                    let dest = e.block.findOne(".Socket-[associatedChoiceIn]")
                    start.createLink(dest)
                }
            })
        // Refresh outbounds when one is added or removed.
        builder.on("blockAdded blockRemoved linkAdded linkRemoved", (e) => {
            if(e.block instanceof OutboundBlock)
                this.refreshOutbounds()
        })
        builder.on("linkAdded linkRemoved", (e) => {
            this.refreshOutbounds()
        })
        // Auto update title changes on main block.
        this.on("titleChanged", (e) => {
            this.mainBlock.setTitle(e.newValue)
        })
        this.mainBlock.on("titleChanged", (e) => {
            this.setTitle(e.newValue)
        })
        return builder
    }

    /**
     * Duplicates the choice and its inner diagram.
     * @returns {BaseChoice}
     */
    duplicate() {
        /** @type {BaseChoice} */
        const duplicata = super.duplicate()
        const uuidMap = new Map()
        // Duplicate blocks
        for(let block of this.innerBuilder.listBlocks()) {
            if(block instanceof this.defaultMainBlockType)
                uuidMap.set(block.uuid, duplicata.mainBlock)
            else {
                let newBlock = block.duplicate()
                duplicata.innerBuilder.addBlock(newBlock)
                uuidMap.set(block.uuid, newBlock)
            }
        }
        // Duplicate notes
        for(let note of this.innerBuilder.listNotes())
            duplicata.innerBuilder.addBlock(note.duplicate())
        // Duplicate links
        for(let link of this.innerBuilder.listLinks()) {
            let [fromSocket, toSocket] = [link.fromSocket, link.toSocket].map(
                s => uuidMap.get(
                    s.findAncestor(".Block").uuid
                ).findOne(`.Socket-[${s.ref}]`)
            )
            fromSocket.createLink(toSocket)
        }
        return duplicata
    }
}