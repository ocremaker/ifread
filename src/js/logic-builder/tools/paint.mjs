/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseTool } from "./base.mjs"
import { ZonePerBuilderDelegate, ZoneSquare } from "../zones/"
import { paint } from "../utils/"

export class PaintTool extends BaseTool {
    constructor(options) {
        super({ name: "Paint zone", defaultCursor: "paint", ...options })
        this.lastDetectedPos = null
    }

    onSelected(stage) {
        super.onSelected(stage)
        stage.setCursorWithPriority("paint", 52)
        stage.on("pointermove.paint", e => {
            let pressing = e.evt.pressure > 0
            if(pressing) {
                let detected = stage.getOffsetPointerPosition()
                if(this.lastDetectedPos !== null)
                    for(let between of stage.grid.squaresBetween(this.lastDetectedPos, detected))
                        this.pointedSquare(stage, between)
                this.pointedSquare(stage, detected)
                this.lastDetectedPos = detected
            }
        })
        stage.on("pointerup.paint", () => {
            this.lastDetectedPos = null
            this.pointedSquare(stage, stage.getOffsetPointerPosition())
        })
        stage.on("pointerdown.paint", () => {
            let detected = stage.getOffsetPointerPosition()
            this.pointedSquare(stage, detected)
            this.lastDetectedPos = detected
        })
    }

    pointedSquare(stage, position) {
        let zoneDelegate = stage.currentFocusedElement
        if(zoneDelegate instanceof ZonePerBuilderDelegate) {
            const squarePos = stage.grid.roundToNearestSquare(position)
            paint(squarePos, zoneDelegate, stage)
            stage.zoneUpdated(zoneDelegate.zone, {
                x1: squarePos.x, y1: squarePos.y,
                x2: squarePos.x + stage.grid.snap,
                y2: squarePos.y + stage.grid.snap,
            })
        }
    }

    onUnselected(stage) {
        super.onUnselected(stage)
        stage.removeCursorWithPriority(52)
        stage.off("pointermove.paint pointerdown.paint pointerup.paint")
    }
}