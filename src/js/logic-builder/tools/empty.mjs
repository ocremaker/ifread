/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FillTool } from "./fill.mjs"
import { Rect } from "konva/lib/shapes/Rect"
import { erase } from "../utils/"

export class EmptyTool extends FillTool {
    constructor(options) {
        super({ name: "Empty", defaultCursor: "erase", ...options })
    }

    /**
     * Creates the temporary square.
     * @param {LogicBuilder} stage
     */
    createSquare(stage) {
        this.startSquare = stage.grid.roundToNearestSquare(stage.getOffsetPointerPosition())
        this.tmpSquare = new Rect({
            x: this.startSquare.x, y: this.startSquare.y,
            width: stage.grid.snap,
            height: stage.grid.snap,
            fill: "white"
        })
        stage.zonesLayer.add(this.tmpSquare)
    }

    /**
     * Updates stage about the newly painted zone.
     * @param {LogicBuilder} stage
     * @param {{x1: number, x2: number, y1: number, y2: number}}square
     */
    updateZone(stage, square) {
        stage.zoneUpdated(null, square)
    }

    /**
     * Applies tool to square position (when released)
     * @param {{x: number, y: number}} position
     * @param {LogicBuilder} stage
     */
    applySquareTo(position, stage) {
        erase(position, stage)
    }
}