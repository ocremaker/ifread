/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { erase } from "../utils/"
import { PaintTool } from "./paint.mjs"

export class EraseTool extends PaintTool {
    constructor(options) {
        super({ name: "Erase zone", defaultCursor: "erase", ...options })
        this.lastDetectedPos = null
    }

    onSelected(stage) {
        super.onSelected(stage)
        stage.setCursorWithPriority("erase", 52)
    }

    pointedSquare(stage, position) {
        const squarePos = stage.grid.roundToNearestSquare(position)
        erase(squarePos, stage)
        stage.zoneUpdated(null, {
            x1: squarePos.x, y1: squarePos.y,
            x2: squarePos.x + stage.grid.snap,
            y2: squarePos.y + stage.grid.snap,
        })
    }
}