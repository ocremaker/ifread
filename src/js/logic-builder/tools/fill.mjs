/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Rect } from "konva/lib/shapes/Rect"
import { BaseTool } from "./base.mjs"
import { ZonePerBuilderDelegate } from "../zones/"
import { paint } from "../utils/"

export class FillTool extends BaseTool {
    constructor(options) {
        super({ name: "Fill", defaultCursor: "cell", ...options })
        this.startSquare = null
        this.tmpSquare = null
        this.currentRemoveTmpTimeout = null
    }

    onSelected(stage) {
        super.onSelected(stage)
        stage.setCursorWithPriority("cell", 52)
        stage.on("pointerdown.fill", () => {
            this.createSquare(stage)
        })
        stage.on("pointermove.fill", (e) => {
            let pressing = e.evt.pressure > 0
            if(pressing && this.tmpSquare !== null) {
                let stagePos = stage.grid.roundToNearestSquare(stage.getOffsetPointerPosition())
                this.tmpSquare.x(Math.min(stagePos.x, this.startSquare.x))
                this.tmpSquare.y(Math.min(stagePos.y, this.startSquare.y))
                this.tmpSquare.width(Math.max(stagePos.x, this.startSquare.x) + stage.grid.snap - this.tmpSquare.x())
                this.tmpSquare.height(Math.max(stagePos.y, this.startSquare.y) + stage.grid.snap - this.tmpSquare.y())
            }
            clearTimeout(this.currentRemoveTmpTimeout)
            this.currentRemoveTmpTimeout = setTimeout(() => {
                // Remove square
                this.tmpSquare?.remove()
                this.tmpSquare = null
            }, 500)
        })
        stage.on("pointerup.fill", () => {
            if(this.tmpSquare !== null) {
                // Fill square
                for(let x = 0; x < this.tmpSquare.width(); x += stage.grid.snap) {
                    for(let y = 0; y < this.tmpSquare.height(); y += stage.grid.snap)
                        this.applySquareTo({
                            x: x + this.tmpSquare.x(),
                            y: y + this.tmpSquare.y()
                        }, stage)
                }
                this.updateZone(stage, {
                    x1: this.tmpSquare.x(), y1: this.tmpSquare.y(),
                    x2: this.tmpSquare.x() + this.tmpSquare.width(),
                    y2: this.tmpSquare.y() + this.tmpSquare.height(),
                })
                // Remove square
                this.tmpSquare.remove()
                this.tmpSquare = null
            }
        })
    }

    onUnselected(stage) {
        super.onUnselected(stage)
        stage.removeCursorWithPriority(52)
        stage.off("pointermove.fill pointerdown.fill pointerup.fill")
    }

    /**
     * Creates the temporary square.
     * @param {LogicBuilder} stage
     */
    createSquare(stage) {
        let zoneDelegate = stage.currentFocusedElement
        if(zoneDelegate instanceof ZonePerBuilderDelegate) {
            // Creating temporary square
            this.startSquare = stage.grid.roundToNearestSquare(stage.getOffsetPointerPosition())
            this.tmpSquare = new Rect({
                x: this.startSquare.x, y: this.startSquare.y,
                width: stage.grid.snap, height: stage.grid.snap,
                fill: zoneDelegate.zone.blockData.color
            })
            stage.zonesLayer.add(this.tmpSquare)
        }
    }

    /**
     * Updates stage about the newly painted zone.
     * @param {LogicBuilder} stage
     * @param {{x1: number, x2: number, y1: number, y2: number}}square
     */
    updateZone(stage, square) {
        stage.zoneUpdated(stage.currentFocusedElement.zone, square)
    }

    /**
     * Applies tool to square position (when released)
     * @param {{x: number, y: number}} position
     * @param {LogicBuilder} stage
     */
    applySquareTo(position, stage) {
        paint(position, stage.currentFocusedElement, stage)
    }
}