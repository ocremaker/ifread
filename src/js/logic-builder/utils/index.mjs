/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export * from './positioning/'
export * from './snap'
export * from './focus'
export * from './paint'

import { Layer, Node, Group } from "konva"

/**
 * Sets the cursor that will appear when hovering the node
 * @param {Node} node
 * @param {string} cursorName
 */
export function setNodeCursor(node, cursorName) {
    let depth
    /** @type {LogicBuilder} */
    let stage

    let onFirstRun = () => {
        depth = node.getDepth()
        stage = node.getStage()
    }

    node.on("mouseenter", (e) => {
        if(depth === undefined) onFirstRun()
        stage.setCursorWithPriority(cursorName, depth)
    })
    node.on("mouseleave", () => {
        if(depth === undefined) onFirstRun()
        stage.removeCursorWithPriority(depth)
    })
}



