/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Utils for painting zones.

import { ZoneSquare } from "../zones/"

/**
 * Paints a given square at a given position within to a given zone.
 * @param {{x: number, y: number}} squarePos
 * @param {ZonePerBuilderDelegate} zoneDelegate
 * @param {LogicBuilder} stage
 */
export function paint(squarePos, zoneDelegate, stage) {
    const intersection = stage.zonesLayer
                              .find(sq => sq.hasName("ZoneSquare") && sq.x() === squarePos.x && sq.y() === squarePos.y)[0]
    if(intersection instanceof ZoneSquare) {
        // Move square to new color.
        if(intersection.getParent()?.getParent() !== zoneDelegate) {
            zoneDelegate.moveSquareToGroup(intersection)
        }
    } else {
        zoneDelegate.addSquare(squarePos)
    }
}

/**
 * Erases a given square at a given position.
 * @param {{x: number, y: number}} squarePos
 * @param {LogicBuilder} stage
 */
export function erase(squarePos, stage) {
    const intersection = stage.zonesLayer
                              .find(sq => sq.hasName("ZoneSquare") && sq.x() === squarePos.x && sq.y() === squarePos.y)[0]
    if(intersection instanceof ZoneSquare) {
        // Removing intersection.
        intersection.remove()
    }
}