/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { containsFlag, ExtensionClass, flags } from "#utils"
import { Group, Node } from "konva"


@flags
export class Anchor {
    static accessor LEFT
    static accessor RIGHT
    static accessor TOP
    static accessor BOTTOM

    /**
     * Calls the updatePosition method of every anchorable node within nodes.
     * @param {(AnchorableDelegate|Node)[]} nodes
     */
    static updatePositions(nodes) {
        for(let node of nodes)
            if(Anchorable.isAncestorOf(node))
                node.updatePosition()
    }
}

const ANCHORS = Object.values(Anchor)

/**
 * Extension class for Konva.Node which sends events when its position or dimensions are changed.
 *
 * @param {typeof Node} baseClass - Class to extend.
 * @returns {typeof Node}
 */
export const Anchorable = new ExtensionClass(Node, (C) =>
    /**
     * @class {AnchorableDelegate}
     * @extends {Node}
     */
    class AnchorableDelegate extends C {

        constructor({ anchor, ...options }) {
            super({
                x: 0, y: 0,
                ...options
            })
            anchor = parseInt(anchor)
            if(isNaN(anchor) || anchor < 0 || anchor > 12)
                throw new Error(`Given anchor '${typeof anchor} ${anchor}' is not a valid set of anchor flags. Use the Anchor enum to create one.`)
            this.anchor = anchor
        }

        set anchor(value) {
            super.anchor = value
            if(this.parent != null)
                this.updatePosition()
        }

        updatePosition() {
            let x, y
            let parent = this.getParent()
            // Calculate x value
            if(containsFlag(this.anchor, Anchor.LEFT))
                x = -this.width() / 2
            else if(containsFlag(this.anchor, Anchor.RIGHT))
                x = parent.width() - this.width() / 2
            else // Default to center
                x = (parent.width() - this.width()) / 2
            // Calculate y value
            if(containsFlag(this.anchor, Anchor.TOP))
                y = -this.height() / 2
            else if(containsFlag(this.anchor, Anchor.BOTTOM))
                y = parent.height() - this.height() / 2
            else // Default to center
                y = (parent.height() - this.height()) / 2
            this.x(x)
            this.y(y)
        }
    }
)

export const AnchorableGroup = Anchorable(Group)
