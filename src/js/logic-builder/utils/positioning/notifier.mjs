/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Node } from "konva"

export const BoundingRectNotifier = {
    /**
     * Applies listener to element to notify parent of position changes.
     * @param {Node} node
     */
    setupOn(node) {
        if(!this.isSetupOn(node)) {
            node.notifiesBoundingRectChanges = true
            for(let prop of ["Width", "Height", "X", "Y"])
                node.on(`${prop.toLowerCase()}Change`, (e) => {
                    let parent = node.getParent()
                    // console.log(`${prop} of (${node.name()}) under Parent(${parent?.name()}) changed from ${e.oldVal} to ${e.newVal}`)
                    if(parent != null)
                        parent.fire(`child${prop}Change`, e)
                })
        }
    },

    /**
     * Checks if the notifier has already been setup on a node.
     *
     * @param {Node} node
     * @returns {boolean}
     */
    isSetupOn(node) {
        return node.notifiesBoundingRectChanges === true
    }
}