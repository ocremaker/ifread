/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ExtensionClass, Shortcut } from "#utils"
import { Group, Node } from "konva"

/**
 * Decorator for parent classes, meant to provide additional functionality to nodes/groups/layouts that can be focused.
 * @type {ExtensionClass}
 */
export const FocusCaptor = new ExtensionClass(Node, (C) =>
    /**
     * @class {FocusCaptorDelegate}
     * @extends {Node}
     */
    class FocusCaptorDelegate extends C {
        /**
         * @param {Shortcut[]} shortcuts - List of shortcuts not listed in the context menu.
         * @param options
         */
        constructor({ shortcuts = [], ...options }) {
            super({
                ...options,
                name: "FocusCaptor"
            })
            if(options.name)
                this.addName(options.name)
            // Ensure the block is selectable
            this.on("focus", () => {
                this.getStage()?.container().dispatchEvent(new CustomEvent("selected", {
                    detail: this
                }))
                this.applySelectedStyle()
            })
            this.on("unfocus", () => {
                this.getStage()?.container().dispatchEvent(new CustomEvent("selected", {
                    detail: null
                }))
                this.applyUnselectedStyle()
            })
            // Handle shortcuts
            this.shortcuts = [
                new Shortcut(" ", () => this.clickButton()),
                new Shortcut("Enter", () => this.clickButton()),
                new Shortcut("ArrowLeft", () => this.x(this.x() - 10)),
                new Shortcut("ArrowRight", () => this.x(this.x() + 10)),
                new Shortcut("ArrowUp", () => this.y(this.y() - 10)),
                new Shortcut("ArrowDown", () => this.y(this.y() + 10)),
                new Shortcut("P", () =>
                    this.getStage()?.container().parentNode.querySelector("#block-properties").focus()
                ),
                ...shortcuts,
            ]
            this.on("keydown", (e) => {
                let shortcuts = [
                    ...this.shortcuts,
                    ...this.contextMenu.map(v => v.shortcut).filter(s => s !== null)
                ]
                for(let shortcut of shortcuts)
                    if(shortcut !== null && shortcut.checkMatchAndRun(e)) {
                        e.evt.preventDefault()
                    }
            })
        }

        /**
         * Buttons of the context menu.
         * @returns {MenuItem[]}
         */
        get contextMenu() {
            throw new Error("Not implemented.")
        }

        /**
         * Clicks the first element in the layout it finds.
         */
        clickButton() {
            let btn = this.findOne?.call(this, ".Button")
            if(btn)
                btn.click()
        }

        /**
         * Applies the style to the node when it is selected.
         */
        applySelectedStyle() {
            throw new Error("Not implemented.")
        }

        /**
         * Applies the style to the node when it is deselected.
         */
        applyUnselectedStyle() {
            throw new Error("Not implemented.")
        }
    }
)

export const FocusGroup = FocusCaptor(class extends Group {})