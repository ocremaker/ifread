/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Snapping utils
 */

/**
 * The offset of a value from snapping to the previous line on the grid.
 * @param {number} value
 * @param {number} snapEvery
 * @returns {number}
 */
function offsetToSnapBefore(value, snapEvery) {
    return value % snapEvery
}

/**
 * The offset of a value from snapping to the next line on the grid.
 * @param {number} value
 * @param {number} snapEvery
 * @returns {number}
 */
function offsetToSnapAfter(value, snapEvery) {
    return -(value % snapEvery - snapEvery)
}

/**
 * Gets a value on an axis (x or y), a dimension (width or eight), and a number representing
 * the amount of pixels each line of the snap grid should be separated by, and this function
 * will return to you to the snapped value if value is within the snap margins of any line,
 * and the original value if no snapping is possible.
 *
 * @param {number} value
 * @param {number} dimension
 * @param {number} snapEvery
 * @param {number} snapMargin
 * @returns {number}
 */
export function attemptSnapOnAxis(value, dimension, snapEvery, snapMargin) {
    let valueEnd = value + dimension
    let valueMiddle = value + dimension / 2
    let snaps = [
        { offset: offsetToSnapBefore(value, snapEvery) }, // Snap block to before
        { offset: offsetToSnapAfter(valueEnd, snapEvery) }, // Snap block to after
        { offset: offsetToSnapBefore(valueMiddle, snapEvery) }, // Snap middle of block to before
        { offset: offsetToSnapAfter(valueMiddle, snapEvery) } // Snap middle of block to before
    ]
    let result = value
    for(let snap of snaps) {
        if(Math.abs(snap.offset) < snapMargin) {
            result = value - snap.offset
        }
    }
    return result
}

/**
 * Attempts to snap the node based on its rect on the grid.
 *
 * @param {Node|Group} node
 * @param {{x: number, y: number, width: number, height: number}} rect
 * @param {number} snapEvery - Amount of pixel separating each snap line of the grid
 * @param {number} snapMargin - The allowed margin to which snap should be attempted.
 */
export function attemptRectSnap(node, rect, snapEvery, snapMargin) {
    const stage = node.getStage()
    const scale = stage.scaleX()
    const x = attemptSnapOnAxis((rect.x - stage.x()) / scale, rect.width / scale, snapEvery, snapMargin)
    const y = attemptSnapOnAxis((rect.y - stage.y()) / scale, rect.height / scale, snapEvery, snapMargin)
    if(x !== rect.x)
        node.x(x)
    if(y !== rect.y)
        node.y(y)
}
