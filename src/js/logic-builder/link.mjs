/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Group } from "konva"
import { ComplexArrow, HorizontalLayout, Socket } from "./shapes/"
import { FocusCaptor } from "./utils/"
import { Alert } from "./alert.mjs"
import { containsFlag } from "#utils"
import { CalcIf, Variable, Types } from "#abstract"
import { AbstractLink, AbstractSocket } from "#abstract/structures"

/** From IconMonstr, Under their license */
const ICON_ALERT = "m12.002 21.534c5.518 0 9.998-4.48 9.998-9.998s-4.48-9.997-9.998-9.997c-5.517 0-9.997 4.479-9.997 9.997s4.48 9.998 9.997 9.998zm0-8c-.414 0-.75-.336-.75-.75v-5.5c0-.414.336-.75.75-.75s.75.336.75.75v5.5c0 .414-.336.75-.75.75zm-.002 3c-.552 0-1-.448-1-1s.448-1 1-1 1 .448 1 1-.448 1-1 1z"
const ICON_LIGHTNING = "M8 24l3-9h-9l14-15-3 9h9l-14 15z"
const ICON_WARN = "M24 20.75h-24l12-22zM11 13.75h2v-7h-2v7zM12 18q0.52083 0 0.88542-0.36458t0.36458-0.88542-0.36458-0.88542-0.88542-0.36458-0.88542 0.36458-0.36458 0.88542 0.36458 0.88542 0.88542 0.36458z"
/** Transformed from Open Iconic, Under MIT */
const ICON_FORK = "M4.5 0C2.01 0 0 2.01 0 4.5c0 1.98 1.23 3.6 3 4.23V15.3c-1.77 0.6-3 2.25-3 4.23 0 2.49 2.01 4.5 4.5 4.5S9 22.02 9 19.53c0-1.8-1.02-3.3-2.52-4.02 0.27-0.27 0.63-0.48 1.02-0.48h6c2.46 0 4.5-2.04 4.5-4.5v-1.77c1.77-0.6 3-2.25 3-4.23C21 2.04 18.99 0.03 16.5 0.03S12 2.04 12 4.53c0 1.98 1.23 3.6 3 4.23v1.77c0 0.84-0.66 1.5-1.5 1.5h-6c-0.51 0-1.05 0.12-1.5 0.27V8.73c1.77-0.6 3-2.25 3-4.23C9 2.01 6.99 0 4.5 0z"

const ICON_HEIGHT = 24


export const COLORS = new Map([
    [Types.Link.TEXT, "#3F67BA"],
    [Types.Link.LINK, "#FFBB33"],
    [Types.Link.BRANCH, "#DD5555"],
    [Types.Link.CONDITION, "#55AA55"],
])

@FocusCaptor
class FocusComplexArrow extends ComplexArrow {
}

export class Link extends FocusComplexArrow {
    static displayName = "Link"

    /**
     *
     * @param {AbstractLink} blockData
     * @param {string} linkType - Type of string from Types.Link
     * @param options
     */
    constructor({ blockData, linkType, ...options }) {
        const color = COLORS.get(linkType)
        super({
            name: "Link",
            stroke: color,
            fill: color,
            ...options
        })
        // Generate link abstract block data
        this.blockData = blockData ?? new AbstractLink({
            uuid: this.uuid,
            linkType,
            from: new AbstractSocket({ ref: this.fromSocket.ref, block: this.startBlock.uuid }),
            to: new AbstractSocket({ ref: this.toSocket.ref, block: this.endBlock.uuid }),
        })
        // Create alerts
        this.pointerAtBeginning(this.canBeBidirectional() && this.blockData.allowBidirectional)
        this.alerts = new Map()
        this.alertsGroup = new HorizontalLayout({
            minHeight: ICON_HEIGHT,
            zIndex: 10
        })
        // Listen to events
        this.on("contextmenu", (e) => {
            // Right-clicking to destroy.
            e.evt.preventDefault()
            /** @type {LogicBuilder} */
            let stage = this.getStage()
            stage.resetCursors()
            stage.removeLink(this)
        })
        this.on("allowBidirectionalChanged", (e) => {
            this.pointerAtBeginning(this.canBeBidirectional() && e.newValue)
        })
        this.on("conditionChanged", (e) => this.updateAlertsWithAssignments())
        this.on("assignmentsChanged", (e) => this.updateAlertsWithAssignments())
        this.startBlock.on("zoneChanged", (e) => this.updateAlertsWithAssignments())
        this.endBlock.on("zoneChanged", (e) => this.updateAlertsWithAssignments())
    }

    canBeBidirectional() {
        return containsFlag(this.fromSocket.ioType, Socket.IO.DESTINATION) && containsFlag(this.toSocket.ioType, Socket.IO.SOURCE)
    }

    remove() {
        super.remove()
        this.alertsGroup.remove()
    }

    regenerate() {
        super.regenerate()
        if(this.alerts !== undefined) // Initialized.
            this.repositionAlerts()
    }

    /**
     * Generates new alerts for assignments if necessary.
     */
    updateAlertsWithAssignments() {
        let repositionAlerts = false

        repositionAlerts = this.registerAlertIf(
            this.blockData.condition !== null, "hasCondition",
            () => new Alert({
                color: "#67E65E",
                icon: ICON_FORK,
                tooltipWidth: 330,
                text: "Requires condition to be fulfilled to link."
            })
        ) || repositionAlerts

        const conditionHasErrors = this.blockData.condition !== null && this.blockData.condition.expressionHasErrors
        repositionAlerts = this.registerAlertIf(
            conditionHasErrors, "errorCondition",
            () => new Alert({
                color: "#CC4444",
                icon: ICON_ALERT,
                tooltipWidth: 250,
                text: `Errors detected in condition.`
            })
        ) || repositionAlerts

        /** @type {VariableAssignment[]} */
        const as = Array.from(this.blockData.assignments)
        repositionAlerts = this.registerAlertIf(
            as.length > 0, "hasAssignments",
            () => new Alert({
                color: "#E6D05E",
                icon: ICON_LIGHTNING,
                tooltipWidth: 250,
                text: "Assigns variables during transit."
            })
        ) || repositionAlerts

        repositionAlerts = this.registerAlertIf(
            as.length > 0 && this.blockData.linkType === Types.Link.TEXT, "assignmentsOnText",
            () => new Alert({
                color: "#5EE6DB",
                icon: ICON_WARN,
                width: 28,
                tooltipWidth: 450,
                text: "Assigning variables on text links create transition buttons."
            })
        ) || repositionAlerts

        const usableVars = this.startBlock.zone()?.listAllDefinedVariables() ?? []
        const targetVars = this.endBlock.zone()?.listAllDefinedVariables() ?? []
        const objectValues =  Variable.convertDefinitionsToObjectValues(usableVars)
        const hasInvalids = as.some(a => {
            /** @type {VariableDefinition} */
            const target = a.getTargetDefinitionWithin(targetVars)
            let hasErrors = target === undefined || a.expressionHasErrors
            if(!hasErrors) {
                const targetType = Variable.TYPE_TO_CALCIFTYPE.get(target.varType)
                const errors = CalcIf.identifyErrors({
                    tokens: a.calcIfExpression,
                    tokenTypes: CalcIf.DEFAULT_TOKEN_TYPES,
                    expectedType: targetType,
                    objectValues,
                })
                hasErrors = errors.length > 0
            }
            return hasErrors
        })
        repositionAlerts = this.registerAlertIf(
            hasInvalids, "errorAssignments",
            () => new Alert({
                color: "#CC4444",
                icon: ICON_ALERT,
                tooltipWidth: 250,
                text: `Errors detected in assignments.`
            })
        ) || repositionAlerts

        if(repositionAlerts)
            this.repositionAlerts()
    }

    /**
     * Registers an alert icon if a condition is met, and removes it if it's present and condition isn't met.
     * @param {boolean} condition
     * @param {string} key
     * @param {function: Alert} createAlert
     * @returns {boolean}
     */
    registerAlertIf(condition, key, createAlert) {
        let added = false
        if(condition) {
            if(!this.alerts.has(key)) {
                const alert = createAlert()
                this.alerts.set(key, alert)
                this.alertsGroup.add(alert)
                added = true
            }
        } else {
            if(this.alerts.has(key)) {
                const og = this.alerts.get(key)
                this.alerts.delete(key)
                og.remove()
                added = true
            }
        }
        return added
    }

    /**
     * Repositions the alerts on the middle of arrow.
     */
    repositionAlerts() {
        const pts = this.points()
        /** @type {{x: number, y: number}[]} */
        const points = []
        for(let i = 0; i < (pts.length - 1) / 2; i++)
            points.push({ x: pts[i * 2], y: pts[i * 2 + 1] })
        const origin = Math.floor(points.length / 2) - 1
        const target = origin + 1
        const point = {
            x: (points[origin].x + points[target].x) / 2,
            y: (points[origin].y + points[target].y) / 2 - ICON_HEIGHT / 2
        }
        this.alertsGroup.refreshLayout()
        this.alertsGroup.x(this.x() + point.x - this.alerts.size * ICON_HEIGHT / 2)
        this.alertsGroup.y(this.y() + point.y)
        // Check if alerts group has been added to layer.
        this.alertsGroup.remove()
        this.getStage().mainLayer.add(this.alertsGroup)

    }

    applySelectedStyle() {
        this.strokeWidth(8)
    }

    applyUnselectedStyle() {
        this.strokeWidth(4)
    }
}