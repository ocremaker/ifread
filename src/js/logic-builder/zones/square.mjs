/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Rect } from "konva/lib/shapes/Rect"
import { Text } from "../shapes/"
import { darkenColor } from "../../utils/index.mjs"

const positiveMod = (number, by) => ((number % by) + by) % by
const rotation = 326.25



export class ZoneSquare extends Rect {
    constructor({ size, fill, x, y, zoneTitle, ...options }) {
        super({
            x, y, fill,
            width: size,
            height: size,
            name: "ZoneSquare",
            ...options
        })
        this.text = null
        const [ xPos, yPos ] = [x / size, y / size]
        let xOffset = positiveMod(yPos, 5)
        if(positiveMod(xPos, 5) === xOffset && positiveMod(yPos, 2) === 0) {
            this.recreateText({ zoneTitle, fill, x, y, size })
        }
    }

    recreateText({ zoneTitle, fill, x, y, size }) {
        this.text = new Text({
            text: zoneTitle.toUpperCase(),
            fontSize: 30,
            rotation: rotation,
            fontStyle: "900",
            globalCompositeOperation: "source-atop",
            fill: darkenColor(fill, .1)
        })
        this.repositionText({ x, y, size})
    }

    repositionText({ x, y, size }) {
        const width = this.text.width()/2
        const height = this.text.height()/2
        const sin = Math.sin(rotation * Math.PI / 180)
        const cos = Math.cos(rotation * Math.PI / 180)
        // sin(342deg) = -0.31, cos(342deg) = 0.95
        this.text.x(x + size/2 - width * cos + height * sin)
        this.text.y(y + size/2 - height * cos - width * sin)
    }

    remove() {
        this.text?.remove()
        return super.remove()
    }
}