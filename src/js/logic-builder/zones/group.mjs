/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FocusGroup, setNodeCursor } from "../utils/"
import { darkenColor, generateUUID } from "#utils"
import { ZoneSquare } from "./square.mjs"
import { Group } from "konva"

/**
 * A 'zone delegate' is an abstract representative of a lot of squares represented on the canvas.
 */
export class ZonePerBuilderDelegate extends FocusGroup {

    /**
     *
     * @param {Zone} zone
     * @param options
     */
    constructor({ zone = null, ...options }) {
        super({
            x: 0,
            y: 0,
            name: "ZoneDelegate",
            ...options
        })
        this.zone = zone
        // this.uuid = generateUUID()

        this.squaresGroup = new Group()
        this.textsGroup = new Group()
        this.add(this.squaresGroup)
        this.add(this.textsGroup)

        this.zone.on("colorChanged", (e) => {
            const color = this.zone.blockData.color
            for(const child of this.squaresGroup.getChildren())
                child.fill(color)
            for(const text of this.textsGroup.getChildren())
                text.fill(darkenColor(color, .1))
        })

        this.zone.on("titleChanged", (e) => {
            const title = this.zone.blockData.title.toUpperCase()
            for(const child of this.squaresGroup.getChildren()) {
                if(child.text !== null) {
                    child.text.text(title)
                    child.repositionText({ x: child.x(), y: child.y(), size: child.width() })
                }
            }
        })

        setNodeCursor(this, "pointer")
    }

    /**
     * Adds a square at the given coordinates.
     * @param {number} x
     * @param {number} y
     */
    addSquare({ x, y }) {
        const size = this.getStage().grid.snap
        const square = new ZoneSquare({
            x, y, size,
            zoneTitle: this.zone.blockData.title,
            fill: this.zone.blockData.color
        })
        this.squaresGroup.add(square)
        if(square.text !== null)
            this.textsGroup.add(square.text)
    }

    /**
     * Moves a square to the group.
     * @param {ZoneSquare} square
     */
    moveSquareToGroup(square) {
        square.fill(this.zone.blockData.color)
        square.moveTo(this.squaresGroup)
        if(square.text !== null) {
            square.text.remove()
            square.recreateText({
                zoneTitle: this.zone.blockData.title.toUpperCase(),
                fill: this.zone.blockData.color,
                x: square.x(), y: square.y(),
                size: square.width(),
            })
            this.textsGroup.add(square.text)
        }
    }

    applySelectedStyle() {

    }

    applyUnselectedStyle() {
    }

    destroy() {
        const stage = this.getStage()
        for(const square of this.squaresGroup.getChildren())
            stage.zoneUpdated(null, {
                x1: square.x(), y1: square.y(),
                x2: square.x() + square.width(),
                y2: square.y() + square.height()
            })
        return super.destroy()
    }
}