/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Node } from "konva"
import { getRandomColor } from "#utils"
import { AbstractZone } from "#abstract/structures"
import { ZonePerBuilderDelegate } from "./group"

export class Zone extends Node {

    /**
     * Registers the zone globally.
     * @param {Zone} zones
     */
    static register(...zones) {
        /** @type {LogicBuilder[]} */
        let builders = Object.values(window.globals.builders)
        for(let zone of zones) {
            window.globals.zones[zone.uuid] = zone
            for(let builder of builders)
                builder.addZoneDelegate(zone.createDelegate(builder))
        }
    }

    /**
     * Returns a list of declared zones who are children of a given zone.
     * @param {Zone|null} zone
     * @returns {Zone[]}
     */
    static listChildrenOf(zone = null) {
        return Object.values(window.globals.zones).filter(z => z !== null && z.parentZone === zone)
    }

    /**
     *
     * @param {AbstractZone?} blockData
     * @param options
     */
    constructor({ blockData, ...options }) {
        super({
            x: 0, y: 0,
            ...options
        })
        this.blockData = blockData ?? new AbstractZone({
            color: getRandomColor(true),
        })
        /** @type {Map<string, ZonePerBuilderDelegate>} */
        this.delegates = new Map()
    }

    /**
     * @return {string}
     */
    get uuid() {
        return this.blockData.uuid
    }

    /**
     * @return {Zone|null}
     */
    get parentZone() {
        return window.globals.zones[this.blockData.parentZone] ?? null
    }

    /**
     * Returns the list of zones that are ancestors (parent & parent of parents) of this zone.
     * @return {Zone[]}
     */
    listAncestors() {
        let zone = this
        const zones = []
        while(zone.parentZone !== null) {
            zones.push(zone.parentZone)
            zone = zone.parentZone
        }
        return zones
    }

    /**
     * Returns a list of all variable definitions from this zone and its ancestors.
     * @return {VariableDefinition[]}
     */
    listAllDefinedVariables() {
        return [this, ...this.listAncestors()].flatMap(z => Array.from(z.blockData.variables))
    }

    /**
     * Creates a Zone delegate that will contain the squares for each builder.
     * @param {LogicBuilder} builder
     * @return {ZonePerBuilderDelegate}
     */
    createDelegate(builder) {
        let delegate = new ZonePerBuilderDelegate({
            zone: this
        })
        this.delegates.set(builder.uuid, delegate)
        return delegate
    }

    destroy() {
        delete window.globals.zones[this.uuid]
        for(let children of Zone.listChildrenOf(this))
            children.destroy()
        for(let delegate of this.delegates.values())
            delegate.destroy()
        this.delegates.clear() // Dereference
        return super.destroy()
    }
}