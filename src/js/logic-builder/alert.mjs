/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Path, Rect } from "konva"
import { StackLayout, Text, VerticalLayout } from "./shapes/index.mjs"

const TEXT_PADDING = 10

export class Alert extends Path {

    constructor({ color, icon, text = "", tooltipWidth = 150, width = 24, height = 24, ...options }) {
        super({
            width, height,
            name: "Alert",
            data: icon,
            fill: color,
            stroke: "black",
            strokeWidth: 1,
            strokeEnabled: true,
            ...options
        })
        // Adding child nodes
        this.textNode = new Text({
            width: tooltipWidth-20,
            height: 16,
            text,
            fill: "white",
            align: "center",
            verticalAlign: "middle",
            name: "Checkbox-Text"
        })

        this.toolip = new StackLayout({
            minHeight: 26,
            minWidth: 30,
            width: tooltipWidth
        }).add(
            new Rect({
                fill: "black"
            }),
            this.textNode
        )

        this.on("pointerenter", (e) => {
            this.getStage().mainLayer.add(this.toolip)
        })

        this.on("pointerleave", (e) => {
            this.toolip.remove()
        })

        this.on("pointermove", (e) => {
            const parent = this.getParent()
            this.toolip.x(parent.x() + this.x() + this.width() / 2 - this.toolip.width() / 2)
            this.toolip.y(parent.y() + this.y() - this.toolip.height() - 5)
        })
    }

}