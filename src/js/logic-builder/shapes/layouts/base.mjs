/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Anchorable, BoundingRectNotifier } from "#logic-builder/utils"
import { Layer, Group } from "konva"

/**
 * Socket class for all layouts.
 *
 * In layouts, width and height are computed dynamically unless specifically set.
 */
export class BaseLayout extends Group {
    constructor({ gap = 0, elements = [], ...options }) {
        super({
            resizable: false,
            ...options
        })
        this.gap = gap
        // Listener for width change on children.
        this.on("childWidthChange", (e) => {
            this.elementWidthUpdated(e.target)
        })
        this.on("childHeightChange", (e) => {
            this.elementHeightUpdated(e.target)
        })
    }

    add(...children) {
        for(let c of children) {
            if(!Anchorable.isAncestorOf(c))
                BoundingRectNotifier.setupOn(c)
            super.add(c)
        }
        this.refreshLayout()
        return this
    }

    /**
     * Returns the list of all children who have been setup by BoundingRectNotifier
     * @returns {Node[]}
     */
    getLayoutElements() {
        return this.getChildren().filter(c => BoundingRectNotifier.isSetupOn(c))
    }

    /**
     * Called when one of the layout element's (children have been setup by BoundingRectNotifier) width has been updated.
     * @param {Node} element
     */
    elementWidthUpdated(element) {
        // Updates the current width if the implicit width has changed.
        let implicit = this.calculateImplicitWidth()
        if(implicit !== this.width())
            this.width(implicit)
    }

    /**
     * Called when one of the layout element's (children who have been setup by BoundingRectNotifier) height has been updated.
     * @param {Node} element
     */
    elementHeightUpdated(element) {
        // Updates the current height if the height width has changed.
        let implicit = this.calculateImplicitHeight()
        if(implicit !== this.height())
            this.height(implicit)
    }

    /**
     * The implicit width is the width the layout would have based on its elements.
     *
     * @returns number
     */
    calculateImplicitWidth() {
        throw new Error("Not implemented.")
    }

    /**
     * The implicit height is the height the layout would have based on its elements.
     *
     * @returns number
     */
    calculateImplicitHeight() {
        throw new Error("Not implemented.")
    }

    /**
     * Recalculates the position of every child node within the layout,
     * and potentially the dimensions of the layout itself.
     */
    refreshLayout() {
        throw new Error("Not implemented.")
    }
}