/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseLayout } from "./base"
import { Rect } from "konva/lib/shapes/Rect"

/**
 * Stack layouts make all children take the entire space within the layout.
 * Useful for layering backgrounds with other types of nodes.
 */
export class StackLayout extends BaseLayout {
    /**
     *
     * @param minWidth - Minimum width of the layout
     * @param minHeight - Minimum height of the layout
     * @param {Rect} background - Optional background
     * @param name - Name of the layout.
     * @param options - Other options
     */
    constructor({ minWidth = 0, minHeight = 0, background = null, name, ...options }) {
        super({
            name: "StackLayout",
            ...options
        })
        if(name)
            this.addName(name)
        this.minWidth = minWidth
        this.minHeight = minHeight
        if(background) {
            this.add(background)
            this.background = background
        }
    }

    height(value) {
        if(value === undefined) // Getter
            return super.height()
        else if(super.height() !== value) {
            // Updates all children's heights to fill the layout
            super.height(value)
            this.refreshLayout()
        }
    }

    width(value) {
        if(value === undefined) // Getter
            return super.width()
        else if(super.width() !== value) {
            // Updates all children's heights to fill the layout
            super.width(value)
            this.refreshLayout()
        }
    }

    getCalculatingElements() {
        return super.getLayoutElements().filter(e => e !== this.background)
    }

    calculateImplicitWidth() {
        let map = this.getCalculatingElements().map(e => e.width())
        return Math.max(this.minWidth, ...map)
    }

    calculateImplicitHeight() {
        let map = this.getCalculatingElements().map(e => e.height())
        return Math.max(this.minHeight, ...map)
    }

    refreshLayout() {
        let width = this.width()
        let height = this.height()
        for(let element of this.getLayoutElements()) {
            element.x(0)
            element.y(0)
            if(element.width() !== width)
                element.width(width)
            if(element.height() !== height)
                element.height(height)
        }
    }
}
