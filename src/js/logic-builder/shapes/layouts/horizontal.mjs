/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { BaseLayout } from "./base"

export class HorizontalLayout extends BaseLayout {
    constructor({ minHeight = 0, verticalPadding = 0, name, ...options }) {
        super({
            name: `HorizontalLayout`,
            ...options
        });
        if(name)
            this.addName(name)
        this.minHeight = minHeight
        this.verticalPadding = verticalPadding
    }

    height(value) {
        if(value === undefined) // Getter
            return super.height()
        else if(super.height() !== value) {
            // Updates all children's heights to fill the layout
            super.height(value)
            this.refreshLayout()
        }
    }

    elementWidthUpdated(element) {
        super.elementWidthUpdated(element);
        this.refreshLayout();
    }

    elementHeightUpdated(element) {
        super.elementHeightUpdated(element);
        // Scales the child up to height if necessary.
        // Otherwise, scales all the other children.
        let elemHeight = element.height()
        let thisHeight = this.height()-this.verticalPadding*2
        if(elemHeight < thisHeight)
            element.height(thisHeight)
        else if(elemHeight > thisHeight)
            this.height(elemHeight)
    }

    calculateImplicitHeight() {
        // Return the maximum height from all children
        return Math.max(this.minHeight, ...this.getLayoutElements().map(e => e.height()+this.verticalPadding*2));
    }

    calculateImplicitWidth() {
        return Math.max(
            0,
            this.getLayoutElements().reduce((a, b) => a + b.width() + this.gap, 0) - this.gap
        )
    }

    refreshLayout() {
        let accumulatedWidth = 0
        let height = this.height()-this.verticalPadding*2
        for(let element of this.getLayoutElements()) {
            element.x(accumulatedWidth)
            element.y(this.verticalPadding)
            element.height(height)
            accumulatedWidth += element.width() + this.gap
        }
    }
}
