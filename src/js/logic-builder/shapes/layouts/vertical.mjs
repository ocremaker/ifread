/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseLayout } from "./base"

export class VerticalLayout extends BaseLayout {
    constructor({ minWidth = 10, horizontalPadding = 0, name, ...options }) {
        super({
            name: "VerticalLayout",
            ...options
        })
        if(name)
            this.addName(name)
        this.minWidth = minWidth
        this.horizontalPadding = horizontalPadding
    }

    width(value) {
        if(value === undefined) // Getter
            return super.width()
        else if(super.width() !== value) {
            // Updates all children's heights to fill the layout
            super.width(value)
            this.refreshLayout()
        }
    }

    elementHeightUpdated(element) {
        super.elementHeightUpdated(element)
        this.refreshLayout()
    }

    elementWidthUpdated(element) {
        super.elementWidthUpdated(element)
        // Scales the child up to width if necessary.
        // Otherwise, scales all the other children.
        let elemWidth = element.width()
        let thisWidth = this.width()-this.horizontalPadding*2
        if(elemWidth < thisWidth)
            element.width(thisWidth)
        else if(elemWidth > thisWidth)
            this.width(elemWidth)
    }

    calculateImplicitWidth() {
        return Math.max(this.minWidth, ...this.getLayoutElements().map(e => e.width()+this.horizontalPadding*2))
    }

    calculateImplicitHeight() {
        return Math.max(
            0,
            this.getLayoutElements().reduce((a, b) => a + b.height() + this.gap, 0) - this.gap
        )
    }

    refreshLayout() {
        let accumulatedHeight = 0
        let width = this.width()-this.horizontalPadding*2
        for(let element of this.getLayoutElements()) {
            element.x(this.horizontalPadding)
            element.y(accumulatedHeight)
            if(element.width() !== width)
                element.width(width)
            accumulatedHeight += element.height() + this.gap
        }
        this.height(accumulatedHeight)
    }
}