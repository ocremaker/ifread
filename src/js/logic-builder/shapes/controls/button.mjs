/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Control } from "./base"
import { Text } from "../components/"
import { Rect, Tween } from "konva"
import { FontLoader, darkenColor } from "#utils"
import { setNodeCursor } from "#logic-builder/utils"

export class Button extends Control {
    constructor({ text, name, background = "#AAAAFF", color = "#000000", ...options }) {
        super({
            name: "Button",
            ...options
        })
        if(name)
            this.addName(name)
        this.add(...[
            this.bgNode = new Rect({
                width: 70,
                height: 24,
                cornerRadius: 12,
                fill: background,
                name: "Button-Background"
            }),
            this.textNode = new Text({
                width: 50,
                height: 24,
                fill: color,
                align: "center",
                verticalAlign: "middle",
                name: "Button-Text"
            })
        ])
        // Setting parameters
        this.text(text)
        setNodeCursor(this, "pointer")

        // Animations
        this.darkBg = darkenColor(background, .2)

        this.on("pointerover", () => this.playMouseOverAnimation("play"))
        this.on("pointerout", () => this.playMouseOverAnimation("reverse"))
    }

    playMouseOverAnimation(cb) {
        if(this.mouseOverTween === undefined)
            this.mouseOverTween = new Tween({
                node: this.bgNode,
                duration: .2,
                fill: this.darkBg
            })
        this.mouseOverTween[cb]()
    }

    text(value) {
        if(value === undefined)
            return this.textNode.text()
        let result = this.textNode.text(value)
        FontLoader.ensureLoaded("Inter", () => {
            // Properly resize everything
            this.textNode.width(Math.ceil(FontLoader.measureWidth("Inter", value)))
            this.bgNode.width(this.textNode.width() + this.bgNode.height()*2)
            this.centerChildren()
        })
        return result
    }

    background(value) {
        if(value === undefined)
            return this.bgNode.fill()
        return this.bgNode.fill(value)
    }

    color(value) {
        if(value === undefined)
            return this.textNode.fill()
        return this.textNode.fill(value)
    }

    click() {
        this.fire("pointerclick", { target: this })
    }
}