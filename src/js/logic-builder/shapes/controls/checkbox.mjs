/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Control } from "./base"
import { HorizontalLayout } from "../layouts/"
import { Text } from "../components/"
import { Path, Rect } from "konva"
import { FontLoader } from "#utils"
import { setNodeCursor } from "#logic-builder/utils"

const CHECKED_PATH = "M11 17l-5-5.299 1.399-1.43 3.574 3.736 6.572-7.007 1.455 1.403-8 8.597zm11-15v20h-20v-20h20zm2-2h-24v24h24v-24z"
const UNCHECKED_PATH = "M 22,2 V 22 H 2 V 2 Z M 24,0 H 0 v 24 h 24 z"

export class Checkbox extends Control {
    #checked = false

    constructor({ text, name, width, height, checked = false, background = "#FFFFFF00", ...options }) {
        super({
            name: "Checkbox",
            width, height,
            ...options
        })
        if(name)
            this.addName(name)
        this.color = "black"
        this.unsetColor = "#777777"
        this.#checked = checked

        this.add(...[
            this.bgNode = new Rect({
                height: 24,
                cornerRadius: 12,
                fill: background,
                name: "Checkbox-Background"
            }),
            this.container = new HorizontalLayout({
                minHeight: 16,
                height: 16
            }).add(...[
                this.checkPath = new Path({
                    width: 16,
                    height: 16,
                    scaleX: .6,
                    scaleY: .6,
                    name: "Checkbox-Box"
                }),
                this.textNode = new Text({
                    width: 50,
                    height: 16,
                    align: "center",
                    verticalAlign: "middle",
                    name: "Checkbox-Text"
                })
            ])
        ])
        this.text(text)
        this.checked(checked)
        this.container.width(this.container.calculateImplicitWidth())
        setNodeCursor(this, "pointer")
        this.on("pointerclick", () => this.checked(!this.checked()))
        this.on("widthChange", () => {
            this.bgNode.width(this.width() - 10)
            this.centerChildren()
        })
    }

    checked(value) {
        if(value === undefined)
            return this.#checked
        else {
            value = value === true
            if(this.#checked !== value) {
                this.#checked = value
                this.fire("checkChanged", { checked: this.#checked })
            }
            this.checkPath.data(this.#checked ? CHECKED_PATH : UNCHECKED_PATH)
            this.checkPath.fill(this.#checked ? this.color : this.unsetColor)
            this.textNode.fill(this.#checked ? this.color : this.unsetColor)
        }
    }

    text(value) {
        let result = this.textNode.text(value)
        if(value !== undefined)
            FontLoader.ensureLoaded("Inter", () => {
                this.textNode.width(Math.ceil(FontLoader.measureWidth("Inter", value)) + this.bgNode.height()/2)
                this.bgNode.width(this.container.width() + this.bgNode.height() * 1.5)
                this.centerChildren()
            })
        return result
    }

    background(value) {
        return this.bgNode.fill(value)
    }
}