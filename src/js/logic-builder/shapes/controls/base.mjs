/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Group } from "konva"

/**
 * A simple control container that centers each of its children.
 */
export class Control extends Group {
    constructor(options) {
        super(options);
        this.on("widthChange", () => this.centerChildren())
        this.on("childWidthChange", () => this.centerChildren())
        this.on("heightChange", () => this.centerChildren())
        this.on("childHeightChange", () => this.centerChildren())
    }

    add(...children) {
        for(let c of children)
            super.add(c)
        this.centerChildren()
    }

    centerChildren() {
        for(let c of this.getChildren()) {
            c.x((this.width() - c.width()) / 2)
            c.y((this.height() - c.height()) / 2)
        }
    }
}