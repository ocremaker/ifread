/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Rect } from "konva"
import { Text } from "./text"
import { StackLayout } from "../layouts/"
import { setNodeCursor } from "#logic-builder/utils"
import { EditText } from "./edittext"

export class Title extends StackLayout {
    constructor({ text, width, height, fill, cornerRadius, allowTitleEdit = true, ...options }) {
        let corners = [cornerRadius, cornerRadius, 0, 0]
        super({
            width,
            height,
            draggable: false,
            name: "Title",
            cornerRadius: corners,
            background: new Rect({
                width,
                height,
                fill,
                cornerRadius: corners,
                name: "Title-Background"
            }),
            ...options
        })
        this.add(...[
            this.edit = new EditText({
                width,
                height,
                text,
                singleLine: true,
                allowEdit: allowTitleEdit,
                align: "center",
                verticalAlign: "middle",
                name: "Title-Text"
            })
        ])
        setNodeCursor(this, "grab")
    }
}