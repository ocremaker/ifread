/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Text as T } from "./text"
import { parseHTML } from "../../../utils/index.mjs"

/**
 * Special type of text node that creates an editable field when double-clicked
 */
export class EditText extends T {
    constructor({ allowEdit = true, singleLine = false, ...options }) {
        super({
            name: "EditText",
            ...options
        })
        this.singleLine = singleLine
        this.lock = false
        if("name" in options)
            this.addName(options.name)
        if(allowEdit)
            this.on("pointerdblclick", (e) => {
                this.startEdit()
            })
    }

    startEdit() {
        if(!this.isEditing() && !this.lock) {
            this.fire("preTextEdit", { text: this.text() })
            this.createEditableTextNode()
            this.text(" ")
        }
    }

    isEditing() {
        return this.textEdit !== undefined
    }

    /**
     * Creates an editable input for the current node
     */
    createEditableTextNode() {
        const stageContainer = this.getStage().container()
        // Positioning the element
        let position = this.absolutePosition()
        let areaPosition = {
            x: stageContainer.offsetLeft + stageContainer.offsetParent.offsetLeft + position.x,
            y: stageContainer.offsetTop + stageContainer.offsetParent.offsetTop + position.y
        }
        if(!stageContainer.offsetParent.classList.contains("main-diagram")) {
            let popup = stageContainer.offsetParent.parentElement.shadowRoot.children[0]
            // Inner diagrams, which are transformed -50% to center.
            // We need to take into account the parent's offset.
            areaPosition = {
                x: areaPosition.x - popup.offsetWidth / 2,
                y: areaPosition.y - popup.offsetHeight / 2
            }
        }
        const scale = this.getStage().scaleX()
        const styles = [
            `top: ${areaPosition.y}px`,
            `left: ${areaPosition.x}px`,
            `width: ${this.width()*scale}px`,
            `height: ${this.height()*scale}px`,
            `line-height: ${this.height()*scale}px`,
        ]
        const textEditContainer = parseHTML(`<div class="text-edit" style="${styles.join("; ")}"></div>`)
        const stylesText = [
            `line-height: ${this.lineHeight()*scale}`,
            `font-size: ${this.fontSize()*scale}px`,
            `text-align: ${this.align()}`,
        ]
        const textEdit = parseHTML(`<p contenteditable="true" style="${stylesText.join("; ")}">
            ${this.singleLine ? this.text().split("\n")[0] : this.text()}
        </p>`)
        if(this.singleLine)
            textEdit.addEventListener("keydown", (e) => {
                if(e.key === "Enter")
                    this.validateEdit()
            })
        // Appending
        textEditContainer.append(textEdit)
        setTimeout(() => textEdit.focus(), 1)
        this.textEditContainer = textEditContainer
        this.textEdit = textEdit
        document.body.append(textEditContainer)
    }

    /**
     * Saves the edited data from the p node.
     */
    validateEdit() {
        if(this.isEditing()) {
            let text = this.textEdit.innerText.trim()
            if(this.singleLine)
                text = text.split("\n")[0]
            this.text(text)
            this.fill("black")
            document.body.removeChild(this.textEditContainer)
            this.textEdit = undefined
            this.textEditContainer = undefined
            this.fire("textEdited", { text })
        }
    }
}