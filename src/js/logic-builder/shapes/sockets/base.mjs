/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Circle, Layer, Group } from "konva"
import { containsFlag, flags } from "#utils"
import { Anchor, AnchorableGroup } from "#logic-builder/utils"
import { setNodeCursor } from "#logic-builder/utils"
import { SimpleArrow } from "../arrow/"
import { Link, COLORS } from "../../link"

/**
 * Socket models allow to declare sockets statically, which allows declarators to access them
 * ahead of time.
 */
export class SocketModel {
    /**
     * @param {string} ref - Unique (within the block) reference of the socket.
     * @param {Symbol} ioType - Either Socket.IO.SOURCE or/and Socket.IO.DESTINATION flags
     * @param {string} linkType - One of Types.Link
     * @param {boolean} allowMultiple - If true, allows this socket to be connected/to connect through multiple links.
     * @param options
     * @returns {*&{ioType, ref, allowMultiple: boolean, linkType}}
     */
    constructor({ ref, ioType, linkType, allowMultiple = false, ...options }) {
        return {
            ref,
            ioType,
            linkType,
            allowMultiple,
            ...options
        }
    }
}

@flags
class IO {
    static accessor DESTINATION
    static accessor SOURCE
}

export class Socket extends AnchorableGroup {
    // IO Types

    static IO = IO

    constructor({ ref, ioType, linkType, allowMultiple = false, ...options }) {
        const radius = 6
        const color = COLORS.get(linkType)
        super({
            ...options,
            draggable: true,
            width: radius * 2,
            height: radius * 2,
            drawBorder: true,
            name: "Socket"
        })
        if(ref === undefined)
            throw new Error("Sockets must have a unique reference ref!")
        this.addName(`Socket-[${ref}]`)
        this.ref = ref

        this.ioType = ioType
        this.color = color
        this.linkType = linkType
        this.allowMultiple = allowMultiple
        this.links = []

        // Adding children
        this.add(...[
            new Circle({
                x: radius, y: radius,
                radius: radius,
                fill: color,
                strokeEnabled: false
            })
        ])
        // Make sure we have a pointer if we're supposed to start from there.
        if(containsFlag(this.ioType, Socket.IO.SOURCE))
            setNodeCursor(this, "pointer")
        // Cancel drag to allow arrow starting
        this.on("dragstart", this.dragStart)
        this.on("dragmove", this.dragMove)
        this.on("drop dragend", this.dragEnd)
    }

    dragStart() {
        console.log("Drag started")
        this.basePosition = this.position()
        this.blockPosition = this.findAncestor(".Block").position()
        if(containsFlag(this.ioType, Socket.IO.SOURCE)) {
            // Start creating link.
            this.tempArrow = new SimpleArrow({
                fromSocket: this,
                targetX: this.x(),
                targetY: this.y(),
                fill: this.color,
                stroke: this.color
            })
            this.getLayer().add(this.tempArrow)
            if(!this.allowMultiple) // Destroy all previous links for the new one.
                this.removeLinks()
        }
    }

    dragMove(e) {
        this.position(this.basePosition)
        this.findAncestor(".Block").position(this.blockPosition)
        if(containsFlag(this.ioType, Socket.IO.SOURCE)) {
            let stage = this.getStage()
            stage.setPointersPositions(e.evt)
            this.tempArrow.target(stage.getOffsetPointerPosition())
        }
    }

    dragEnd(e) {
        if(containsFlag(this.ioType, Socket.IO.SOURCE)) {
            this.tempArrow.destroy()
            this.tempArrow = null
            // Getting current position on stage
            let stage = this.getStage()
            stage.setPointersPositions(e.evt)
            let pos = stage.getPointerPosition()
            let target = stage.getIntersection({ x: pos.x, y: pos.y })
            /** @type {Socket} */
            let socket = target?.findAncestor(".Socket", true) ?? null
            if(socket === null && target !== null) {
                // We might have targeted the block. Check if there is one socket which we can use for it.
                socket = target?.findAncestor(".Block", true).find(".Socket").filter(
                    s => s.linkType === this.linkType && containsFlag(s.ioType, Socket.IO.DESTINATION)
                )[0] ?? null
            }
            // Make sure the two sockets are compatible.
            if(socket !== null && containsFlag(socket.ioType, Socket.IO.DESTINATION) && socket.linkType === this.linkType) {
                console.log("Creating link...")
                this.createLink(socket)
            }
        }
    }

    /**
     * Destroys all links associated with this socket.
     */
    removeLinks() {
        /** @type {LogicBuilder} */
        let stage = this.getStage()
        for(let link of this.links) {
            stage?.removeLink(link)
        }
    }

    destroy() {
        this.removeLinks()
        super.destroy()
    }

    /**
     * Returns the current position of the socket relative to the Layer.
     * @returns {{x: number, y: number}}
     */
    getPositionInLayer() {
        let position = this.position()
        let element = this
        while(element.getParent() != null && !(element.getParent() instanceof Layer)) {
            element = element.getParent()
            position = {
                x: position.x + element.x(),
                y: position.y + element.y()
            }
        }
        return position
    }

    /**
     * Position toward which the socket points (aka where the link should start/end).
     * @returns {{x: number, y: number}}
     */
    outPosition() {
        let absPos = this.getPositionInLayer()
        if(containsFlag(this.anchor, Anchor.RIGHT))
            absPos.x += this.width()
        else if(!containsFlag(this.anchor, Anchor.LEFT)) // Not right or left
            absPos.x += this.width() / 2
        if(containsFlag(this.anchor, Anchor.BOTTOM))
            absPos.y += this.height()
        else if(!containsFlag(this.anchor, Anchor.TOP)) // Not top or bottom
            absPos.y += this.height() / 2
        return absPos
    }

    /**
     * Adds a new link between two socket
     * @param {Socket} targetSocket
     */
    createLink(targetSocket) {
        let arrow = new Link({
            fromSocket: this,
            toSocket: targetSocket,
            linkType: this.linkType
        })
        // Add to layer
        /** @type {LogicBuilder} */
        let stage = this.getStage()
        stage.addLink(arrow)
        return arrow
    }

    /**
     * Adds a link to the current socket
     * @param {Link} link
     */
    addLink(link) {
        // Resetting links
        if(!this.allowMultiple) {
            this.removeLinks()
            this.links = [link]
        } else
            this.links.push(link)
        let socket = this === link.fromSocket ? link.toSocket : link.fromSocket
        this.fire("linked", {
            socket: this, targetLink: link, targetBlock: socket.findAncestor(".Block")
        }, true)
    }

    /**
     * Removes a link from the current socket.
     * @param link
     */
    removeLink(link) {
        this.links.splice(this.links.indexOf(link), 1)
        let socket = this === link.fromSocket ? link.toSocket : link.fromSocket
        this.fire("unlinked", {
            socket: this, targetLink: link, targetBlock: socket.findAncestor(".Block")
        }, true)
    }
}