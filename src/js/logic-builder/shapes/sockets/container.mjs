/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { StackLayout } from "../layouts/"
import { Anchor } from "#logic-builder/utils"
import { Text } from "../components/"

export class SocketContainer extends StackLayout {
    constructor({ name, text = "", align = "left", sockets = [], ...options }) {
        super({
            name: "Socket-Container",
            ...options
        })
        if(name)
            this.addName(name)
        if(sockets.length > 0)
            this.add(...sockets)
        this.sockets = sockets
        // Update children
        Anchor.updatePositions(this.getChildren())
        this.on("widthChange", () => Anchor.updatePositions(this.getChildren()))
        this.on("heightChange", () => Anchor.updatePositions(this.getChildren()))
        // Add text node
        this.add(...[
            this.textNode = new Text({
                text,
                align,
                verticalAlign: "middle",
            })
        ])
    }

    text(value) {
        if(value === undefined)
            return this.textNode.text()
        else
            return this.textNode.text(value)
    }
}