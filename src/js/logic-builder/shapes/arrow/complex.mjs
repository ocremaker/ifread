/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Node, Arrow, Group } from "konva"
import { BaseLine } from "./base"
import { setNodeCursor, Anchor } from "#logic-builder/utils"
import { Socket } from "../"
import { containsFlag, generateUUID } from "#utils"

export class ComplexArrow extends Arrow {
    /**
     *
     * @param {Socket} fromSocket
     * @param {Socket} toSocket
     * @param options
     */
    constructor({ fromSocket, toSocket, ...options }) {
        super({
            name: "ComplexArrow",
            pointerAtEnding: true,
            pointerAtBeginning: false,
            fill: "black",
            stroke: "black",
            strokeWidth: 4,
            shadowEnabled: true,
            ...options
        })
        this.fromSocket = fromSocket
        this.toSocket = toSocket
        this.uuid = generateUUID()
        this.regenerate()
        setNodeCursor(this, "pointer")
        // Adding listeners
        this.startBlock = fromSocket.findAncestor(".Block")
        this.endBlock = toSocket.findAncestor(".Block")
        /** @type {Node[]} */
        this.listeners = [fromSocket, toSocket, this.startBlock, this.endBlock]
        for(let toListen of this.listeners) {
            toListen.on(`xChange.link${this.uuid} yChange.link${this.uuid}`, () => this.regenerate())
        }
    }

    destroy() {
        for(let toListen of this.listeners)
            toListen.off(`xChange.link${this.uuid} yChange.link${this.uuid}`)
        super.destroy()
    }

    /**
     * Regenerates the arrow if new positions for start or end sockets are set.
     */
    regenerate() {
        let pos1 = this.fromSocket.outPosition()
        let pos2 = this.toSocket.outPosition()
        this.setAttrs({
            x: Math.min(pos1.x, pos2.x),
            y: Math.min(pos1.y, pos2.y)
        })
        this.generatePoints()
    }

    /**
     * Generates a BaseLine (with a limitation) for a given socket.
     * @param {Socket} socket
     * @returns {BaseLine}
     */
    lineOfSocket(socket) {
        let pos = socket.outPosition()
        let isHorizontal = (socket.anchor === Anchor.LEFT) || (socket.anchor === Anchor.RIGHT)
        let goAbove = (containsFlag(socket.anchor, Anchor.RIGHT) || containsFlag(socket.anchor, Anchor.BOTTOM)) &&
                                !containsFlag(socket.anchor, Anchor.TOP)
        let startAt = isHorizontal ? pos.x : pos.y
        return new BaseLine({
            direction: isHorizontal ? BaseLine.HORIZONTAL : BaseLine.VERTICAL,
            value: isHorizontal ? pos.y : pos.x,
            startAt: startAt,
            above: goAbove ? startAt : -Infinity,
            under: !goAbove ? startAt : Infinity,
            isArrow: containsFlag(socket.ioType, Socket.IO.DESTINATION)
        })
    }

    /**
     * Generates a set of points from the generated lines.
     */
    generatePoints() {
        let points = this.generateLines().map(l => l.getPoints())
        let pts = [points[0].startX - this.x(), points[0].startY - this.y()]
        for(let point of points)
            pts.push(point.targetX - this.x(), point.targetY - this.y())
        this.points(pts)
    }

    /**
     * Generates a set of lines to represent the right angle arrow.
     * @returns {BaseLine[]}
     */
    generateLines() {
        let fromStart = [this.lineOfSocket(this.fromSocket)]
        let fromEnd = [this.lineOfSocket(this.toSocket)]
        let afterEnd = { value: fromEnd[0].startAt }
        let processStart = true
        // Flip generating one at the start, then one at the end, until two lines intersect
        while(!fromStart.at(-1).canIntersect(fromEnd.at(-1))) {
            let fs1 = fromStart.at(-1)
            let fe1 = fromEnd.at(-1)
            if(processStart)
                fromStart.push(fs1.createNextOrthogonal(fe1))
            else
                fromEnd.push(fe1.createNextOrthogonal(fs1))
            if(fromStart.length + fromEnd.length > 20) {
                console.log(fromStart.map(l => l.getPoints()), fromEnd.map(l => l.getPoints()))
                throw new Error("Too many generations.")
            }
            processStart = !processStart
        }
        // Great! Now that both ends match one another, reverse the end tree to put it at the start.
        for(let i = fromEnd.length - 1; i >= 0; i--) {
            let line = fromEnd[i]
            let previous = fromStart.at(-1)
            line.startAt = previous.value
            line.next = null
            previous.next = line
            fromStart.push(line)
        }
        // Put the finishing touch on the last semi line.
        fromStart.at(-1).next = afterEnd
        // Generate points for Konva
        // for(let line of fromStart)
        //     line.updateKonvaLinePoints()
        // console.log("Generated lines", this.uuid, fromStart.map(l => l.getPoints()))
        return fromStart
    }
}