/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Arrow } from "konva"
import { containsFlag } from "#utils"
import { Socket } from "../sockets/"

export class SimpleArrow extends Arrow {
    /**
     * Simple arrow class meant to show the user they're linking something before the link is fully established.
     * @param {Socket} fromSocket
     * @param {number} targetX
     * @param {number} targetY
     * @param options
     */
    constructor({ fromSocket, targetX, targetY, ...options }) {
        super({
            name: "SimpleArrow",
            pointerAtEnding: true,
            pointerAtBeginning: containsFlag(fromSocket.ioType, Socket.IO.DESTINATION), // Two-way sockets
            fill: "black",
            stroke: "black",
            strokeWidth: 4,
            ...options
        })
        this.fromSocket = fromSocket
        this.targetX = targetX
        this.targetY = targetY
        this.regenerate()
    }

    target({ x, y }) {
        this.targetX = x
        this.targetY = y
        this.regenerate()
    }

    regenerate() {
        let pos1 = this.fromSocket.outPosition()
        let x = Math.min(pos1.x, this.targetX)
        let y = Math.min(pos1.y, this.targetY)
        this.x(x)
        this.y(y)
        this.width(Math.abs(pos1.x - this.targetX))
        this.height(Math.abs(pos1.y - this.targetY))
        this.points([pos1.x - x, pos1.y - y, this.targetX - x, this.targetY - y])
    }
}