/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Arrow } from "konva"

// When a line's length is flexible (when is not contrained by external factors), this should be its minimum width
const MIN_FLEX_SIZE = 40

export class BaseLine extends Arrow {
    static HORIZONTAL = Symbol("HORIZONTAL")
    static VERTICAL = Symbol("VERTICAL")

    /**
     *
     * @param {Symbol} direction - Direction of the line
     * @param {number} value - Value used to define the line in a given direction (e.g. 4 for y=4)
     * @param {number} above - Topmost value at which the line is defined
     * @param {number} under - Lowmost value at which the line is defined
     * @param {boolean} isArrow - If true, the ending of the line will be an arrow.
     * @param {number} startAt - Value to start creating the line at.
     * @param {BaseLine|null} next - Optional value of the next baseLine
     * @param options
     */
    constructor({
        direction, value,
        above = -Infinity, under = Infinity,
        isArrow = false,
        startAt = null,
        next = null,
        ...options
    }) {
        super({
            pointerWidth: isArrow ? 10 : 0,
            pointerLength: isArrow ? 10 : 0,
            pointerAtEnding: isArrow,
            fill: 'black',
            stroke: 'black',
            strokeWidth: 4,
            ...options
        })
        this.direction = direction
        this.value = value
        this.under = under
        this.above = above
        if(this.startAt === null)
            throw new Error("Cannot have a line that does not have a starting point.")
        this.startAt = startAt
        this.next = next
    }

    /**
     * Returns true if this line can intersect another line..
     * @param {BaseLine} line
     * @returns {boolean}
     */
    canIntersect(line) {
        return this.direction !== line.direction && this.definedAt(line.value) && line.definedAt(this.value)
    }

    /**
     * Checks if the current line could intersect another line orthogonally to it if it were placed at a given value.
     * @param {number} oppositeDirectionValue
     * @returns {boolean}
     */
    definedAt(oppositeDirectionValue) {
        return (oppositeDirectionValue < this.under) && (oppositeDirectionValue > this.above)
    }

    /**
     * Creates a new BaseLine that will be the next one to the current one
     * @param {BaseLine} triesConnectTo - Line toward which the next line should aim.
     * @returns {BaseLine}
     */
    createNextOrthogonal(triesConnectTo) {
        let value = this.startAt + (this.under === this.startAt ? -MIN_FLEX_SIZE : MIN_FLEX_SIZE)
        if(triesConnectTo.direction === this.direction)
            // Set the line to aim be created at the middle of the two semi lines
            value = Math.max(this.above + MIN_FLEX_SIZE / 2, Math.min(this.under - MIN_FLEX_SIZE/2,
                (triesConnectTo.startAt + this.startAt) / 2
            ))
        this.next = new BaseLine({
            direction: this.direction === BaseLine.HORIZONTAL ? BaseLine.VERTICAL : BaseLine.HORIZONTAL,
            value,
            startAt: this.value
        })
        return this.next
    }

    getPoints() {
        let attrs
        if(this.direction === BaseLine.HORIZONTAL)
            attrs = {
                startX: this.startAt,
                startY: this.value,
                targetX: this.next?.value,
                targetY: this.value
            }
        else
            attrs = {
                startX: this.value,
                startY: this.startAt,
                targetX: this.value,
                targetY: this.next?.value
            }
        return attrs
    }

    updateKonvaLinePoints() {
        let attrs = this.getPoints()
        this.setAttrs({
            x: Math.min(attrs.startX, attrs.targetX),
            y: Math.min(attrs.startY, attrs.targetY),
            width: Math.abs(attrs.targetX - attrs.startX)+1,
            height: Math.abs(attrs.targetY - attrs.startY)+1,
        })
        this.points([
            attrs.startX-this.x(),
            attrs.startY-this.y(),
            attrs.targetX-this.x(),
            attrs.targetY-this.y()
        ])
    }

    toString() {
        return `{${this.direction.description} line ${this.value} defined in ]${this.above};${this.under}[ }`
    }
}