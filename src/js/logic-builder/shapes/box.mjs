/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { StackLayout, VerticalLayout } from "./layouts/"
import { Title, Text } from "./components/"
import { FocusCaptor, setNodeCursor } from "#logic-builder/utils"
import { darkenColor } from "#utils";
import { Rect, Tween } from "konva"

@FocusCaptor
class FocusStackLayout extends StackLayout {}

export class Box extends FocusStackLayout {
    constructor({
        name,
        title = "",
        titleBackground,
        background,
        border,
        width,
        height,
        titleHeight = 40,
        ...options
    }) {
        super({
            width, height,
            draggable: true,
            name: "Box",
            background: new Rect({
                width, height,
                fill: background,
                stroke: border,
                strokeWidth: 2,
                cornerRadius: 10,
                name: "Box-Background"
            }),
            ...options
        })
        if(name !== undefined)
            this.addName(name)
        this.border = border
        let gradLight = darkenColor(border, -.2)
        let gradDark = darkenColor(border, .1)
        this.borderGradient = [0, gradLight, .5, gradDark, 1, gradLight]
        this.defaultWidth = width
        // Add children
        this.add(
            this.container = new VerticalLayout({
                width, height,
                gap: 5,
                minWidth: width,
                offsetY: -1,
                horizontalPadding: 1,
                name: "Box-Properties"
            }).add(...[
                this.titleNode = new Title({
                    width: width,
                    height: titleHeight,
                    text: title,
                    fill: titleBackground,
                    cornerRadius: 9
                })
            ])
        )
        // Resize immediately.
        this.container.refreshLayout()

        setNodeCursor(this, "default")

        // Handle changing title
        this.on("dragstart unfocus contextmenu", () => {
            this.titleNode.edit.validateEdit()
        })
    }

    applySelectedStyle() {
        let bg = this.background
        let start = { x: 0, y: 0 }
        let end = { x: bg.width(), y: bg.height() }
        bg.strokeWidth(6)
        bg.strokeLinearGradientStartPoint(start)
        bg.strokeLinearGradientEndPoint(end)
        bg.strokeLinearGradientColorStops(this.borderGradient)
        this.container.horizontalPadding = 3
        this.offsetX(this.container.getChildren().length === 1 ? -2 : 2)
        this.offsetY(2)
        this.container.offsetY(-3)
        this.container.width(this.defaultWidth-4)
        this.container.refreshLayout()
        this.background.height(this.background.height()+3)
    }

    applyUnselectedStyle() {
        this.background.strokeWidth(2)
        this.background.stroke(this.border)
        this.background.strokeLinearGradientColorStops([0, this.border, 1, this.border])
        this.container.horizontalPadding = 1
        this.offsetX(0)
        this.offsetY(0)
        this.container.offsetY(-1)
        this.container.width(this.defaultWidth)
        this.container.refreshLayout()
        this.background.height(this.background.height())
    }
}