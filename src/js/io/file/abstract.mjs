/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export class AbstractFileInterface {
    /**
     * Returns a bool to indicate whether this interface is available at the current time.
     * @returns {boolean}
     */
    static supported() {
        throw new Error("Not implemented.");
    }

    /**
     * Returns the human-readable name of the interface.
     * @returns {string}
     */
    static get interfaceName() { throw new Error("Not implemented"); }

    /**
     * Returns the opened file name, null otherwise.
     * @returns {string|null}
     */
    get fileName() { throw new Error("Not implemented"); }


    /**
     * Sends the user an open dialog to open a file, and resolves the opened file.
     * @returns {Promise<File>}
     */
    showOpenDialog() {
        throw new Error("Not implemented.");
    }

    /**
     * Called to auto save the file onto its medium, on top of being saved into the Origin Private File System.
     * Should not provide any user interaction.
     * @param {string} contents
     * @returns {Promise}
     */
    autosave(contents) {
        throw new Error("Not implemented.");
    }

    /**
     * Called when the user interacts with the save button (or uses Ctrl+S).
     * Can provide user interaction if necessary.
     * @param {string} contents
     * @returns {Promise}
     */
    save(contents) {
        throw new Error("Not implemented.");
    }

    /**
     * Resets the interface when the loaded file is cleared.
     */
    reset() {
        throw new Error("Not implemented.");
    }
}