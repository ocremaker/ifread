/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AbstractFileInterface } from "./abstract.mjs"

export class FSAccessFileInterface extends AbstractFileInterface {
    /**
     * Returns a bool to indicate whether this interface is available at the current time.
     * @returns {boolean}
     */
    static supported() {
        return !!window.showOpenFilePicker
    }

    /**
     * Returns the human-readable name of the interface.
     * @returns {string}
     */
    static get interfaceName() {
        return "Device (direct)"
    }

    /**
     * Opened file handle
     * @type {FileSystemFileHandle|null}
     */
    #fileHandle = null
    #fileName = null

    /**
     * Returns the opened file name, null otherwise.
     * @returns {string|null}
     */
    get fileName() {
        return this.#fileName
    }

    async _getWritePermission() {
        let gotPermission = (await this.#fileHandle.queryPermission({ mode: "readwrite" })) === "granted"
        if(!gotPermission)
            gotPermission = (await this.#fileHandle.requestPermission({ mode: "readwrite" })) === "granted"
        return gotPermission
    }

    /**
     * Sends the user an open dialog to open a file, and resolves the opened file.
     * @returns {Promise<File>}
     */
    async showOpenDialog() {
        [this.#fileHandle] = await window.showOpenFilePicker({
            id: "ifread-open-dialog",
            multiple: false,
            startIn: "documents",
            types: [{
                description: "Ifread Documents",
                accept: {
                    "application/x-ifread": [".ifread"]
                }
            }]
        })
        if(!await this._getWritePermission())
            throw new Error("Permission denied.")
        const file = await this.#fileHandle.getFile()
        this.#fileName = file.name
        return file
    }

    /**
     * Called to auto save the file onto its medium, on top of being saved into the Origin Private File System.
     * Should not provide any user interaction.
     * @param {string} contents
     * @returns {Promise}
     */
    async autosave(contents) {
        if(this.#fileHandle !== null) {
            await this.save(contents)
        }
    }

    /**
     * Called when the user interacts with the save button (or uses Ctrl+S).
     * Can provide user interaction if necessary.
     * @param {string} contents
     * @returns {Promise}
     */
    async save(contents) {
        if(this.#fileHandle === null) {
            this.#fileHandle = await window.showSaveFilePicker({
                id: "ifread-open-dialog",
                excludeAcceptAllOption: true,
                startIn: "documents",
                types: [{
                    description: "Ifread Documents",
                    accept: {
                        "application/x-ifread": [".ifread"]
                    }
                }]
            })
        }
        if(!await this._getWritePermission())
            throw new Error("Permission denied.")
        const write = await this.#fileHandle.createWritable({
            keepExistingData: false,
            mode: "siloed"
        })
        await write.write(contents)
        await write.close()
    }
}