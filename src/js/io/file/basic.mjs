/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AbstractFileInterface } from "./abstract.mjs"
import { parseHTML } from "../../utils/index.mjs"

export class BasicFileInterface extends AbstractFileInterface {
    /**
     * Returns a bool to indicate whether this interface is available at the current time.
     * @returns {boolean}
     */
    static supported() {
        return HTMLInputElement !== undefined
    }

    /**
     * Returns the human-readable name of the interface.
     * @returns {string}
     */
    static get interfaceName() { return "Device (load/download)" }

    #fileName = null

    /**
     * Returns the opened file name, null otherwise.
     * @returns {string|null}
     */
    get fileName() {
        return this.#fileName
    }

    /**
     * Sends the user an open dialog to open a file, and resolves the opened file.
     * @returns {Promise<File>}
     */
    showOpenDialog() {
        return new Promise((resolve, reject) => {
            /** @type {HTMLInputElement} */
            const input = parseHTML('<input type="file" accept=".ifread,application/x-ifread">')
            input.addEventListener("change", e => {
                if(input.files.length === 1) {
                    const file = input.files[0]
                    this.#fileName = file.name
                    resolve(file)
                } else
                    reject(new Error(`${input.files.length} files passed, not one.`))
            })
            input.click()
        })
    }

    /**
     * Called to auto save the file onto its medium, on top of being saved into the Origin Private File System.
     * Should not provide any user interaction.
     * @param {string} contents
     * @returns {Promise}
     */
    async autosave(contents) {
        // No implementation of autosave here. Fall back on the browser autosave.
    }

    /**
     * Called when the user interacts with the save button (or uses Ctrl+S).
     * Can provide user interaction if necessary.
     * @param {string} contents
     * @returns {Promise}
     */
    async save(contents) {
        const blob = new Blob([contents], { type: "application/x-ifread" })
        const link = document.createElement("a")
        link.href = URL.createObjectURL(blob)
        link.download = this.fileName ?? "fiction.ifread"
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
    }

    /**
     * Resets the interface when the loaded file is cleared.
     */
    reset() {
        this.#fileName = null
    }
}