/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { LogicBuilder } from "#logic-builder"
import { StartBlock } from "#logic-builder/blocks"
import {
    Chapter,
    CSSExportFormatSettings,
    CSSExportSelector,
    ExportFormatsSettings,
    IfreadSaveFile
} from "#abstract/structures"
import { exportCurrentDiagram } from "./save/index.mjs"
import { generateUUID } from "../utils/index.mjs"
import { INTERFACES } from "./file/index.mjs"
import { loadChapter } from "./load/index.mjs"

/**
 * Resets the diagram to empty.
 */
export function reinitialize() {

    /** @type {Record.<string, ({insert: string|object}|{delete: number}|{retain: number})[]>} */
    window.globals.texts = {}

    /** @type {Record.<string, Zone>} */
    window.globals.zones = {}

    /** @type {Record.<string, LogicBuilder>} */
    window.globals.builders = {
        main: new LogicBuilder({
            container: document.createElement("div"),
            uuid: "main",
            width: 1,
            height: 1
        })
    }

    // Adding canvas
    document.querySelector("logic-builder-canvas.main-diagram")?.remove()
    const builder = document.createElement("logic-builder-canvas")
    builder.builderId = "main"
    builder.nodeTypesList = "main"
    builder.classList.add("main-diagram")
    const main = document.body.querySelector("main")
    main.insertBefore(builder, main.querySelector(".chapters"))
}

/**
 * Creates a new diagram.
 */
export function createNewFile() {
    reinitialize()

    window.globals.currentChapterVersion = {}
    /** @type {CompiledChapterVersion} */
    window.globals.currentChapterVersion.previousVersionCompilation = exportCurrentDiagram()

    window.globals.save = {}
    /** @type {AbstractFileInterface} */
    window.globals.save.interface = INTERFACES.filesystem
    window.globals.save.currentChapterIndex = 0
    window.globals.save.fileData = new IfreadSaveFile({
        title: "New document",
        firstCreated: Date.now(),
        lastModified: Date.now(),
        chapters: [
            new Chapter({
                uuid: generateUUID(),
                title: "Chapter 1",
                currentAbstract: {}, // TODO
                currentVersion: window.globals.sessionUUID,
                versions: []
            })
        ],
        lastExportFormatSettings: new ExportFormatsSettings({
            css: new CSSExportFormatSettings({
                selector: CSSExportSelector.SIBLINGS
            })
        })
    })

    // Initialize
    window.globals.builders.main.addNewBlock(StartBlock, { x: 0, y: 0 })
}


/**
 *
 * @param {IfreadSaveFile} ifreadFile
 * @param {string} title
 */
export function loadFile(ifreadFile) {
    window.globals.save = {}
    window.globals.save.interface = INTERFACES.filesystem
    window.globals.save.currentChapterIndex = 0
    window.globals.save.fileData = ifreadFile

    loadChapter(0)
}