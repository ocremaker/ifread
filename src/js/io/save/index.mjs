/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DataContainer, serialize, unserialize } from "#abstract"
import { ChapterVersion, CompiledChapterVersion, createDifferenceBetweenVersions } from "#abstract/structures"
import { exportZones } from "./zone.mjs"
import { exportBuilders } from "./builder.mjs"
import { exportTexts } from "./text.mjs"

/**
 * Exports the current diagrams into a compiled chapter version.
 * @return {CompiledChapterVersion}
 */
export function exportCurrentDiagram() {
    const {zones, zonesDelegates} = exportZones()
    const builders = exportBuilders(zonesDelegates)
    const texts = exportTexts()
    const compiled = new CompiledChapterVersion({
        uuid: window.globals.sessionUUID,
        author: "TODO",
        lastModified: Date.now(),
        previousVersion: null,
        texts,
        zones,
        builders
    })
    // Duplicate referenced objects.
    return unserialize(serialize(compiled, CompiledChapterVersion, DataContainer.NONE), CompiledChapterVersion, DataContainer.NONE)
}


/**
 * Exports the current diagrams into a diff set compared to the current version.
 * @return {ChapterVersion}
 */
function exportChapterVersion() {
    const newVersion = exportCurrentDiagram()
    const compareAgainst = window.globals.currentChapterVersion.previousVersionCompilation
    return createDifferenceBetweenVersions(compareAgainst, newVersion)
}

/**
 * Exports the current diagram into a chapter version, and saves it into the opened file data.
 */
export function saveChapterVersionToOpenedFile() {
    const chapterVersion = exportChapterVersion()
    const file = window.globals.save.fileData
    const chapterIdx = window.globals.save.currentChapterIndex
    const chapterVersionUUID = window.globals.sessionUUID
    const chapter = file.chapters[chapterIdx]
    const chapterVersionIdx = chapter.versions.findIndex(v => v.uuid === chapterVersionUUID)
    if(chapterVersionIdx === -1)
        chapter.versions.push(chapterVersion)
    else
        chapter.versions[chapterVersionIdx] = chapterVersion
    chapter.currentVersion = chapterVersionUUID
}