/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AbstractBlock, AbstractLogicBuilder } from "#abstract/structures"
import { BaseChoice } from "../../logic-builder/blocks/index.mjs"

/**
 *
 * @param {string} builderUUID
 * @param {AbstractZoneDelegate[]} zonesDelegates
 * @return {AbstractLogicBuilder}
 * @private
 */
export function _exportBuilder(builderUUID, zonesDelegates) {
    const abstractBlocks = []
    const abstractLinks = []
    const parentBuilderOf = []
    const builder = window.globals.builders[builderUUID]

    for(const block of [...builder.listBlocks(), ...builder.listNotes()]) {
        abstractBlocks.push(new AbstractBlock({
            uuid: block.uuid,
            x: block.x(),
            y: block.y(),
            data: block.blockData
        }))

        if(block instanceof BaseChoice) {
            parentBuilderOf.push(block.innerBuilderId)
        }
    }

    for(const link of builder.listLinks()) {
        abstractLinks.push(link.blockData)
    }

    return new AbstractLogicBuilder({
        uuid: builderUUID,
        childBuilders: parentBuilderOf,
        zonesDelegates: zonesDelegates,
        blocks: abstractBlocks,
        links: abstractLinks
    })
}

/**
 * Exports all builders given exported zone delegates.
 * @param {Map<string, AbstractZoneDelegate[]>} zonesDelegates
 * @return {AbstractLogicBuilder[]}
 */
export function exportBuilders(zonesDelegates) {
    const builders = []
    for(const uuid of Object.keys(window.globals.builders))
        builders.push(_exportBuilder(uuid, zonesDelegates.get(uuid) ?? []))
    return builders
}