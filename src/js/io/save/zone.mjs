/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AbstractZoneDelegate, AbstractZoneSquare } from "#abstract/structures"

/**
 *
 * @param {ZonePerBuilderDelegate} zoneDelegate
 * @returns {AbstractZoneDelegate}
 * @private
 */
function _exportZoneDelegate(zoneDelegate) {
    const squares = []
    for(const square of zoneDelegate.squaresGroup.getChildren()) {
        squares.push(new AbstractZoneSquare({
            x: square.x() / square.width(),
            y: square.y() / square.width(),
        }))
    }
    return new AbstractZoneDelegate({
        uuid: zoneDelegate.zone.uuid,
        squares
    })
}

/**
 * Exports all zones from window.globals.zones and window.globals.builders.
 * @return {{zones: AbstractZone[], zonesDelegates: Map<string, AbstractZoneDelegate[]>}}
 */
export function exportZones() {
    /** @type {AbstractZone[]} */
    const abstractZones = []
    const abstractZonesDelegatesMap = new Map(Object.keys(window.globals.builders).map(uuid => [uuid, []]))

    for(const zone of Object.values(window.globals.zones)) {
        abstractZones.push(zone.blockData)
        for(const [builderUUID, delegate] of zone.delegates) {
            abstractZonesDelegatesMap.get(builderUUID).push(_exportZoneDelegate(delegate))
        }
    }

    return { zones: abstractZones, zonesDelegates: abstractZonesDelegatesMap }
}