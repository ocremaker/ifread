/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Link, LogicBuilder } from "#logic-builder"
import { DATA_TO_BLOCKTYPE, Note } from "#logic-builder/blocks/"
import { loadZoneDelegates, loadZones } from "./zone.mjs"

/**
 *
 * @param {LogicBuilder} builder
 * @param {AbstractBlock[]} blocks
 */
function loadBlocks(builder, blocks) {
    for(const abstractBlock of blocks) {
        const blockType = DATA_TO_BLOCKTYPE.get(abstractBlock.data.constructor)
        const block = new blockType({
            uuid: abstractBlock.uuid,
            blockData: abstractBlock.data
        })
        block.x(abstractBlock.x)
        block.y(abstractBlock.y)
        builder.addBlock(block)
    }
}
/**
 *
 * @param {LogicBuilder} builder
 * @param {AbstractLink[]} links
 */
function loadLinks(builder, links) {
    for(const abstractLink of links) {
        const blocks = builder.listBlocks()
        const startBlock = blocks.find(b => b.uuid === abstractLink.from.block)
        const endBlock = blocks.find(b => b.uuid === abstractLink.to.block)
        const fromSocket = startBlock.findOne(`.Socket-[${abstractLink.from.ref}]`)
        const toSocket = endBlock.findOne(`.Socket-[${abstractLink.to.ref}]`)
        const link = new Link({
            blockData: abstractLink,
            linkType: abstractLink.linkType,
            fromSocket,
            toSocket,
        })
        builder.addLink(link)
        link.updateAlertsWithAssignments()
    }
}

/**
 *
 * @param {AbstractLogicBuilder[]} abstractBuilders
 */
export function loadBuilders(abstractBuilders) {
    const sortedBuilders = abstractBuilders.sort(
        (a, b) => a.childBuilders.includes(b.uuid) ? 1 : -1
    )

    // Create builder instances.
    for(const abstractBuilder of sortedBuilders) {
        let builder = window.globals.builders[abstractBuilder.uuid]
        if(!builder) {
            builder = new LogicBuilder({
                uuid: abstractBuilder.uuid,
                container: document.createElement("div"),
                width: 1,
                height: 1
            })
            window.globals.builders[builder.uuid] = builder
        }

        loadZoneDelegates(builder, abstractBuilder.zonesDelegates)
        loadBlocks(builder, abstractBuilder.blocks)
        loadLinks(builder, abstractBuilder.links)
    }
}