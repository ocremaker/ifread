/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { reinitialize } from "../reinitialize.mjs"
import { loadZones } from "./zone.mjs"
import { loadBuilders } from "./builder.mjs"
import { loadTexts } from "./texts.mjs"
import {
    AbstractZone,
    ChapterVersion,
    compileSequentialVersions,
    QuillDeltaWithID,
    VersionDiff
} from "#abstract/structures/"
import { generateUUID } from "#utils"

const EMPTY_CHAPTER_VERSION = new ChapterVersion({
    uuid: generateUUID(),
    author: "NONE",
    lastModified: -1,
    previousVersion: null,
    texts: new (VersionDiff(QuillDeltaWithID))({}),
    zones: new (VersionDiff(AbstractZone))({}),
    builders: []
})

/**
 * Loads a compiled chapter into the builders.
 * @param {CompiledChapterVersion} compiledChapterVersion
 */
export function loadCompiledChapterVersion(compiledChapterVersion) {
    reinitialize()

    window.globals.currentChapterVersion = {}
    /** @type {CompiledChapterVersion} */
    window.globals.currentChapterVersion.previousVersionCompilation = compiledChapterVersion

    loadTexts(compiledChapterVersion.texts)
    loadZones(compiledChapterVersion.zones)
    loadBuilders(compiledChapterVersion.builders)
}

/**
 * Loads the latest chapter version for given chapter index.
 * @param {number} chapterIndex
 */
export function loadChapter(chapterIndex) {
    reinitialize()

    const chapter = window.globals.save.fileData.chapters[chapterIndex]
    const latestVersion = chapter.versions.find(v => v.uuid === chapter.currentVersion)
    const chapterVersions = chapter.compileVersionAncestryFor(latestVersion)
    const compiledChapterVersion = compileSequentialVersions(chapterVersions)

    window.globals.currentChapterVersion = {}
    /** @type {CompiledChapterVersion} */
    let previousCompiledVersion = compiledChapterVersion
    if(latestVersion.uuid === window.globals.sessionUUID) { // Loaded in the same session.
        let previousVersion = chapter.versions.find(v => v.uuid === latestVersion.previousVersion)
        if(previousVersion === undefined) // Create dummy chapter version.
            previousVersion = EMPTY_CHAPTER_VERSION
        const previousVersions = chapter.compileVersionAncestryFor(previousVersion)
        previousCompiledVersion = compileSequentialVersions(previousVersions)
    }
    window.globals.currentChapterVersion.previousVersionCompilation = previousCompiledVersion

    loadTexts(compiledChapterVersion.texts)
    loadZones(compiledChapterVersion.zones)
    loadBuilders(compiledChapterVersion.builders)
}
