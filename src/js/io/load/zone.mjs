/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Zone } from "#logic-builder/zones"

/**
 * Loads a list of abstract zones into zones.
 * @param {AbstractZone[]} abstractZones
 */
export function loadZones(abstractZones) {
    for(const abstractZone of abstractZones)
        Zone.register(new Zone({
            blockData: abstractZone
        }))
}

/**
 *
 * @param {LogicBuilder} builder
 * @param {AbstractZoneDelegate[]} abstractZoneDelegates
 */
export function loadZoneDelegates(builder, abstractZoneDelegates) {
    const squareSize = builder.grid.snap
    for(const abstractZoneDelegate of abstractZoneDelegates) {
        const zone = window.globals.zones[abstractZoneDelegate.uuid]
        const delegate = zone.createDelegate(builder)
        builder.addZoneDelegate(delegate)
        for(const asquare of abstractZoneDelegate.squares) {
            delegate.addSquare({
                x: asquare.x * squareSize,
                y: asquare.y * squareSize
            })
        }
    }
}