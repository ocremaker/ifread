/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ExportType, SerializableStructure } from "../base.mjs"
import { SERIALIZERS } from "./basic.mjs"
import { Enum } from "../types/index.mjs"
import { DataContainer } from "../container.mjs"

/**
 * Serializes a given object into a JSONable object.
 * @param {Set|Array|SerializableStructure|string|number|boolean|null} data
 * @param {any} type
 * @param {string} container - If set, will serialize the container as an array whose data is of given type.
 * @param {boolean} allowNull - If true, allows the value to be null.
 * @return {{"@type"}|string|number|boolean|null}
 */
export function serialize(data, type, container, allowNull = false) {
    let serialized
    // Check for array or sets
    if(!DataContainer.contains(container)) {
        throw new Error(`Unknown data container ${container}.`)
    } else if(!DataContainer.isContainedWithin(container, data)) {
        throw new Error(`Data passed not contained within container type ${container}.`)
    } else if(container !== DataContainer.NONE) {
        serialized = Array.from(DataContainer.map(container, data, (d) => serialize(d, type, DataContainer.NONE, allowNull)))
    } else if(data === null) {
        if(allowNull)
            return null
        else
            throw new Error(`Data must be a ${type.name ?? type.toString()} and not null.`)
    } else if(type.prototype instanceof SerializableStructure) {
        // Check for structures
        if(type.exportType === ExportType.PARTIAL)
            serialized = _serializePartial(data, type)
        else
            serialized = _serializeStructure(data)
    } else if(type.prototype instanceof Enum) {
        // Check for Enums
        if(typeof data !== "string" || !type.contains(data))
            throw new Error(`Invalid enum ${type.name} value ${data}.`)
        serialized = data
    } else if(SERIALIZERS.has(type)) {
        // Check for basic types
        serialized = SERIALIZERS.get(type)(data)
    } else
        throw new Error(`Unrecognized type ${type}.`)
    return serialized
}

/**
 * Serializes a given object into a JSONable object.
 * @param {SerializableStructure} struct
 * @return {{"@type"}}
 */
function _serializeStructure(struct) {
    const exprt = struct.constructor.exportType === ExportType.WITH_TYPE ? { "@type": struct.constructor.name } : {}
    for(const property of struct.constructor.properties) {
        exprt[property.name] = serialize(struct[property.name], property.type, property.container, property.allowNull)
    }
    return exprt
}

/**
 * Serializes the defined properties of a given object into a JSONable one.
 * @param {SerializableStructure} struct
 * @param {typeof SerializableStructure} type
 * @private
 */
function _serializePartial(struct, type) {
    const exprt = {}
    for(const property of type.properties) {
        if(struct[property.name] !== undefined)
            exprt[property.name] = serialize(struct[property.name], property.type, property.container, property.allowNull)
    }
    return exprt
}