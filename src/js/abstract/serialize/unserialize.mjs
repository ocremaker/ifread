/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { COLLECTED_CLASSES, ExportType, SerializableStructure } from "../base.mjs"
import { IMPORTERS } from "./basic.mjs"
import { Enum } from "../types"
import { DataContainer } from "../container.mjs"


/**
 * Unserializes a serialized structure, primitive type, or array of any of those.
 * @param {object|string|number|boolean|null} data
 * @param {any} type
 * @param {string} container
 * @param {boolean} allowNull
 * @return {SerializableStructure|string|number|boolean|Array|null}
 */
export function unserialize(data, type, container, allowNull = false) {
    let parsedValue

    if(!DataContainer.contains(container)) {
        throw new Error(`Unknown data container ${container}.`)
    } else if(container !== DataContainer.NONE) {
        if(!Array.isArray(data))
            throw new Error("Serialized list not contained within an array.")
        parsedValue = DataContainer.map(container, data, (d) => unserialize(d, type, DataContainer.NONE, allowNull))
    } else if(data === null) {
        if(allowNull)
            return null
        else
            throw new Error(`Data must be a ${type.name ?? type.toString()} and not null.`)
    } else if(type.prototype instanceof SerializableStructure) {
        // Check validity of data structure.
        if(type.exportType === ExportType.PARTIAL) {
            if(typeof data !== "object" || Object.keys(data).some(k => type.properties.find(p => p.name === k) === undefined))
                throw new Error(`Data isn't a valid partial of ${type.name}.`)
            parsedValue = _unserializePartial(data, type)
        } else if(type.exportType === ExportType.WITH_TYPE) {
            if(typeof data !== "object" || !data.hasOwnProperty("@type"))
                throw new Error(`Data isn't a valid serialized ${type.name}.`)
            const propCls = COLLECTED_CLASSES.get(data["@type"])
            if(!(propCls.prototype instanceof type))
                throw new Error(`${data["@type"]} isn't a valid subclass of ${type.name}.`)
            delete data["@type"]
            parsedValue = _unserializeStructure(data, propCls)
        } else if(type.exportType === ExportType.CLASSIC) {
            if(typeof data !== "object" || Object.keys(data).some(k => type.properties.find(p => p.name === k) === undefined))
                throw new Error(`Data isn't a valid serialized ${type.name}.`)
            parsedValue = _unserializeStructure(data, type)
        } else
            throw new Error(`Unknown export type ${type.exportType}.`)
    } else if(type.prototype instanceof Enum) {
        if(typeof data !== "string")
            throw new Error(`Data must be a string.`)
        if(!type.contains(data))
            throw new Error(`Data not a valid value of ${type.name}.`)
        parsedValue = data
    } else if(IMPORTERS.has(type)) {
        const importer = IMPORTERS.get(type)
        parsedValue = importer(data)
    } else {
        throw new Error(`Unrecognized type ${type}.`)
    }
    return parsedValue
}

/**
 * Unserializes a serialized structure.
 * @param {object} data
 * @param {typeof SerializableStructure} cls
 * @return {SerializableStructure}
 * @private
 */
function _unserializeStructure(data, cls) {
    const args = {}
    for(const property of cls.properties) {
        if(!Object.hasOwn(data, property.name))
            throw new Error(`No property ${property.name} found in ${JSON.stringify(data)}.`)
        // Handle properties and sets.
        args[property.name] = unserialize(data[property.name], property.type, property.container, property.allowNull)
    }
    return new cls(args)
}

/**
 * Unserializes a serialized partial structure.
 * @param {object} data
 * @param {typeof SerializableStructure} type
 * @return {SerializableStructure}
 * @private
 */
function _unserializePartial(data, type) {
    const args = {}
    for(const property of type.properties) {
        if(Object.hasOwn(data, property.name)) {
            args[property.name] = unserialize(data[property.name], property.type, property.container, property.allowNull)
        }
    }
    return new type(args)
}

/**
 * Unserializes data from Native objects
 * @param {any} data
 * @private
 */
function _unserializeUnknownData(data) {
    let unserialized
    if(typeof data === "object" && Object.hasOwn(data, "@type")) {
        const propCls = COLLECTED_CLASSES.get(data["@type"])
        if(!(propCls?.prototype instanceof SerializableStructure))
            throw new Error(`${data["@type"]} isn't a valid serialized structure.`)
        if(propCls.exportType === ExportType.PARTIAL)
            unserialized = _unserializePartial(data, propCls)
        else if(propCls.exportType === ExportType.CLASSIC)
            unserialized = _unserializeStructure(data, propCls)
        else if(propCls.exportType === ExportType.WITH_TYPE)
            unserialized = _unserializeStructure(data, propCls)
        else
            throw new Error(`Unknown export type ${propCls.exportType}.`)
    } else if(typeof data === "string" || typeof data === "boolean" || typeof data === "number") {
        unserialized = data
    } else if(Array.isArray(data)) {
        unserialized = data.map(d => _unserializeUnknownData(d))
    } else
        throw new Error("Cannot unserialized unknown subproperty of native object.")
    return unserialized
}

/**
 * Unserializes a native object.
 * @param data
 * @private
 */
function _unserializeObject(data) {
    const unserialized = {}
    for(const [k, v] of Object.entries(data)) {
        unserialized[k] = _unserializeUnknownData(v)
    }
    return unserialized
}