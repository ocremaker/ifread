/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Delta from "quill-delta"
import { Color, Integer } from "../types/index.mjs"
import { Token, importToken, DEFAULT_TOKEN_TYPES } from "../calcif"


const Id = (x) => x

export const SERIALIZERS = new Map([
    [String, Id],
    [Number, Id],
    [Boolean, Id],
    [Integer, Id],
    [Color, Id],
    [Token, Id],
    [Delta, Id],
])

export const IMPORTERS = new Map([
    [String, String],
    [Number, Number],
    [Boolean, Boolean],
    [Integer, (x) => Math.round(Number(x))],
    [Color, Id],
    [Token, (t) => importToken(t, DEFAULT_TOKEN_TYPES)],
    [Delta, Id],
])

export const ENFORCERS = new Map([
    [String, String],
    [Number, Number],
    [Boolean, Boolean],
    [Integer, (x) => Math.round(Number(x))],
    [Color, String],
    [Token, (t) => importToken(t, DEFAULT_TOKEN_TYPES)],
    [Delta, Id],
])
