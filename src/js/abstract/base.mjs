/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ENFORCERS } from "./serialize/basic.mjs"
import { Enum } from "./types/index.mjs"
import { DataContainer } from "./container.mjs"

/**
 * @typedef {object} SerializableStructurePropertyProperties
 *
 * @property {any?} type
 * @property {string?} container - Value of DataContainer enum.
 * @property {boolean?} toDuplicate - If set to false, the data will NOT be copied for duplicated items.
 * @property {boolean?} allowNull - If set to true, null will be allowed as an alternative value.
 * @property {(boolean|function(any): boolean)?} userEditable - If true, means the user can edit it.
 */

export const COLLECTED_CLASSES = new Map()
const COLLECTED_PROPERTIES = new Set()

/**
 * Export types define how serializable structures are to be exported and imported.
 */
export class ExportType extends Enum {
    /**
     * 'With type' export type adds an '@type' property during serialization.
     * Required to unserialize subclasses of serialized structures (e.g. block types)
     * @type {string}
     */
    static WITH_TYPE = "with_type"
    /**
     * Classic export type serializes all object's the properties, nothing more, nothing less.
     * @type {string}
     */
    static CLASSIC = "classic"
    /**
     * Partial export type serializes only the *defined* object properties.
     * Used for version diffed objects.
     * @type {string}
     */
    static PARTIAL = "partial"
}

/**
 * Base class for all serializable objects (non-types).
 * Registers all properties, but needs to be collected.
 */
export class SerializableStructure {
    /** @type {{name: string, type: any, container: string, toDuplicate: boolean, allowNull: boolean, userEditable: boolean|function(any): boolean}[]} */
    static properties = []
    static _collected = false
    /**
     * Define the exportation type for this structure.
     */
    static exportType = ExportType.CLASSIC

    constructor() {
        if(!this.constructor._collected)
            throw new Error(`Cannot construct non-collected serializable structure ${this.constructor.name}.`)
    }
}

/**
 * Returns true if the given type of data is serializable. False otherwise.
 * @param {any} type
 * @return {boolean}
 */
export function isTypeSerializable(type) {
    return ENFORCERS.has(type) || type.prototype instanceof Enum || type.prototype instanceof SerializableStructure
}

/**
 * Decorator property for accessors in abstract blocks.
 * Enforces typechecks.
 * @param {SerializableStructurePropertyProperties} options
 * @returns {function({get, set}, {kind: *, name: *, addInitializer: *}): {get?, set?, init?}}
 */
export function property(options) {
    const {
        type = String,
        container = DataContainer.NONE,
        toDuplicate = true,
        allowNull = false,
        userEditable = false
    } = (options ?? {})

    if(!DataContainer.contains(container))
        throw new Error(`Unknown container ${container}.`)

    return ({ get, set }, { kind, name }) => {
        if(kind !== "accessor")
            throw new Error("Must decorate an auto-accessor")
        if(!isTypeSerializable(type))
            throw new Error(`${type} is neither a native type nor a subclass of Enum or SerializableStructure.`)
        COLLECTED_PROPERTIES.add({ name, type, container, toDuplicate, allowNull, userEditable })
        return {
            get,
            set(newValue) {
                let toBeSet = newValue
                if(newValue !== undefined || this.constructor.exportType !== ExportType.PARTIAL) {
                    if(!DataContainer.isContainedWithin(container, newValue))
                        throw new Error(`Data being set not contained within container type ${container}.`)
                    toBeSet = DataContainer.map(container, newValue, (value) => {
                        if(newValue !== null) {
                            if(type.prototype instanceof Enum && !type.contains(value))
                                throw new Error(`${value} of ${name} isn't a value of enum ${type.name}.`)
                            if(type.prototype instanceof SerializableStructure && !(value instanceof type))
                                throw new Error(`${value} of ${name} isn't an instance of ${type.name}.`)
                        } else if(!allowNull) {
                            throw new Error(`${value} of ${name} cannot be null.`)
                        }
                        if(newValue !== null && ENFORCERS.has(type))
                            return ENFORCERS.get(type)(value)
                        else
                            return value
                    })
                }
                set.call(this, toBeSet)
            }
        }
    }
}

/**
 * Collects the class properties and registers the class into being serializable.
 * @param {typeof SerializableStructure} cls
 */
export function collect(cls) {
    if(!(cls.prototype instanceof SerializableStructure))
        throw new Error(`Can only collect subclasses of SerializableStructure.`)
    cls.properties = [...cls.properties, ...COLLECTED_PROPERTIES]
    COLLECTED_PROPERTIES.clear()
    cls._collected = true
    COLLECTED_CLASSES.set(cls.name, cls)
    return cls
}