/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { generateUUID } from "#utils"
import { Definition, Type } from "./definition.mjs"
import { collect, property, SerializableStructure } from "../base.mjs"
import { Enum } from "../types"
import { Token } from "../calcif"
import { DataContainer } from "../container.mjs"
import * as CalcIf from "../calcif/index.mjs"

export class BehaviorOnOverflow extends Enum {
    static CLAMP = "clamp"
    static MODULO = "modulo"
}

export const TYPE_TO_CALCIFTYPE = new Map([
    [Type.INT, CalcIf.Type.INTEGER],
    [Type.BOOL, CalcIf.Type.BOOLEAN]
])

/**
 * Converts the given definition to the object values usable for calc if expressions.
 * @param {VariableDefinition[]} variableDefinitions - Definitions to map
 * @param {Record<string, number|boolean>?} valuePerUUID - (Optional) Value to give for variable matching their UUID.
 *                                                          Gives default (false or zero) value if undefined.
 * @return {Map<string, Map<string, number|boolean>>}
 */
export function convertDefinitionsToObjectValues(variableDefinitions, valuePerUUID = {}) {
    const boolVars = variableDefinitions.filter(v => v.varType === Type.BOOL)
    const intVars = variableDefinitions.filter(v => v.varType === Type.INT)

    return new Map([
        ["variable-bool", new Map(boolVars.map(v => [v.uuid, valuePerUUID[v.uuid] ?? false]))],
        ["variable-int", new Map(intVars.map(v => [v.uuid, valuePerUUID[v.uuid] ?? 0]))]
    ])
}

/**
 * Class that represents the assignment of a variable.
 */
@collect
class VariableAssignment extends SerializableStructure {

    /**
     * UUID of the variable definition.
     */
    @property({ type: String })
    accessor targetUUID

    /**
     * The unique id of this assignment.
     * @type {string}
     */
    @property({ type: String })
    accessor uuid

    /**
     * The expression to which the variable can be assigned to.
     * @type {Token[]}
     */
    @property({ type: Token, container: DataContainer.ARRAY })
    accessor calcIfExpression

    /**
     * Stores whether the expression had errors when it was last set.
     * @type {Token[]}
     */
    @property({ type: Boolean })
    accessor expressionHasErrors

    /**
     * What algorithm to use when the value of the integer overflows.
     * @type {string}
     */
    @property({ type: BehaviorOnOverflow })
    accessor behaviorOnOverflow

    /**
     * @param {string} targetUUID - UUID of the variable definition.
     * @param {Token[]} calcIfExpression - Expression used to set the variable.
     * @param {boolean} expressionHasErrors - Stores whether the expression had errors when it was last set.
     * @param {string} behaviorOnOverflow - What algorithm to use when the value of the integer overflows
     * @param {string?} uuid - The unique id of the instruction.
     */
    constructor({ targetUUID, calcIfExpression, expressionHasErrors, behaviorOnOverflow = BehaviorOnOverflow.CLAMP, uuid = null }) {
        super()
        this.targetUUID = targetUUID
        this.calcIfExpression = calcIfExpression
        this.expressionHasErrors = expressionHasErrors
        this.behaviorOnOverflow = behaviorOnOverflow
        this.uuid = uuid ?? generateUUID()
    }

    /**
     * Returns the variable definition this instruction sets. If not found, type undefined.
     * @param {VariableDefinition[]} variableDefinitions
     * @type {VariableDefinition|undefined}
     */
    getTargetDefinitionWithin(variableDefinitions) {
        return variableDefinitions.find(t => t.uuid === this.targetUUID)
    }
}

export const Assignment = VariableAssignment