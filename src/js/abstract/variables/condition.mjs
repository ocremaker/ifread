/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { collect, property, SerializableStructure } from "../base.mjs"
import { Token } from "../calcif/index.mjs"
import { DataContainer } from "../container.mjs"

@collect
class VariableCondition extends SerializableStructure {
    /**
     * The expression of the condition.
     * @type {Token[]}
     */
    @property({ type: Token, container: DataContainer.ARRAY })
    accessor calcIfExpression

    /**
     * Stores whether the expression had errors when it was last set.
     * @type {Token[]}
     */
    @property({ type: Boolean })
    accessor expressionHasErrors

    /**
     * @param {Token[]} calcIfExpression - Expression of the condition.
     * @param {boolean} expressionHasErrors - Stores whether the expression had errors when it was last set.
     */
    constructor({ calcIfExpression, expressionHasErrors }) {
        super()
        this.calcIfExpression = calcIfExpression
        this.expressionHasErrors = expressionHasErrors
    }
}

export const Condition = VariableCondition