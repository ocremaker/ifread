/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, property, SerializableStructure } from "../base.mjs"
import { Color } from "../types/index.mjs"
import * as Variable from "../variables/index.mjs"
import { DataContainer } from "../container.mjs"
import { generateUUID } from "../../utils/index.mjs"

@collect
export class AbstractZone extends SerializableStructure {
    /**
     * @type {string}
     */
    @property({ type: String, toDuplicate: false })
    accessor uuid

    /**
     * UUID of the parent zone of this zone.
     * @type {string|null}
     */
    @property({ type: String, toDuplicate: false, allowNull: true })
    accessor parentZone

    /**
     * Title of the zone.
     * @type {string}
     */
    @property({ type: String, userEditable: true })
    accessor title

    /**
     * Background color of the zone.
     * @type {string}
     */
    @property({ type: Color, userEditable: true })
    accessor color

    /**
     * Variables defined in this zone.
     * @type {Set<VariableDefinition>}
     */
    @property({ type: Variable.Definition, container: DataContainer.SET, userEditable: true })
    accessor variables

    constructor({ uuid, parentZone, title, color, variables }) {
        super()
        this.uuid = uuid ?? generateUUID()
        this.parentZone = parentZone ?? null
        this.title = title ?? "Untitled Zone"
        this.color = color
        this.variables = variables ?? new Set()
    }
}

