/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { collect, property, SerializableStructure } from "../base.mjs"
import { ChapterVersion } from "./partial/version.mjs"
import { DataContainer } from "../container.mjs"

@collect
export class Chapter extends SerializableStructure {
    /**
     * @type {string}
     */
    @property({ type: String })
    accessor uuid

    /**
     * @type {string}
     */
    @property({ type: String })
    accessor title

    /**
     * @type {string}
     */
    @property({ type: String })
    accessor currentVersion

    // @property({ type: object })
    // accessor currentAbstract

    /**
     * @type {ChapterVersion[]}
     */
    @property({ type: ChapterVersion, container: DataContainer.ARRAY })
    accessor versions

    constructor({ uuid, title, currentVersion, currentAbstract, versions }) {
        super()
        this.uuid = uuid
        this.title = title
        this.currentVersion = currentVersion
        this.currentAbstract = currentAbstract
        this.versions = versions
    }

    /**
     * Compile all versions ancestors required to compile given version.
     * @param {ChapterVersion} version
     * @returns {ChapterVersion[]}
     */
    compileVersionAncestryFor(version) {
        const versions = [version]
        while(versions.at(-1).previousVersion !== null) {
            const current = versions.at(-1)
            let previousVersion = this.versions.find(v => v.uuid === current.previousVersion)
            if(previousVersion === undefined)
                throw new Error(`Unknown version by ID ${current.previousVersion}.`)
            versions.push(previousVersion)
        }
        versions.reverse() // Go from oldest to current
        return versions
    }
}