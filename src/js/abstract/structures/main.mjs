/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { collect, property, SerializableStructure } from "../base.mjs"
import { ExportFormatsSettings } from "./exports.mjs"
import { Chapter } from "./chapter.mjs"
import { DataContainer } from "../container.mjs"
import { Integer } from "../types/index.mjs"

@collect
export class IfreadSaveFile extends SerializableStructure {

    /**
     * @type {string}
     */
    @property({ type: String })
    accessor $schema = "https://ocremaker.frama.io/ifread/schema/v1/ifread.schema.json"

    /**
     * @type {string}
     */
    @property({ type: String })
    accessor title

    /**
     * @type {number}
     */
    @property({ type: Integer })
    accessor firstCreated

    /**
     * @type {number}
     */
    @property({ type: Integer })
    accessor lastModified

    /**
     * @type {ExportFormatsSettings}
     */
    @property({ type: ExportFormatsSettings })
    accessor lastExportFormatSettings

    /**
     * @type {Chapter[]}
     */
    @property({ type: Chapter, container: DataContainer.ARRAY })
    accessor chapters

    constructor({ title, firstCreated, lastModified, lastExportFormatSettings, chapters }) {
        super()
        this.title = title
        this.firstCreated = firstCreated
        this.lastModified = lastModified
        this.lastExportFormatSettings = lastExportFormatSettings
        this.chapters = chapters
    }
}