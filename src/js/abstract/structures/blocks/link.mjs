/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, property, SerializableStructure } from "../../base.mjs"
import * as Types from "../../types/index.mjs"
import * as Variable from "../../variables/index.mjs"
import { DataContainer } from "../../container.mjs"

@collect
export class AbstractSocket extends SerializableStructure {
    /**
     * Unique identifiable name of the socket within the block.
     * @type {string}
     */
    @property({ type: String })
    accessor ref

    /**
     * UUID of the block this socket is in.
     * @type {string}
     */
    @property({ type: String })
    accessor block

    constructor({ ref, block }) {
        super()
        this.ref = ref
        this.block = block
    }

}

@collect
export class AbstractLink extends SerializableStructure {
    /**
     * @type {string}
     */
    @property({ type: String, toDuplicate: false })
    accessor uuid

    /**
     * @type {string}
     */
    @property({ type: String, userEditable: true })
    accessor title

    /**
     * @type {string}
     */
    @property({ type: Types.Link })
    accessor linkType

    /**
     * If true and possible, makes the link bidirectional.
     * @type {boolean}
     */
    @property({ type: Boolean, userEditable: (link) => link.canBeBidirectional() })
    accessor allowBidirectional

    /**
     * @type {AbstractSocket}
     */
    @property({ type: AbstractSocket })
    accessor from

    /**
     * @type {AbstractSocket}
     */
    @property({ type: AbstractSocket })
    accessor to

    /**
     * Condition required to be fulfilled to display this link/the data after this link.
     * NOTE: Depending on the link type, may cause some different behaviors.
     * @type {VariableCondition|null}
     */
    @property({ type: Variable.Condition, allowNull: true, userEditable: true })
    accessor condition

    /**
     * List of assignments to be triggered when passing this link.
     * NOTE: Depending on the link type, may cause some different behaviors.
     * @type {Set<VariableAssignment>}
     */
    @property({ type: Variable.Assignment, container: DataContainer.SET, userEditable: true })
    accessor assignments

    constructor({ uuid, title, linkType, allowBidirectional, from, to, condition, assignments }) {
        super()
        this.uuid = uuid
        this.title = title ?? ""
        this.linkType = linkType
        this.allowBidirectional = allowBidirectional ?? true
        this.from = from
        this.to = to
        this.condition = condition ?? null
        this.assignments = assignments ?? new Set()
    }
}