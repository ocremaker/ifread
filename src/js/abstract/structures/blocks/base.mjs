/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, ExportType, property, SerializableStructure } from "../../base.mjs"
import { serialize, unserialize } from "../../serialize/index.mjs"

/**
 * @abstract
 */
@collect
export class AbstractBlockData extends SerializableStructure {
    static exportType = ExportType.WITH_TYPE

    constructor({}) {
        super()
    }

    /**
     * Duplicates the abstract block.
     * @return {AbstractBlockData}
     */
    duplicate() {
        const newData = {}
        const properties = this.constructor.properties.filter(prop => prop.toDuplicate)
        for(let { name, type, container, allowNull } of properties) {
            const value = this[name]
            // Set the data
            newData[name] = unserialize(serialize(value, type, container, allowNull), type, container, allowNull)
        }
        return new this.constructor(newData)
    }
}

/**
 * @abstract
 */
@collect
export class BaseBlockData extends AbstractBlockData {

    @property({ type: String, userEditable: true })
    accessor title

    constructor({ title = "", ...options }) {
        super(options)
        this.title = title
    }
}