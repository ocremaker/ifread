/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Class constructed to deal with block data, abstracting builders away.
 */
export class FlatBlock {

    /**
     * UUID of the block.
     * @type {string}
     */
    uuid

    /**
     * Object recording all linked blocks based on the ref of the socket through which they are linked.
     * @type {Record<string, FlatBlock[]>}
     */
    linkedThrough

    /**
     * @type {BaseBlockData}
     */
    data

    /**
     *
     * @param {string} uuid
     * @param {Record<string, FlatBlock[]>} linkedThrough
     * @param {BaseBlockData} data
     */
    constructor({ uuid, linkedThrough, data }) {
        this.uuid = uuid
        this.linkedThrough = linkedThrough
        this.data = data
    }
}