/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { collect, property } from "../../../../base.mjs"
import { TextBlockData } from "../../main/index.mjs"

@collect
export class CoreBlockData extends TextBlockData {

    /**
     * If hasTrick is true, then this core gains the possibility to have text for a trick that
     * will be shown upon pressing its trick button.
     * @type {boolean}
     */
    @property({ type: Boolean, userEditable: true })
    accessor hasTrick

    constructor({ hasTrick = false, ...options }) {
        super(options)
        this.hasTrick = hasTrick
    }
}

@collect
export class FirstCoreBlockData extends TextBlockData {}