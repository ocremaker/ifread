/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { BaseVersionDiff, PartialByUUID, VersionDiff } from "../partial/index.mjs"
import { serialize } from "../../serialize/index.mjs"
import { DataContainer } from "../../container.mjs"

/**
 *
 * @param {SerializableStructure} p
 * @return {SerializableStructure}
 */
function clone(p) {
    const data = {}
    for(const prop of p.constructor.properties)
        data[prop.name] = p[prop.name]
    return new p.constructor(data)
}

/**
 * Checks whether two serializable structures are deep equal.
 * Since both structures are serializable, and consistently generated, we can just use JSON.stringify.
 * @param item1
 * @param item2
 * @param type
 * @param {string} container
 * @param {boolean} allowNull
 * @return {boolean}
 */
function areItemsEqual(item1, item2, type, container, allowNull) {
    if(typeof item1 === "object") {
        const compare1 = JSON.stringify(serialize(item1, type, container, allowNull))
        const compare2 = JSON.stringify(serialize(item2, type, container, allowNull))
        return compare1 === compare2
    }else
        return item1 === item2
}

/**
 * Compile various variables from VersionDiffs into a singular flat array.
 * @param {BaseVersionDiff[]} vdiffs
 * @returns {SerializableStructure[]}
 */
export function compileVersionDiffs(vdiffs) {
    if(vdiffs.length === 0)
        throw new Error(`No VersionDiff instance passed.`)
    if(vdiffs.some(vd => !(vd instanceof BaseVersionDiff) || vd?.constructor !== vdiffs[0].constructor))
        throw new Error(`Cannot compile different types of VersionDiffs.`)
    const added = vdiffs.flatMap(vd => vd.added ?? [])
    const removed = vdiffs.flatMap(vd => vd.removed ?? [])
    const present = added.filter(a => !removed.includes(a.uuid)).map(clone)
    for(const version of vdiffs) {
        for(const modified of (version.modified ?? [])) {
            const foundElement = present.find(p => p.uuid === modified.uuid)
            if(foundElement !== undefined) {
                for(const prop of foundElement.properties) {
                    if(Object.hasOwn(modified, prop.name)) {
                        foundElement[prop.name] = modified[prop.name]
                    }
                }
            }
        }
    }
    return present
}

/**
 * Partitions items changed from an original list to a changed one into three categories for version diffs.
 * In added, the new elements added in newItems.
 * In removed, the UUIDs of the elements removed in newItems
 * In modified, arrays containing both the old and new elements.
 * @param {{uuid: string}[]} originalItems
 * @param {{uuid: string}[]} newItems
 * @returns {{added: SerializableStructure[], modified: SerializableStructure[][], removed: string[]}}
 */
export function partIntoAddedRemovedModified(originalItems, newItems) {
    const added = newItems.filter(i => !originalItems.some(item => i.uuid === item.uuid))
    const removed = []
    const modified = []

    for(const item of originalItems) {
        const itemInNew = newItems.find(i => i.uuid === item.uuid)
        if(itemInNew === undefined)
            removed.push(item.uuid)
        else
            modified.push([item, itemInNew])
    }
    return { added, removed, modified }
}

/**
 * Partitions items changed from an original list to a changed one into three categories for version diffs.
 * In added, the new elements added in newItems.
 * In removed, the UUIDs of the elements removed in newItems
 * In modified, the partial by uuid structures modified between both new and old items.
 *
 * @param {{uuid: string}[]} originalItems
 * @param {{uuid: string}[]} newItems
 * @param {typeof SerializableStructure} type
 * @returns {BaseVersionDiff}
 */
export function createVersionDiff(originalItems, newItems, type) {
    const { added, removed, modified } = partIntoAddedRemovedModified(originalItems, newItems)
    const partialItems = []
    for(const [oldItem, newItem] of modified) {
        const properties = { uuid: oldItem.uuid }
        for(const { name, type, container, allowNull } of oldItem.constructor.properties) {
            if(!areItemsEqual(oldItem[name], newItem[name], type, container, allowNull))
                properties[name] = newItem[name]
        }
        if(Object.keys(properties).length > 1)
            partialItems.push(new (PartialByUUID(type))(properties))
    }
    const returns = {}
    if(added.length > 0) returns.added = added
    if(removed.length > 0) returns.removed = removed
    if(partialItems.length > 0) returns.modified = partialItems
    return new (VersionDiff(type))(returns)
}