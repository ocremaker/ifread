/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


import { compileVersionDiffs } from "./versiondiff.mjs"
import { AbstractZoneDelegate } from "../compiled/zone.mjs"
import { AbstractLogicBuilder } from "../compiled/builder.mjs"
import Delta from "quill-delta"
import { QuillDeltaWithID } from "../delta.mjs"
import { CompiledChapterVersion } from "../compiled/version.mjs"

/**
 *
 * @param {{added: string[]?, removed: string[]?}[]} childBuilderDiffs
 * @returns {Set<string>}
 * @private
 */
function _constructChildBuilders(childBuilderDiffs) {
    const currentChildBuilders = new Set()

    for(const versionChildBuilders of childBuilderDiffs) {
        const { added, removed } = versionChildBuilders
        for(const addedChildBuilder of added ?? [])
            currentChildBuilders.add(addedChildBuilder)
        for(const removedChildBuilder of removed ?? [])
            currentChildBuilders.delete(removedChildBuilder)
    }
    return currentChildBuilders
}

/**
 * @param {string} builderUUID
 * @param {PartialAbstractLogicBuilder[]} builderPartials
 * @param {AbstractZone[]} zones
 * @returns {AbstractLogicBuilder}
 * @private
 */
function _constructBuilder(builderUUID, builderPartials, zones) {
    const links = compileVersionDiffs(builderPartials.map(b => b.links))
    const blocks = compileVersionDiffs(builderPartials.map(b => b.blocks))
    const childBuilders = Array.from(_constructChildBuilders(builderPartials.map(b => b.childBuilders)))
    // Building zone delegates
    /** @type {Map<string, PartialAbstractZoneDelegate[]>} */
    const zonesDelegatesMap = new Map()
    for(const builder of builderPartials) {
        for(const zoneDelegate of builder.zonesDelegates) {
            if(!zonesDelegatesMap.has(zoneDelegate.uuid)) {
                const matchingZone = zones.some(z => z.uuid === zoneDelegate.uuid)
                if(matchingZone)
                    zonesDelegatesMap.set(zoneDelegate.uuid, [zoneDelegate])
            } else {
                zonesDelegatesMap.get(zoneDelegate.uuid).push(zoneDelegate)
            }
        }
    }
    const zonesDelegates = []
    for(const [zoneUUID, zoneDelegates] of zonesDelegatesMap) {
        const squares = new Set()
        for(const zoneDelegate of zoneDelegates) {
            if(zoneDelegate.squares.added !== undefined)
                for(const square of zoneDelegate.squares.added)
                    squares.add(square)
            if(zoneDelegate.squares.removed !== undefined)
                for(const removed of (zoneDelegate.squares.removed)) {
                    const sq = squares.find(s => s.x === removed.x && s.y === removed.y)
                    squares.remove(sq)
                }
        }
        zonesDelegates.push(new AbstractZoneDelegate({ uuid: zoneUUID, squares: Array.from(squares) }))
    }
    return new AbstractLogicBuilder({
        uuid: builderUUID,
        childBuilders,
        zonesDelegates,
        blocks,
        links
    })
}

/**
 *
 * @param {string} textUUID
 * @param {{insert?, retain?, delete?}[][]} deltas
 * @returns {QuillDeltaWithID}
 * @private
 */
function _constructText(textUUID, deltas) {
    let compiledDelta = new Delta(deltas[0])
    for(let i = 1; i < deltas.length; i++)
        compiledDelta = compiledDelta.compose(new Delta(deltas[i]))
    return new QuillDeltaWithID({ uuid: textUUID, contents: compiledDelta.ops })
}


/**
 * Recursively finds all referenced builders from the main one.
 * @param {Map<string, PartialAbstractLogicBuilder[]>} buildersMap
 * @returns {string[]}
 * @private
 */
function _getReferencedBuilderIDs(buildersMap) {
    const validatedBuilderIDs = buildersMap.has("main") ? ["main"] : []
    let toValidateBuilderIDs = validatedBuilderIDs
    while(toValidateBuilderIDs.length > 0) {
        const idsToValidate = toValidateBuilderIDs
        toValidateBuilderIDs = []
        for(const toValidate of idsToValidate) {
            const builderPartials = buildersMap.get(toValidate)
            const childBuilders = _constructChildBuilders(builderPartials.map(b => b.childBuilders))
            toValidateBuilderIDs.push(...childBuilders)
        }
        validatedBuilderIDs.push(...toValidateBuilderIDs)
    }
    return validatedBuilderIDs
}


/**
 * Compiles several diffed chapter versions into a full one.
 * @param {ChapterVersion[]} versions
 * @returns {CompiledChapterVersion}
 */
export function compileSequentialVersions(versions) {
    /** @type {AbstractZone[]} */
    const zones = compileVersionDiffs(versions.map(v => v.zones))
    /** @type {Map<string, PartialAbstractLogicBuilder[]>} */
    const buildersMap = new Map()
    /** @type {Map<string, {insert?, retain?, delete?}[][]>} */
    const textsMap = new Map()
    for(const v of versions) {
        for(const builder of v.builders) {
            if(!buildersMap.has(builder.uuid))
                buildersMap.set(builder.uuid, [builder])
            else
                buildersMap.get(builder.uuid).push(builder)
        }
        const { added, modified, removed } = v.texts
        for(const newText of added ?? [])
            textsMap.set(newText.uuid, [newText.contents])
        for(const diffText of modified ?? [])
            textsMap.get(diffText.uuid).push(diffText.contents)
        for(const removedUUID of removed ?? [])
            textsMap.delete(removedUUID)
    }

    // Constructing Builders
    const referencedBuilders = _getReferencedBuilderIDs(buildersMap)
    const builders = []
    for(const [builderUUID, buildersData] of buildersMap) {
        if(referencedBuilders.includes(builderUUID))
            builders.push(_constructBuilder(builderUUID, buildersData, zones))
    }

    // Compile texts
    const texts = []
    for(const [textUUID, textDeltas] of textsMap) {
        texts.push(_constructText(textUUID, textDeltas))
    }

    return new CompiledChapterVersion({
        uuid: versions.at(-1).uuid,
        author: versions.at(-1).author,
        lastModified: versions.at(-1).lastModified,
        previousVersion: versions.at(-1).previousVersion,
        texts, zones, builders
    })
}