/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { FlatBlock, StartBlockData, ChoiceBlockData, OutboundBlockData } from "../blocks/"

/**
 *
 * @param {FlatBlock} originalBlock
 * @param {AbstractLogicBuilder}builder
 * @return {Generator<{block: FlatBlock, builder}, void, *>}
 * @private
 */
function* _createLinkedBlocks(originalBlock, builder) {
    const links = builder.links.filter(link => link.from.block === originalBlock.uuid)
    for(const link of links) {
        const targetBlock = builder.blocks.find(b => b.uuid === link.to.block)
        const flatTargetBlock = new FlatBlock({
            uuid: targetBlock.uuid,
            linkedThrough: { [link.to.ref]: [originalBlock] },
            data: targetBlock.data
        })
        // Push to link
        if(Object.hasOwn(originalBlock.linkedThrough, link.from.ref))
            originalBlock.linkedThrough[link.from.ref].push(flatTargetBlock)
        else
            originalBlock.linkedThrough[link.from.ref] = [flatTargetBlock]
        yield { block: flatTargetBlock, builder }
    }
}

/**
 *
 * @param {FlatBlock} choiceBlock
 * @param {AbstractLogicBuilder[]} builders
 * @param {AbstractLogicBuilder} innerBuilder
 * @param {AbstractLogicBuilder} outerBuilder
 * @return {Generator<{block: FlatBlock, builder}, void, *>}
 * @private - The flat blocks of the ones linked in outbound.
 */
function* _insertBuilderWithin(choiceBlock, builders, innerBuilder, outerBuilder) {
    /** @type {ChoiceBlockData} */
    const data = choiceBlock.data
    const originalUUID = choiceBlock.uuid

    const mainInnerBlock = innerBuilder.blocks.find(b => b.data instanceof data.innerMainBlockType)
    choiceBlock.data = mainInnerBlock.data // Delete choice block data (which *should* be empty)
    choiceBlock.uuid = mainInnerBlock.uuid
    const innerOutbounds = _parseInnerBuilder(choiceBlock, innerBuilder, builders)
    choiceBlock.uuid = originalUUID
    /** @type {AbstractLink[]} */
    const outboundLinks = outerBuilder.links.filter(link => link.from.block === choiceBlock.uuid)
    for(const link of outboundLinks) {
        const targetBlock = outerBuilder.blocks.find(b => b.uuid === link.to.block)
        // Delete outbound block, and hook target block to the one that used to be linked to it.
        const outboundBlock = innerOutbounds.find(o => link.from.ref === `Inbound-${o.uuid}`)
        const blockBeforeOutbound = outboundBlock.linkedThrough["toOutbound"][0] // There is exactly one in this configuration.
        const flatTargetBlock = new FlatBlock({
            uuid: targetBlock.uuid,
            linkedThrough: { [link.to.ref]: [blockBeforeOutbound] },
            data: targetBlock.data
        })
        // Hooking the block before the outbound block to the one outside the builder.
        const refToOutbound = Object.keys(blockBeforeOutbound.linkedThrough).find(r => blockBeforeOutbound.linkedThrough[r].includes(outboundBlock))
        const outboundIdx = blockBeforeOutbound.linkedThrough[refToOutbound].indexOf(outboundBlock)
        blockBeforeOutbound.linkedThrough[refToOutbound][outboundIdx] = flatTargetBlock
        yield { block: flatTargetBlock, builder: outerBuilder }
    }

}

/**
 *
 * @param {FlatBlock} flatStartBlock
 * @param {AbstractLogicBuilder} innerBuilder
 * @param {AbstractLogicBuilder[]} builders
 * @return {FlatBlock[]} Outbounds of the builder.
 * @private
 */
function _parseInnerBuilder(flatStartBlock, innerBuilder, builders) {
    const outbounds = []
    const blocksToCheck = [
        { block: flatStartBlock, builder: innerBuilder }
    ]

    for(const { block, builder } of blocksToCheck) {
        const data = block.data
        if(data instanceof ChoiceBlockData) {
            const newInnerBuilder = builders.find(b => b.uuid === data.innerBuilderId(block.uuid))
            blocksToCheck.push(..._insertBuilderWithin(block, builders, newInnerBuilder, innerBuilder))
        } else if(data instanceof OutboundBlockData) {
            outbounds.push(block)
        } else {
            blocksToCheck.push(..._createLinkedBlocks(block, builder))
        }
    }
    return outbounds
}

/**
 *
 * @param {CompiledChapterVersion} compiledChapterVersion
 * @returns {FlatBlock}
 */
export function createFlatBlockRow(compiledChapterVersion) {
    const mainBuilder = compiledChapterVersion.builders.find(b => b.uuid === "main")
    if(!mainBuilder)
        throw new Error("No block data present in compiled chapter.")
    const startBlock = mainBuilder.blocks.find(b => b.data instanceof StartBlockData)
    const flatStartBlock = new FlatBlock({
        uuid: startBlock.uuid,
        linkedThrough: {},
        data: startBlock.data
    })
    _parseInnerBuilder(flatStartBlock, mainBuilder, compiledChapterVersion.builders)
    return flatStartBlock
}