/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {
    ZoneSquaresDiff,
    ChapterVersion,
    PartialAbstractLogicBuilder,
    PartialAbstractZoneDelegate, PartialByUUID, ChildBuildersDiff,
    VersionDiff
} from "../partial/"
import { createVersionDiff, partIntoAddedRemovedModified } from "./versiondiff.mjs"
import { QuillDeltaWithID } from "../delta.mjs"
import { AbstractZone } from "../zone.mjs"
import { AbstractLogicBuilder } from "../compiled/builder.mjs"
import { AbstractBlock, AbstractLink } from "../blocks/"
import Delta from "quill-delta"
import { AbstractZoneDelegate } from "../compiled/index.mjs"

/**
 *
 * @param {QuillDeltaWithID[]} originalTexts
 * @param {QuillDeltaWithID[]} newTexts
 * @returns {BaseVersionDiff}
 * @private
 */
function _createDifferenceBetweenTexts(originalTexts, newTexts) {
    const { added, removed, modified } = partIntoAddedRemovedModified(originalTexts, newTexts)
    const modifiedDeltas = []
    for(const [oldItem, newItem] of /** @type {QuillDeltaWithID[][]} */ modified) {
        const oldDelta = new Delta(oldItem.contents)
        const newDelta = new Delta(newItem.contents)
        const delta = oldDelta.diff(newDelta)
        if(delta.ops.length > 1 || (delta.ops.length === 1 && !Object.hasOwn(delta.ops[0], 'retain')))
            // Only push if the resulting delta actually modifies something.
            modifiedDeltas.push(new (PartialByUUID(QuillDeltaWithID))({
                uuid: oldItem.uuid,
                contents: delta.ops
            }))
    }
    const returns = {}
    if(added.length > 0) returns.added = added
    if(removed.length > 0) returns.removed = removed
    if(modifiedDeltas.length > 0) returns.modified = modifiedDeltas
    return new (VersionDiff(QuillDeltaWithID))(returns)
}

/**
 *
 * @param {AbstractZoneSquare[]} originalSquares
 * @param {AbstractZoneSquare[]} newSquares
 * @returns {ZoneSquaresDiff}
 * @private
 */
function _createPartialZoneDelegateSquares(originalSquares, newSquares) {
    const added = newSquares.filter(sq => !originalSquares.some(sq2 => sq.x === sq2.x && sq.y === sq2.y))
    const removed = originalSquares.filter(sq => !newSquares.some(sq2 => sq.x === sq2.x && sq.y === sq2.y))
    const result = {}
    if(added.length > 0) result.added = added
    if(removed.length > 0) result.removed = removed
    return new ZoneSquaresDiff(result)
}

/**
 *
 * @param {string[]} originalChildBuilders
 * @param {string[]} newChildBuilders
 * @returns {ChildBuildersDiff}
 * @private
 */
function _createPartialChildBuilderUUIDs(originalChildBuilders, newChildBuilders) {
    const added = newChildBuilders.filter(uuid => !originalChildBuilders.includes(uuid))
    const removed = originalChildBuilders.filter(uuid => !newChildBuilders.includes(uuid))
    const result = {}
    if(added.length > 0) result.added = added
    if(removed.length > 0) result.removed = removed
    return new ChildBuildersDiff(result)
}

/**
 *
 * @param {AbstractLogicBuilder} originalBuilder
 * @param {AbstractLogicBuilder} newBuilder
 * @returns {PartialAbstractLogicBuilder}
 * @private
 */
function _createPartialBuilder(originalBuilder, newBuilder) {
    const blocks = createVersionDiff(originalBuilder.blocks, newBuilder.blocks, AbstractBlock)
    const links = createVersionDiff(originalBuilder.links, newBuilder.links, AbstractLink)
    const childBuilders = _createPartialChildBuilderUUIDs(originalBuilder.childBuilders, newBuilder.childBuilders)
    const zonesDelegates = []
    for(const zd of newBuilder.zonesDelegates) {
        let zdo = originalBuilder.zonesDelegates.find(zd2 => zd2.uuid === zd.uuid)
        if(zdo === undefined)
            zdo = new AbstractZoneDelegate({ uuid: zd.uuid, squares: [] })
        const squaresDiff = _createPartialZoneDelegateSquares(zdo.squares, zd.squares)
        if(squaresDiff.added !== undefined || squaresDiff.removed !== undefined)
            zonesDelegates.push(new PartialAbstractZoneDelegate({
                uuid: zd.uuid,
                squares: squaresDiff
            }))
    }
    return new PartialAbstractLogicBuilder({
        uuid: newBuilder.uuid,
        childBuilders,
        zonesDelegates,
        blocks,
        links
    })
}

/**
 *
 * @param {AbstractLogicBuilder[]} originalBuilders
 * @param {AbstractLogicBuilder[]} newBuilders
 * @returns {PartialAbstractLogicBuilder[]}
 * @private
 */
function _createDifferenceBetweenBuilders(originalBuilders, newBuilders) {
    const diffedBuilders = []
    for(const newBuilder of newBuilders) {
        let originalBuilder = originalBuilders.find(b => b.uuid === newBuilder.uuid)
        if(originalBuilder === undefined)
            // Newly added builder (e.g. new choice).
            // Create a dummy empty builder in the place of undefined.
            originalBuilder = new AbstractLogicBuilder({
                uuid: newBuilder.uuid,
                zonesDelegates: [],
                childBuilders: [],
                blocks: [],
                links: []
            })
        const partialBuilder = _createPartialBuilder(originalBuilder, newBuilder)
        if(partialBuilder.zonesDelegates.length > 0 || !partialBuilder.blocks.isEmpty() || !partialBuilder.links.isEmpty())
            diffedBuilders.push(partialBuilder)
    }

    return diffedBuilders
}

/**
 * Creates a difference between two compiled chapter versions, and returns the partial that contains the difference.
 * @param {CompiledChapterVersion} originalVersion
 * @param {CompiledChapterVersion} newVersion
 * @returns {ChapterVersion}
 */
export function createDifferenceBetweenVersions(originalVersion, newVersion) {
    const texts = _createDifferenceBetweenTexts(originalVersion.texts, newVersion.texts)
    const zones = createVersionDiff(originalVersion.zones, newVersion.zones, AbstractZone)
    const builders = _createDifferenceBetweenBuilders(originalVersion.builders, newVersion.builders)
    return new ChapterVersion({
        uuid: newVersion.uuid,
        author: newVersion.author,
        lastUpdate: newVersion.lastModified,
        previousVersion: originalVersion.uuid !== newVersion.uuid ? originalVersion.uuid : originalVersion.previousVersion,
        texts,
        zones,
        builders
    })
}