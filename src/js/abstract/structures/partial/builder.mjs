/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, ExportType, property, SerializableStructure } from "../../base.mjs"
import { DataContainer } from "../../container.mjs"
import { PartialAbstractZoneDelegate } from "./zone.mjs"
import { VersionDiff } from "./versiondiff.mjs"
import { AbstractLink } from "../blocks/link.mjs"
import { AbstractBlock } from "../blocks/block.mjs"
import { AbstractZoneSquare } from "../zonesquare.mjs"

@collect
export class ChildBuildersDiff extends SerializableStructure {
    static exportType = ExportType.PARTIAL

    /**
     * @type {string[]|undefined}
     */
    @property({ type: String, container: DataContainer.ARRAY })
    accessor added
    /**
     * @type {string[]|undefined}
     */
    @property({ type: String, container: DataContainer.ARRAY })
    accessor removed

    constructor({ added, removed }) {
        super()
        this.added = added
        this.removed = removed
    }
}

@collect
export class PartialAbstractLogicBuilder extends SerializableStructure {
    /**
     * @type {string}
     */
    @property({ type: String })
    accessor uuid

    /**
     * @type {ChildBuildersDiff}
     */
    @property({ type: ChildBuildersDiff })
    accessor childBuilders

    /**
     * @type {PartialAbstractZoneDelegate[]}
     */
    @property({ type: PartialAbstractZoneDelegate, container: DataContainer.ARRAY })
    accessor zonesDelegates

    /**
     * @type {BaseVersionDiff}
     */
    @property({ type: VersionDiff(AbstractBlock) })
    accessor blocks

    /**
     * @type {BaseVersionDiff}
     */
    @property({ type: VersionDiff(AbstractLink) })
    accessor links

    constructor({ uuid, childBuilders, zonesDelegates, blocks, links }) {
        super()
        this.uuid = uuid
        this.childBuilders = childBuilders
        this.zonesDelegates = zonesDelegates
        this.blocks = blocks
        this.links = links
    }
}