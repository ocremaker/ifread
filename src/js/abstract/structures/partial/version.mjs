/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, property, SerializableStructure } from "../../base.mjs"
import { QuillDeltaWithID } from "../delta.mjs"
import { Integer } from "../../types/index.mjs"
import { DataContainer } from "../../container.mjs"
import { VersionDiff } from "./versiondiff.mjs"
import { AbstractZone } from "../zone.mjs"
import { PartialAbstractLogicBuilder } from "./builder.mjs"

@collect
export class ChapterVersion extends SerializableStructure {
    /**
     * @type {string}
     */
    @property({ type: String })
    accessor uuid

    /**
     * @type {string}
     */
    @property({ type: String })
    accessor author

    /**
     * @type {number}
     */
    @property({ type: Integer })
    accessor lastModified

    /**
     * UUID of the parent version
     * @type {string|null}
     */
    @property({ type: String, allowNull: true })
    accessor previousVersion

    /**
     * @type {BaseVersionDiff}
     */
    @property({ type: VersionDiff(QuillDeltaWithID) })
    accessor texts

    /**
     * @type {BaseVersionDiff}
     */
    @property({ type: VersionDiff(AbstractZone) })
    accessor zones

    /**
     * @type {PartialAbstractLogicBuilder[]}
     */
    @property({ type: PartialAbstractLogicBuilder, container: DataContainer.ARRAY })
    accessor builders

    constructor({ uuid, author, lastModified, previousVersion, texts, zones, builders }) {
        super()
        this.uuid = uuid
        this.author = author
        this.lastModified = lastModified
        this.previousVersion = previousVersion
        this.texts = texts
        this.zones = zones
        this.builders = builders
    }
}