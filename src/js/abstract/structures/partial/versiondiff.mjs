/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, ExportType, property, SerializableStructure } from "../../base.mjs"
import { DataContainer } from "../../container.mjs"
import { ExtensionClass } from "#utils"

/**
 * Converts a SerializableStructure to a class where the only required property is the UUID.
 * @type {function(typeof SerializableStructure): typeof SerializableStructure}
 */
export const PartialByUUID = new ExtensionClass(SerializableStructure, (C) => {
        class PartialByUUID extends C {
            static exportType = ExportType.PARTIAL

            constructor(props) {
                super(props)
                if(!Object.hasOwn(props, "uuid"))
                    throw new Error(`${C.name} partial must have a UUID.`)
            }

        }
        Object.defineProperty(PartialByUUID, "name", { value: `PartialByUUID<${C.name}>`, writable: false })
        return collect(PartialByUUID)
    }
)

/**
 * Base class for all version diffs.
 * @abstract
 */
export class BaseVersionDiff extends SerializableStructure {
    static exportType = ExportType.PARTIAL
    /** @type {Array} */
    accessor added
    /** @type {Array} */
    accessor modified
    /** @type {string[]} */
    accessor removed

    /**
     * If this VersionDiff does not contain any modification, returns true. False otherwise.
     * @return {boolean}
     */
    isEmpty() {
        return this.added === undefined && this.modified === undefined && this.removed === undefined
    }
}

/**
 * Creates a VersionDiff subclass of argument.
 * @param {typeof SerializableStructure} cls
 * @returns {typeof BaseVersionDiff}
 * @type {function(typeof SerializableStructure): typeof BaseVersionDiff}
 */
export const VersionDiff = new ExtensionClass(SerializableStructure, (C) => {
    class VersionDiffDelegate extends BaseVersionDiff {
        @property({ type: C, container: DataContainer.ARRAY })
        accessor added

        @property({ type: String, container: DataContainer.ARRAY })
        accessor removed

        @property({ type: PartialByUUID(C), container: DataContainer.ARRAY })
        accessor modified

        constructor({ added, removed, modified }) {
            super()
            this.added = added
            this.modified = modified
            this.removed = removed
        }
    }
    Object.defineProperty(VersionDiffDelegate, "name", { value: `VersionDiff<${C.name}>`, writable: false })
    return collect(VersionDiffDelegate)
})