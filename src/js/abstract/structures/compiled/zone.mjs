/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, property, SerializableStructure } from "../../base.mjs"
import { DataContainer } from "../../container.mjs"
import { AbstractZoneSquare } from "../zonesquare.mjs"

@collect
export class AbstractZoneDelegate extends SerializableStructure {
    /**
     * UUID of the Zone for which this one is a delegate.
     * @type {string}
     */
    @property({ type: String })
    accessor uuid

    /**
     * @type {AbstractZoneSquare[]}
     */
    @property({ type: AbstractZoneSquare, container: DataContainer.ARRAY })
    accessor squares

    constructor({ uuid, squares }) {
        super()
        this.uuid = uuid
        this.squares = squares
    }
}