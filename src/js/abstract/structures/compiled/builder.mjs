/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, property, SerializableStructure } from "../../base.mjs"
import { DataContainer } from "../../container.mjs"
import { AbstractBlock } from "../blocks/block.mjs"
import { AbstractLink } from "../blocks/link.mjs"
import { AbstractZoneDelegate } from "./zone.mjs"

@collect
export class AbstractLogicBuilder extends SerializableStructure {
    /**
     * @type {string}
     */
    @property({ type: String })
    accessor uuid

    /**
     * @type {string[]}
     */
    @property({ type: String, container: DataContainer.ARRAY })
    accessor childBuilders

    /**
     * @type {AbstractZoneDelegate[]}
     */
    @property({ type: AbstractZoneDelegate, container: DataContainer.ARRAY  })
    accessor zonesDelegates

    /**
     * @type {AbstractBlock[]}
     */
    @property({ type: AbstractBlock, container: DataContainer.ARRAY })
    accessor blocks

    /**
     * @type {AbstractLink[]}
     */
    @property({ type: AbstractLink, container: DataContainer.ARRAY })
    accessor links

    constructor({ uuid, childBuilders, zonesDelegates, blocks, links }) {
        super()
        this.uuid = uuid
        this.childBuilders = childBuilders
        this.zonesDelegates = zonesDelegates
        this.blocks = blocks
        this.links = links
    }
}