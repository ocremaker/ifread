/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import { collect, property, SerializableStructure } from "../../base.mjs"
import { Integer } from "../../types/index.mjs"
import { QuillDeltaWithID } from "../delta.mjs"
import { DataContainer } from "../../container.mjs"
import { AbstractZone } from "../zone.mjs"
import { AbstractLogicBuilder } from "./builder.mjs"

@collect
export class CompiledChapterVersion extends SerializableStructure {
    /**
     * @type {string}
     */
    @property({ type: String })
    accessor uuid

    /**
     * @type {string}
     */
    @property({ type: String })
    accessor author

    /**
     * UUID of the parent version of the latest partial version.
     * @type {string|null}
     */
    @property({ type: String, allowNull: true })
    accessor previousVersion

    /**
     * @type {number}
     */
    @property({ type: Integer })
    accessor lastModified

    /**
     * @type {QuillDeltaWithID[]}
     */
    @property({ type: QuillDeltaWithID, container: DataContainer.ARRAY })
    accessor texts

    /**
     * @type {AbstractZone[]}
     */
    @property({ type: AbstractZone, container: DataContainer.ARRAY })
    accessor zones

    /**
     * @type {AbstractLogicBuilder[]}
     */
    @property({ type: AbstractLogicBuilder, container: DataContainer.ARRAY })
    accessor builders

    constructor({ uuid, author, previousVersion, lastModified, texts, zones, builders }) {
        super()
        this.uuid = uuid
        this.author = author
        this.previousVersion = previousVersion
        this.lastModified = lastModified
        this.texts = texts
        this.zones = zones
        this.builders = builders
    }
}