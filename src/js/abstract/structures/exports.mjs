/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { collect, property, SerializableStructure } from "../base.mjs"
import { Enum } from "../types/index.mjs"

export class CSSExportSelector extends Enum {
    /**
     * Uses CSS + (siblings) selectors to create ifs (less accessible, but better supported)
     * @type {string}
     */
    static SIBLINGS = "siblings"
    /**
     * Uses CSS :has (target element within) selectors. Provides better accessibility and data structures,
     * but can be slightly buggier and not quite as well supported.
     * @type {string}
     */
    static HAS = ":has"
}

@collect
export class CSSExportFormatSettings extends SerializableStructure {
    /**
     * @type {string}
     */
    @property({ type: CSSExportSelector })
    accessor selector

    constructor({ selector }) {
        super()
        this.selector = selector
    }

}

@collect
export class ExportFormatsSettings extends SerializableStructure {
    /**
     * @type {CSSExportFormatSettings}
     */
    @property({ type: CSSExportFormatSettings })
    accessor css

    constructor({ css }) {
        super()
        this.css = css
    }
}