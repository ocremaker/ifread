/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Enum } from "./types/index.mjs"

export class DataContainer extends Enum {
    static NONE = "none"
    static ARRAY = "array"
    static SET = "set"
    /**
     * Checks if the data is encapsulated by
     * @param {string} container
     * @param data
     * @return {boolean}
     */
    static isContainedWithin(container, data) {
        switch(container) {
            case DataContainer.ARRAY:
                return Array.isArray(data)
            case DataContainer.SET:
                return (data instanceof Set)
            case DataContainer.NONE:
                return !Array.isArray(data) && !(data instanceof Set)
            default:
                throw new Error(`Unknown data container ${container}.`)
        }
    }
    /**
     * Calls the method for every value of the container.
     * @param {string} container
     * @param data
     * @param {function(any): any} method
     * @return {*|Set|Array}
     */
    static map(container, data, method) {
        switch(container) {
            case DataContainer.ARRAY:
                return data.map(method)
            case DataContainer.SET:
                return new Set(Array.from(data).map(method))
            case DataContainer.NONE:
                return method(data)
            default:
                throw new Error(`Unknown data container ${container}.`)
        }
    }
}