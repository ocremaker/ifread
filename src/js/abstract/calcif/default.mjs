/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {
    ConstantTokenType,
    FunctionTokenType,
    NumberTokenType, ObjectTypeTokenType,
    PunctuationTokenType,
    OperatorTokenType, TernaryOperatorTokenType
} from "./tokentypes/"
import { Type } from "./types.mjs"

export const DEFAULT_TOKEN_TYPES = [
    new NumberTokenType(),
    new PunctuationTokenType("("),
    new PunctuationTokenType(")"),
    new ObjectTypeTokenType("variable-int", Type.INTEGER),
    new ObjectTypeTokenType("variable-bool", Type.BOOLEAN),
    // Constants
    new ConstantTokenType("true", true),
    new ConstantTokenType("false", false),
    new ConstantTokenType("pi", Math.PI),
    new ConstantTokenType("e", Math.E),
    // Number operators
    new OperatorTokenType("+", 50, [
        { operands: [Type.INTEGER, Type.INTEGER], result: Type.INTEGER, call: (x, y) => x + y },
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.DECIMAL, call: (x, y) => x + y }
    ]),
    new OperatorTokenType("-", 50, [
        { operands: [Type.INTEGER, Type.INTEGER], result: Type.INTEGER, call: (x, y) => x - y },
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.DECIMAL, call: (x, y) => x - y }
    ]),
    new OperatorTokenType("*", 100, [
        { operands: [Type.INTEGER, Type.INTEGER], result: Type.INTEGER, call: (x, y) => x * y },
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.DECIMAL, call: (x, y) => x * y }
    ]),
    new OperatorTokenType("/", 100, [
        { operands: [Type.INTEGER, Type.INTEGER], result: Type.INTEGER, call: (x, y) => x / y },
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.DECIMAL, call: (x, y) => x / y }
    ]),
    new OperatorTokenType("%", 100, [
        { operands: [Type.INTEGER, Type.INTEGER], result: Type.INTEGER, call: (x, y) => x % y },
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.DECIMAL, call: (x, y) => x % y }
    ]),
    new OperatorTokenType("mod", 100, [
        { operands: [Type.INTEGER, Type.INTEGER], result: Type.INTEGER, call: (x, y) => x % y },
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.DECIMAL, call: (x, y) => x % y }
    ]),
    new OperatorTokenType("^", 150, [
        { operands: [Type.INTEGER, Type.INTEGER], result: Type.INTEGER, call: Math.pow },
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.DECIMAL, call: Math.pow }
    ]),
    // Boolean operators
    new OperatorTokenType("and", 0, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x && y },
    ]),
    new OperatorTokenType("&&", 0, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x && y },
    ]),
    new OperatorTokenType("or", 0, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x || y },
    ]),
    new OperatorTokenType("||", 0, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x || y },
    ]),
    new OperatorTokenType("xor", 0, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x !== y },
    ]),
    new OperatorTokenType("nor", 0, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => !(x || y) },
    ]),
    new OperatorTokenType("nand", 0, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => !(x && y) },
    ]),
    new OperatorTokenType("xnor", 0, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x === y },
    ]),
    // Value comparators
    new OperatorTokenType("=", 0, [
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.BOOLEAN, call: (x, y) => x === y },
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x === y },
    ]),
    new OperatorTokenType("==", 0, [
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.BOOLEAN, call: (x, y) => x === y },
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x === y },
    ]),
    new OperatorTokenType("!=", 0, [
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.BOOLEAN, call: (x, y) => x !== y },
        { operands: [Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y) => x !== y },
    ]),
    // Number comparaison operators
    new OperatorTokenType(">", 0, [
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.BOOLEAN, call: (x, y) => x > y },
    ]),
    new OperatorTokenType("<", 0, [
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.BOOLEAN, call: (x, y) => x < y },
    ]),
    new OperatorTokenType(">=", 0, [
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.BOOLEAN, call: (x, y) => x >= y },
    ]),
    new OperatorTokenType("<=", 0, [
        { operands: [Type.NUMBER, Type.NUMBER], result: Type.BOOLEAN, call: (x, y) => x <= y },
    ]),
    // Ternary operators
    new TernaryOperatorTokenType("?", ":", -50, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y, z) => x ? y : z },
        { operands: [Type.BOOLEAN, Type.INTEGER, Type.INTEGER], result: Type.INTEGER, call: (x, y, z) => x ? y : z },
        { operands: [Type.BOOLEAN, Type.NUMBER, Type.NUMBER], result: Type.DECIMAL, call: (x, y, z) => x ? y : z }
    ]),
    new TernaryOperatorTokenType("if", "else", -50, [
        { operands: [Type.BOOLEAN, Type.BOOLEAN, Type.BOOLEAN], result: Type.BOOLEAN, call: (x, y, z) => y ? x : z },
        { operands: [Type.INTEGER, Type.BOOLEAN, Type.INTEGER], result: Type.INTEGER, call: (x, y, z) => y ? x : z },
        { operands: [Type.NUMBER, Type.BOOLEAN, Type.NUMBER], result: Type.DECIMAL, call: (x, y, z) => y ? x : z }
    ]),
    // Functions
    new FunctionTokenType("!", Type.BOOLEAN, Type.BOOLEAN, x => !x),
    new FunctionTokenType("not", Type.BOOLEAN, Type.BOOLEAN, x => !x),
    new FunctionTokenType("round", Type.NUMBER, Type.INTEGER, Math.round),
    new FunctionTokenType("floor", Type.NUMBER, Type.INTEGER, Math.floor),
    new FunctionTokenType("ceil", Type.NUMBER, Type.INTEGER, Math.ceil),
    new FunctionTokenType("sin", Type.NUMBER, Type.DECIMAL, Math.sin),
    new FunctionTokenType("cos", Type.NUMBER, Type.DECIMAL, Math.cos),
    new FunctionTokenType("tan", Type.NUMBER, Type.DECIMAL, Math.tan)
]