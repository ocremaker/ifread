/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Token, TokensError, TokenStream } from "./token.mjs"
import {
    buildOperationStreamAST,
    SymbolTokenType,
    HUMAN_READABLE_OBJECT_TYPES,
    ObjectTypeTokenType, UnknownIdentifierTokenType
} from "./tokentypes/index.mjs"
import { containsFlag } from "../../utils/index.mjs"
import { getHumanReadableOfType, Type } from "./types.mjs"
import { isValueTypeInvalid } from "./ast/index.mjs"

const fuzzyThreshold = 0.3

/**
 * @typedef {object} CalcIfErrorParams
 *
 * @property {Token[]} tokens - List of tokens of the expression.
 * @property {TokenType[]} tokenTypes - List of valid token types.
 * @property {CalcIfValueType} expectedType - Expected output type
 * @property {Map.<string, Map.<string, number|boolean>>} objectValues - Values of the objects by their type > internal value.
 * @property {TokensError[]?} errors - List of errors.
 */

/**
 * Returns the closest fuzzy match of forString among the given list.
 * @param {string} forString
 * @param {string[]} among
 * @returns {string[]}
 */
function findClosestMatches(forString, among) {
    // Sort by fuzzyMatching
    const sortedRecommendations = among.map(t => [t, t.fuzzyMatch(forString)])
                                                       .sort((a, b) => b[1] - a[1])
    return sortedRecommendations.filter(r => r[1] > fuzzyThreshold).map(r => r[0])
}

function isTokenStreamEmpty(params) {
    const { tokens, errors } = params
    if(tokens.length === 0) {
        const msg = "Expression is <b>empty</b>."
        errors.push(new TokensError(msg, []))
    }
}

function areThereAnyUnknownTokens(params) {
    const { tokens, errors, tokenTypes } = params
    const unknownTokens = tokens.filter(t => t.type === "unknown")
    for(const t of unknownTokens) {
        const symbols = tokenTypes.filter(t => t instanceof SymbolTokenType).map(t => t.symbol)
        const matches = findClosestMatches(t.value, symbols)
        let msg = `Symbol <b>${t.value}</b> is unknown and could not be parsed.`
        if(matches.length > 0)
            msg += ` Did you mean '${matches.join("' or '")}'?`
        errors.push(new TokensError(msg, [tokens.indexOf(t)]))
    }
}

function areThereAnyUnknownIdentifiers(params) {
    const { tokens, errors, tokenTypes } = params
    const unknownTokens = tokens.filter(t => t.type === "identifier")
    for(const t of unknownTokens) {
        const identifiers = tokenTypes.filter(t => t instanceof SymbolTokenType && UnknownIdentifierTokenType.REGEXP.test(t.symbol))
                                            .map(t => t.symbol)
        const matches = findClosestMatches(t.value, identifiers)
        let msg = `String of text <b>${t.value}</b> does not match any known function, operator or value.`
        if(matches.length > 0)
            msg += ` Did you mean '${matches.join("' or '")}'?`
        errors.push(new TokensError(msg, [tokens.indexOf(t)]))
    }
}

function isInvalidObjectToken(t, objectValues) {
    let invalid = false
    if(t.tokenType instanceof ObjectTypeTokenType) {
        const value = objectValues.get(t.type)?.get(ObjectTypeTokenType.getInternalValue(t.value))
        invalid = isValueTypeInvalid(value, t.tokenType.valueType)
    }
    return invalid
}

function areThereAnyInvalidObjects(params) {
    const { tokens, errors, objectValues } = params
    const undefinedObjectTypesTokens = tokens.filter(t => isInvalidObjectToken(t, objectValues))
    const undefinedTypes = new Set(undefinedObjectTypesTokens.map(t => t.type))
    for(const undefinedType of undefinedTypes) {
        const tokenIndexes = undefinedObjectTypesTokens.filter(t => t.type === undefinedType).map(t => tokens.indexOf(t))
        const objectName = HUMAN_READABLE_OBJECT_TYPES.get(undefinedType)
        let msg = tokenIndexes.length === 1 ? `There is an <b>undefined ${objectName}</b>.` : `There are <b>undefined ${objectName}s</b>.`
        errors.push(new TokensError(msg, tokenIndexes))
    }
}

function checkAST(params) {
    const { tokens, errors } = params
    try {
        if(errors.length === 0)
            params.ast = buildOperationStreamAST(new TokenStream(tokens))
    } catch(e) {
        if(e instanceof TokensError)
            errors.push(e)
        else
            throw e
    }
}

function checkASTForTypeErrors(params) {
    if(params.ast !== undefined) {
        const { ast, errors, expectedType } = params
        try {
            const outputType = ast.computeType()
            if(!containsFlag(expectedType, outputType)) {
                const hrExpected = getHumanReadableOfType(expectedType)
                const hrOutput = getHumanReadableOfType(outputType)
                let msg = `Expression's result is a <b>${hrOutput}</b> though it should be a ${hrExpected}.`
                if(expectedType === Type.INTEGER && outputType === Type.DECIMAL)
                    msg += " Tip: You can use a function like 'round' for conversion."
                errors.push(new TokensError(msg, []))
            }
        } catch(e) {
            if(e instanceof TokensError)
                errors.push(e)
            else
                throw e
        }
    }
}

const TESTS = [
    isTokenStreamEmpty,
    areThereAnyUnknownTokens,
    areThereAnyUnknownIdentifiers,
    areThereAnyInvalidObjects,
    checkAST,
    checkASTForTypeErrors
]

/**
 * Checks constructed tokens for errors and returns an array of them.
 * @param {CalcIfErrorParams} params
 * @returns {TokensError[]}
 */
export function identifyErrors(params) {
    params.errors = []
    for(const test of TESTS)
        test(params)
    return params.errors
}