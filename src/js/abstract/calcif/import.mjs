/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { UnknownTokenType } from "./tokentypes/"
import { Token } from "./token.mjs"

const UNKNOWN_TOKEN = new UnknownTokenType()

/**
 * Imports a serialized (JSON stringified and parsed) token into a Token.
 * @param {{type: string, value: string}} token
 * @param {TokenType[]} tokenTypes
 */
export function importToken(token, tokenTypes) {
    const tokenType = tokenTypes.find(t => t.tokenName === token.type && t.isValidFor(token.value))
    return new Token(tokenType ?? UNKNOWN_TOKEN, token.value)
}