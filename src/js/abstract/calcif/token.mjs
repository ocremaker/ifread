/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export class Token {
    #tokenType

    /**
     *
     * @param {TokenType} tokenType
     * @param {string} value
     */
    constructor(tokenType, value) {
        this.#tokenType = tokenType
        this.value = value
        this.type = tokenType.tokenName
    }

    /**
     * @return {TokenType}
     */
    get tokenType() { return this.#tokenType }
}

export class TokenStream extends Array {
    /**
     *
     * @param {Token[]} stream
     */
    constructor(stream) {
        super()
        this.push(...stream)
        this.currentIndex = 0
    }

    /**
     * @return {Token}
     */
    get currentToken() { return this[this.currentIndex] }

    /**
     * Returns true if end of stream has been reached.
     * @return {boolean}
     */
    hasEnded() {
        return this.currentIndex >= this.length
    }
}

export class TokensError {
    constructor(message, tokenIndexes) {
        this.message = message
        this.overTokens = tokenIndexes
    }
}