/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ASTElement } from "./base.mjs"

export class ASTSubExpression extends ASTElement {
    /**
     *
     * @param {number} openTokenIndex - Index of the opening parenthesis.
     * @param {ASTElement} wrappedAst - Value within the parentheses.
     * @param {number} closeTokenIndex - Index of the closing parenthesis.
     */
    constructor(openTokenIndex, wrappedAst, closeTokenIndex) {
        super([openTokenIndex, ...wrappedAst.tokenIndexes, closeTokenIndex])
        this.openTokenIndex = openTokenIndex
        this.wrappedAst = wrappedAst
        this.closeTokenIndex = closeTokenIndex
    }

    evaluate(params) {
        return this.wrappedAst.evaluate(params)
    }

    computeType() {
        return this.wrappedAst.computeType()
    }
}