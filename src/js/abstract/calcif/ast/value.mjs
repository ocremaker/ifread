/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ASTElement } from "./base.mjs"
import { Type } from "../types.mjs"

class ASTValue extends ASTElement {
    /**
     * @param {number} tokenIndex - Index of the token.
     * @param {number|boolean} value
     */
    constructor(tokenIndex, value) {
        super([tokenIndex])
        this.value = value
    }

    evaluate(params) {
        return this.value
    }
}

export class ASTBoolean extends ASTValue {
    /**
     * @param {number} tokenIndex
     * @param {boolean} value
     */
    constructor(tokenIndex, value) {
        super(tokenIndex, value)
    }

    computeType() {
        return Type.BOOLEAN
    }
}


export class ASTNumber extends ASTValue {
    /**
     * @param {number} tokenIndex
     * @param {number} value
     */
    constructor(tokenIndex, value) {
        super(tokenIndex, value)
    }

    computeType() {
        return Number.isInteger(this.value) ? Type.INTEGER : Type.DECIMAL
    }
}