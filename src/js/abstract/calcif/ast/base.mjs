/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { Type } from "../types.mjs"

/**
 * Base class for Abstract Syntax Tree elements.
 * @abstract
 */
export class ASTElement {

    /**
     * @param {number[]} tokenIndexes - Indexes of the tokens required to create this AST element..
     */
    constructor(tokenIndexes) {
        this.tokenIndexes = tokenIndexes
    }

    /**
     * Yields the value this AST element returns. NOTE: You should check for type and other errors AHEAD of evaluation.
     * @param {Map.<string, Map.<string, number|boolean>>} objectValues
     * @returns {number|boolean}
     */
    evaluate({ objectValues }) {
        throw new Error("Not implemented.")
    }

    /**
     * Yields the type of value this AST element returns.
     * @throws {TokensError} - When type issues are detected within the tree.
     * @returns {CalcIfValueType} - Return SINGULAR (non-composed) value type flag.
     */
    computeType() {
        throw new Error("Not implemented.")
    }
}

/**
 * Checks if the given value is invalid and returns true if so.
 * @param {number|boolean} value
 * @param {CalcIfValueType} type
 * @return {boolean}
 */
export function isValueTypeInvalid(value, type) {
    let valid
    if(type === Type.INTEGER) {
        valid = typeof value === "number" && Number.isInteger(value) && value >= 0
    } else if(type === Type.DECIMAL) {
        valid = typeof value === "number" && (!Number.isInteger(value) || value < 0)
    } else if(type === Type.BOOLEAN) {
        valid = typeof value === "boolean"
    } else
        valid = true
    return !valid
}

/**
 * Returns the type of value.
 * @param {number|boolean} value
 * @returns {CalcIfValueType}
 */
export function typeOfValue(value) {
    let result
    if(typeof value === "number" && Number.isInteger(value) && value >= 0)
        result = Type.INTEGER
    else if(typeof value === "number" && (!Number.isInteger(value) || value < 0))
        result = Type.DECIMAL
    else if(typeof value === "boolean")
        result = Type.BOOLEAN
    else
        throw new Error(`Invalid argument passed: ${value}`)
    return result
}