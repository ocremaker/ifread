/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ASTElement } from "./base.mjs"
import { TokensError } from "../token.mjs"
import { getHumanReadableOfType } from "../types.mjs"
import { containsFlag } from "../../../utils/index.mjs"

export class ASTFunction extends ASTElement {
    /**
     *
     * @param {number} tokenIndex - Index of the function token.
     * @param {string} functionName
     * @param {ASTElement} astValue
     * @param {CalcIfValueType} parameterType
     * @param {CalcIfValueType} returnType
     * @param {(function(number): number)|(function(boolean): boolean)} functionToCall
     */
    constructor(tokenIndex, functionName, astValue, parameterType, returnType, functionToCall) {
        super([tokenIndex, ...astValue.tokenIndexes])
        this.functionName = functionName
        this.astValue = astValue
        this.parameterType = parameterType
        this.returnType = returnType
        this.functionToCall = functionToCall
    }

    evaluate(params) {
        return this.functionToCall(this.astValue.evaluate(params))
    }

    computeType() {
        const paramType = this.astValue.computeType()
        if(!containsFlag(this.parameterType, paramType)) {
            const hrParameter = getHumanReadableOfType(paramType)
            const hrRequired = getHumanReadableOfType(this.parameterType)
            const msg = `Parameter of function '<b>${this.functionName}</b>' must be a ${hrRequired}, not a ${hrParameter}.`
            throw new TokensError(msg, this.tokenIndexes)
        }
        return this.returnType
    }
}