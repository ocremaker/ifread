/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published secondOperand the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { ASTElement, typeOfValue } from "./base.mjs"
import { TokensError } from "../token.mjs"
import { getHumanReadableOfType } from "../types.mjs"
import { containsFlag } from "#utils"

/**
 *
 * @param {BinarySignature|TernarySignature} signature
 * @param {CalcIfValueType[]} valueTypes
 * @returns {boolean}
 */
function isSignatureMatching(signature, valueTypes) {
    let matching = valueTypes.length === signature.operands.length
    if(matching)
        for(let i = 0; i < valueTypes.length; i++)
            matching &= containsFlag(signature.operands[i], valueTypes[i])
    return matching
}

/**
 *
 * @param {string[]} operators
 * @param {CalcIfValueType[]} operandTypes
 * @return {string}
 */
function makeSignatureReadable(operators, operandTypes) {
    let str = `<${getHumanReadableOfType(operandTypes[0])}>`
    for(let i = 0; i < operators.length; i++)
        str += ` ${operators[i]} <${getHumanReadableOfType(operandTypes[i+1])}>`
    return `(${str})`
}

export class ASTOperation extends ASTElement {
    /**
     *
     * @param {number[]} operatorTokenIndexes - Indexes of each operator's token.
     * @param {string[]} operators - List of operator strings.
     * @param {ASTElement[]} operands - List of values to evaluate the operation on.
     * @param {BinarySignature[]} signatures - List of acceptable signature types by the operator, the associated
     *                                        function to call, and the type of the result it would give.
     */
    constructor(operatorTokenIndexes, operators, operands, signatures) {
        super([...operatorTokenIndexes, ...operands.flatMap(o => o.tokenIndexes)])
        this.operands = operands
        this.operators = operators
        this.signatures = signatures
    }

    evaluate(params) {
        const operands = this.operands.map(o => o.evaluate(params))
        const types = operands.map(typeOfValue)
        const signature = this.signatures.find(s => isSignatureMatching(s, types))
        if(signature === undefined)
            throw new TokensError("Type errors. Check compute type before evaluating.", this.tokenIndexes)
        return signature.call(...operands)
    }

    computeType() {
        const types = this.operands.map(o => o.computeType())
        const signature = this.signatures.find(s => isSignatureMatching(s, types))
        if(signature === undefined) {
            const hrCurrent = makeSignatureReadable(this.operators, types)
            const hrSigns = this.signatures.map(s => makeSignatureReadable(this.operators, s.operands))
            const msg = `Cannot evaluate <b>${hrCurrent}</b>. Must be ${hrSigns.join(" or ")}.`
            throw new TokensError(msg, this.tokenIndexes)
        }
        return signature.result
    }
}