/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ASTElement } from "./base.mjs"
import { TokensError } from "../token.mjs"

export class ASTObject extends ASTElement {
    /**
     * @param {number} tokenIndex
     * @param {string} internalValue
     * @param {CalcIfValueType} objectType
     */
    constructor(tokenIndex, internalValue, objectType) {
        super([tokenIndex])
        this.internalValue = internalValue
        this.objectType = objectType
    }

    evaluate({ objectValues }) {
        const category = objectValues.get(this.tokenName)
        if(category === undefined)
            throw new TokensError( `Undefined object category '<b>${this.tokenName}</b>'.`, this.tokenIndexes)
        const value = category.get(this.internalValue)
        if(value === undefined)
            throw new TokensError( `Undefined object '<b>${this.internalValue}</b>' within category '${this.tokenName}'.`, this.tokenIndexes)
        return value
    }

    computeType() {
        return this.objectType
    }
}