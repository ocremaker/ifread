/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Token } from "./token.mjs"
import { UnknownIdentifierTokenType, UnknownTokenType } from "./tokentypes/"

const UNKNOWN_TOKEN = new UnknownTokenType()
const UNKNOWN_IDENTIFIER_TOKEN = new UnknownIdentifierTokenType()

class Parser {
    /**
     *
     * @param {string} input
     * @param {TokenType[]} tokenTypes
     */
    constructor(input, tokenTypes) {
        this.input = input;
        this.tokenTypes = tokenTypes;
        this.currentPosition = 0;
        this.tokens = []
    }

    /**
     * @return {Token[]}
     */
    parse() {
        while(this.currentPosition < this.input.length) {
            this._parseSingle()
        }
        return this.tokens
    }

    /**
     * Searches for a token which can be started at this position, and adds all characters until it finds at least one
     * into a new "Unknown" token.
     * @return {TokenType[]}
     * @private
     */
    _parseUnknownUntilTokenIsFound() {
        let undefinedChars = ""
        let matchedTokenTypes
        do {
            const char = this.input[this.currentPosition]
            matchedTokenTypes = this.tokenTypes.filter(t => t.canStartWith(char))
            if(matchedTokenTypes.length === 0) {
                undefinedChars += char
                this.currentPosition++
            }
        } while(matchedTokenTypes.length === 0 && this.currentPosition < this.input.length)
        if(undefinedChars !== "")
            this.tokens.push(new Token(UNKNOWN_TOKEN, undefinedChars))
        return matchedTokenTypes
    }

    /**
     * Parses a singular valid token if possible.
     * @private
     */
    _parseSingle() {
        const matchedTokenTypes = this._parseUnknownUntilTokenIsFound()
        if(matchedTokenTypes.length > 0) {
            this._parseSingleDirect(matchedTokenTypes)
        }
    }

    /**
     * Parses the current text and finds a valid token type for the parsed token.
     * @param {TokenType[]} validTokenTypes
     * @private
     */
    _parseSingleDirect(validTokenTypes) {
        const isIdentifier = UnknownIdentifierTokenType.REGEXP.test(this.input[this.currentPosition])
        const unknownTokenType = isIdentifier ? UNKNOWN_IDENTIFIER_TOKEN : UNKNOWN_TOKEN
        let parsed = ""
        let current = this.input[this.currentPosition]
        let matchedTokenTypes = validTokenTypes
        const validate = isIdentifier ?
            () => unknownTokenType.accepts(current, parsed) :
            () => matchedTokenTypes.some(t => t.accepts(current, parsed))
        do {
            matchedTokenTypes = matchedTokenTypes.filter(t => t.accepts(current, parsed))
            parsed += current
            this.currentPosition++
            current = this.input[this.currentPosition]
        } while(this.currentPosition < this.input.length && validate())
        // Check for which one fulfills the obligation completely:
        matchedTokenTypes = matchedTokenTypes.filter(t => t.isValidFor(parsed))
        if(matchedTokenTypes.length > 1)
            throw new Error("[SHOULD NOT HAPPEN] Two or more token types valid for token value.")
        this.tokens.push(new Token(matchedTokenTypes[0] ?? unknownTokenType, parsed))
    }
}

/**
 * Parses the expression into a set of tokens.
 * @param {string} str
 * @param {TokenType[]} tokenTypes
 * @return {Token[]}
 */
export function parse(str, tokenTypes) {
    const parser = new Parser(str, tokenTypes)
    return parser.parse()
}