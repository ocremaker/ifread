/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SymbolTokenType } from "./base.mjs"
import { ASTSubExpression } from "../ast/index.mjs"
import { buildOperationStreamAST } from "./operator.mjs"
import { TokensError } from "../token.mjs"

export class PunctuationTokenType extends SymbolTokenType {
    get tokenName() {
        return "punct"
    }

    constructor(punct) {
        super(punct)
    }

    buildAst(stream) {
        let ast
        switch(this.symbol) {
            case ")":
                throw new TokensError("Unexpected closing parenthesis <b>)</b>.")
            case "(":
                const startIdx = stream.currentIndex
                stream.currentIndex++
                let inner = null
                if(!stream.hasEnded())
                    inner = buildOperationStreamAST(stream, ")")
                if(stream.hasEnded() || stream.currentToken.value !== ")") {
                    // range(startIdx, stream.currentIndex)
                    const indexes = Array(stream.currentIndex - startIdx).fill(startIdx).map((x, i) => x + i)
                    const msg = `Subexpression started by <b>(</b> not closed. Found ${stream.currentToken?.value ?? "the end of the expression"} instead.`
                    throw new TokensError(msg, indexes)
                }
                if(inner === null) {
                    const indexes = [startIdx, startIdx+1]
                    const msg = `Subexpression started by <b>(</b> must contain a value.`
                    throw new TokensError(msg, indexes)
                }
                ast = new ASTSubExpression(startIdx, inner, stream.currentIndex)
                // stream.currentIndex++ // Disabled, because operations add one anyway.
        }
        return ast
    }
}