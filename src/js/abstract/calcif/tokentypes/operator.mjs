/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SymbolTokenType, TokenType } from "./base.mjs"
import { ASTOperation } from "../ast/index.mjs"
import { TokenStream, Token, TokensError } from "../token.mjs"


/**
 * @typedef {object} BinarySignature
 * @property {CalcIfValueType} result - Type the operation signature results to.
 * @property {CalcIfValueType[]} operands - Required types of the values of the first operand.
 * @property {function(number|boolean, number|boolean): number|boolean} call - Function to call to evaluate the operator.
 */

/**
 * @typedef {object} TernarySignature
 * @extends BinarySignature
 * @property {function(number|boolean, number|boolean, number|boolean): number|boolean} call - Function to call to evaluate the operator.
 */

export class OperatorTokenType extends SymbolTokenType {
    get tokenName() {
        return "operator"
    }

    /**
     * Class for operators like '+', '%', or 'and'
     * @param {string} operator - Symbol of the operator.
     * @param {number} priority - Priority of the operator compared to others.
     * @param {BinarySignature[]} signatures - List of acceptable signature types by the operator, the associated
     *                                        function to call, and the type of the result it would give.
     */
    constructor(operator, priority, signatures) {
        super(operator)
        this.signatures = signatures
        this.priority = priority
    }
}

export class TernaryOperatorTokenType extends TokenType {
    get tokenName() {
        return "operator"
    }

    /**
     * Class for operators like '<value> if <condition> else <value>', '<condition> ? <value> : <value>'
     * @param {string} firstOperator - Symbol of the operator.
     * @param {string} secondOperator - Symbol of the operator.
     * @param {number} priority - Priority of the operator compared to others.
     * @param {TernarySignature[]} signatures - List of acceptable signature types by the operator, the associated
     *                                          function to call, and the type of the result it would give.
     */
    constructor(firstOperator, secondOperator, priority, signatures) {
        super()
        this.firstOperator = firstOperator
        this.secondOperator = secondOperator
        this.priority = priority
        this.signatures = signatures
    }

    /**
     * Returns true if this token can be started with the given character
     * @param {string} character
     * @returns {boolean}
     */
    canStartWith(character) {
        return this.firstOperator.startsWith(character) || this.secondOperator.startsWith(character)
    }

    /**
     * Returns true if the token accepts this character being appended to the end.
     * @param {string} newCharacter
     * @param {string} context - Current value of the token
     * @returns {boolean}
     */
    accepts(newCharacter, context) {
        const full = context + newCharacter
        return this.firstOperator.startsWith(full) || this.secondOperator.startsWith(full)
    }

    /**
     * Returns true if the given value is valid to form this token.
     * @param {string} tokenValue
     * @returns {boolean}
     */
    isValidFor(tokenValue) {
        return this.firstOperator === tokenValue || this.secondOperator === tokenValue
    }
}



/**
 * Parses an expression into an Abstract Syntax Tree.
 * @param {TokenStream} stream
 * @param {string} endsWith - (Optional) string to stop parsing EOF for end of expression, token value otherwise.
 * @returns {ASTElement}
 */
export function buildOperationStreamAST(stream, endsWith = null) {
    /** @type {{token: Token, index: number, tokenType: OperatorTokenType}[]} */
    const operatorTokens = []
    /** @type {ASTElement[]} */
    const valueAsts = []
    let nextIsValue = true
    // Accumulate values and operators into their respective arrays.
    while(!stream.hasEnded() && stream.currentToken.value !== endsWith) {
        const token = stream.currentToken
        if(nextIsValue) {
            if(token.type === "operator")
                throw new TokensError(`Expected value, not operator <b>${token.value}</b>.`, [stream.currentIndex])
            valueAsts.push(token.tokenType.buildAst(stream))
        } else {
            if(token.type !== "operator")
                throw new TokensError(`Expected operator, not ${token.type} <b>${token.value}</b>.`, [stream.currentIndex])
            operatorTokens.push({ token, index: stream.currentIndex, tokenType: token.tokenType })
        }
        stream.currentIndex++
        nextIsValue = !nextIsValue
    }
    if(nextIsValue) {
        // Ended on an operator.
        const EOF = "end of expression"
        const humanToken = stream.hasEnded() ? `<b>${EOF}</b>` : `${stream.currentToken?.type ?? ""} <b>${stream.currentToken?.value ?? EOF}</b>`
        const idx = stream.hasEnded() ? stream.currentIndex - 1 : stream.currentIndex
        throw new TokensError(`Expected value after operator, not ${humanToken}.`, [idx])
    }
    const operatorsByPriority = operatorTokens.toSorted((o1, o2) => o2.tokenType.priority - o1.tokenType.priority) // From highest to lowest priority
    for(const op of operatorsByPriority) {
        const opIdx = operatorTokens.indexOf(op)
        if(opIdx !== -1) { // Check if it hasn't already been removed.
            if(op.tokenType instanceof TernaryOperatorTokenType) {
                // Ternary operator have the lowest of priorities.
                if(op.token.value === op.tokenType.secondOperator) {
                    const msg = `Second operator '<b>${op.token.value}</b>' used before its precessor '${op.tokenType.firstOperator}'.`
                    throw new TokensError(msg, [op.index])
                } else {
                    const nextOp = operatorTokens[opIdx + 1]
                    if(nextOp?.tokenType !== op.tokenType) {
                        const nextOpIdx = operatorTokens[opIdx + 1]?.index ?? stream.length
                        // range(opIdx, nextOpIdx)
                        const indexes = Array(nextOpIdx - opIdx).fill(opIdx).map((x, i) => x + i)
                        const msg = `First operator '<b>${op.token.value}</b>' must be followed by a value and its associated second '${op.tokenType.secondOperator}'.`
                        throw new TokensError(msg, indexes)
                    }
                    const operands = valueAsts.slice(opIdx, opIdx + 3)
                    const operation = new ASTOperation(
                        [op.index, nextOp.index],
                        [op.token.value, nextOp.token.value],
                        operands, op.tokenType.signatures
                    )
                    valueAsts.splice(opIdx, 3, operation)
                    operatorTokens.splice(opIdx, 2)
                }
            } else {
                // Binary operator.
                const operands = valueAsts.slice(opIdx, opIdx + 2)
                const operation = new ASTOperation([op.index], [op.token.value], operands, op.tokenType.signatures)
                valueAsts.splice(opIdx, 2, operation)
                operatorTokens.splice(opIdx, 1)
            }
        }
    }
    return valueAsts[0]
}