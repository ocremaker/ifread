/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { TokensError } from "../token.mjs"
import { TokenType } from "./base.mjs"


export class UnknownTokenType extends TokenType {
    get tokenName() {
        return "unknown"
    }

    canStartWith(character) {
        return true
    }

    accepts(newCharacter, context) {
        return true
    }

    isValidFor(tokenValue) {
        return true
    }

    buildAst(stream) {
        throw new TokensError("Cannot build AST with unknown tokens.", [stream.currentIndex])
    }
}

/**
 * Tokens of type 'identifier' are created when no known identifier match the given value (and the expression is used to match identifiers).
 */
export class UnknownIdentifierTokenType extends TokenType {
    static REGEXP = /^[a-z][a-z _]*$/i

    get tokenName() {
        return "identifier"
    }

    /**
     * Returns true if this token can be started with the given character
     * @param {string} character
     * @returns {boolean}
     */
    canStartWith(character) {
        return this.constructor.REGEXP.test(character)
    }

    /**
     * Returns true if the token accepts this character being appended to the end.
     * @param {string} newCharacter
     * @param {string} context - Current value of the token
     * @returns {boolean}
     */
    accepts(newCharacter, context) {
        return this.constructor.REGEXP.test(context + newCharacter)
    }

    /**
     * Returns true if the given value is valid to form this token.
     * @param {string} tokenValue
     * @returns {boolean}
     */
    isValidFor(tokenValue) {
        return this.constructor.REGEXP.test(tokenValue)
    }

    /**
     * Builds an abstract syntax tree for this token based on a stream of tokens.
     * @param {TokenStream} stream
     * @throws {TokensError} - When an error occurs during the building of the AST.
     * @returns {ASTElement}
     */
    buildAst(stream) {
        throw new TokensError("Cannot build AST with unknown tokens.", [stream.currentIndex])
    }
}