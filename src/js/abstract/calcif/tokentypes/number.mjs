/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { TokenType } from "./base.mjs"
import { ASTNumber } from "../ast/index.mjs"

const ZERO_CHARCODE = "0".charCodeAt(0)
const NINE_CHARCODE = "9".charCodeAt(0)
const REG = /[0-9]+(\.[0-9]*)?/

export class NumberTokenType extends TokenType {
    get tokenName() {
        return "number"
    }

    isDigit(character) {
        const charCode = character.charCodeAt(0)
        return charCode >= ZERO_CHARCODE && charCode <= NINE_CHARCODE
    }

    canStartWith(character) {
        return this.isDigit(character)
    }

    accepts(newCharacter, context) {
        return this.isDigit(newCharacter) || (newCharacter === "." && !context.includes("."))
    }

    isValidFor(tokenValue) {
        return REG.test(tokenValue)
    }

    buildAst(stream) {
        return new ASTNumber(stream.currentIndex, parseFloat(stream.currentToken.value))
    }
}