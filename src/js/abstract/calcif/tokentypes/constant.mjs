/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SymbolTokenType } from "./base.mjs"
import { ASTBoolean, ASTNumber } from "../ast/index.mjs"
import { TokensError } from "../token.mjs"

export class ConstantTokenType extends SymbolTokenType {
    get tokenName() {
        return "constant"
    }

    /**
     * Class for singular identifier constants like 'true', 'false', or 'pi'.
     * @param {string} constantName - Name of the constant.
     * @param {number|boolean} value
     */
    constructor(constantName, value) {
        super(constantName)
        this.value = value
    }

    buildAst(stream) {
        if(typeof this.value === "number")
            return new ASTNumber(stream.currentIndex, this.value)
        else if(typeof this.value === "boolean")
            return new ASTBoolean(stream.currentIndex, this.value)
        else
            throw new TokensError(`Constant '<b>${this.symbol}</b>' value ${JSON.stringify(this.value)} is invalid.`, [stream.currentIndex])
    }
}