/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SymbolTokenType } from "./base.mjs"
import { ASTFunction } from "../ast/index.mjs"
import { TokensError } from "../token"


export class FunctionTokenType extends SymbolTokenType {
    get tokenName() {
        return "function"
    }

    /**
     * Class for singular argument functions like '!' or 'round'.
     * @param {string} functionSymbol - Name of the function.
     * @param {CalcIfValueType} parameterType
     * @param {CalcIfValueType} resultType
     * @param {(function(number): number)|(function(boolean): boolean)} call - Function to call.
     */
    constructor(functionSymbol, parameterType, resultType, call) {
        super(functionSymbol)
        this.parameterType = parameterType
        this.resultType = resultType
        this.call = call
    }

    buildAst(stream) {
        const functionTokenIdx = stream.currentIndex
        const nextToken = stream[stream.currentIndex + 1]
        if(nextToken === undefined)
            throw new TokensError(`No parameter provided to function '<b>${this.symbol}</b>'.`, [stream.currentIndex])
        if(nextToken.type === "operator")
            throw new TokensError(`Cannot pass operator as parameter of function '<b>${this.symbol}</b>'.`, [stream.currentIndex, stream.currentIndex + 1])
        stream.currentIndex++
        const parameter = nextToken.tokenType.buildAst(stream)
        return new ASTFunction(functionTokenIdx, this.symbol, parameter, this.parameterType, this.resultType, this.call)
    }
}