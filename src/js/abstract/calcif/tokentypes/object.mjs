/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { TokenType } from "./base.mjs"
import { TokensError } from "../token.mjs"
import { ASTObject } from "../ast/index.mjs"

export const HUMAN_READABLE_OBJECT_TYPES = new Map([
    ["variable-int", "integer variable"],
    ["variable-bool", "boolean variable"]
])

export class ObjectTypeTokenType extends TokenType {
    #tokenName
    static REG = /^\{([a-z-]+):([^}]+)}$/i

    /**
     * Returns the internal value of the token.
     * @param {string} tokenValue
     * @return {string}
     */
    static getInternalValue(tokenValue) {
        return this.REG.exec(tokenValue)[2]
    }

    get tokenName() {
        return this.#tokenName
    }

    /**
     * Class for tokens with custom HTML implementation whose value is internal that can be built through identifiers.
     * @note Internal values must NOT contain the "{" symbol in their name OR Value.
     * @param {string} tokenName - Identifier for this type of object.
     * @param {CalcIfValueType} valueType - The type of value returned by the object.
     */
    constructor(tokenName, valueType) {
        super()
        this.#tokenName = tokenName
        this.valueType = valueType
    }

    /**
     * Returns true if this token can be started with the given character
     * @param {string} character
     * @returns {boolean}
     */
    canStartWith(character) {
        return "{" === character
    }

    /**
     * Returns true if the token accepts this character being appended to the end.
     * @param {string} newCharacter
     * @param {string} context - Current value of the token
     * @returns {boolean}
     */
    accepts(newCharacter, context) {
        const partialTokenName = (context + newCharacter).substring(1).split(":")[0]
        const startsWithBrace = (context.startsWith("{") || (newCharacter === "{" && context === ""))
        return startsWithBrace && this.#tokenName.startsWith(partialTokenName) && !context.endsWith("}")
    }

    /**
     * Returns true if the given value is valid to form this token.
     * @param {string} tokenValue
     * @returns {boolean}
     */
    isValidFor(tokenValue) {
        return tokenValue.startsWith(`{${this.#tokenName}:`) && this.constructor.REG.test(tokenValue)
    }

    /**
     * Builds an abstract syntax tree for this token based on a stream of tokens.
     * @param {TokenStream} stream
     * @returns {ASTElement}
     */
    buildAst(stream) {
        return new ASTObject(stream.currentIndex, stream.currentToken.value, this.valueType)
    }
}
