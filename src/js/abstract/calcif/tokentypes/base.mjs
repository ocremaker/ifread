/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { TokensError } from "../token.mjs"

/**
 * Base class for registrable token types.
 *
 * @abstract
 */
export class TokenType {
    get tokenName() {
        throw new Error("Not implemented.")
    }

    /**
     * Returns true if this token can be started with the given character
     * @param {string} character
     * @returns {boolean}
     */
    canStartWith(character) {
        throw new Error("Not implemented.")
    }

    /**
     * Returns true if the token accepts this character being appended to the end.
     * @param {string} newCharacter
     * @param {string} context - Current value of the token
     * @returns {boolean}
     */
    accepts(newCharacter, context) {
        throw new Error("Not implemented.")
    }

    /**
     * Returns true if the given value is valid to form this token.
     * @param {string} tokenValue
     * @returns {boolean}
     */
    isValidFor(tokenValue) {
        throw new Error("Not implemented.")
    }

    /**
     * Builds an abstract syntax tree for this token based on a stream of tokens.
     * @param {TokenStream} stream
     * @throws {TokensError} - When an error occurs during the building of the AST.
     * @returns {ASTElement}
     */
    buildAst(stream) {
        throw new Error("Not implemented.")
    }
}

export class SymbolTokenType extends TokenType {
    /**
     * Base class for all tokens made from symbols
     * @param {string} symbol - String representing the exact symbol of the token.
     */
    constructor(symbol) {
        super()
        this.symbol = symbol
    }

    /**
     * Returns true if this token can be started with the given character
     * @param {string} character
     * @returns {boolean}
     */
    canStartWith(character) {
        return this.symbol.startsWith(character)
    }

    /**
     * Returns true if the token accepts this character being appended to the end.
     * @param {string} newCharacter
     * @param {string} context - Current value of the token
     * @returns {boolean}
     */
    accepts(newCharacter, context) {
        return this.symbol.startsWith(context + newCharacter)
    }

    /**
     * Returns true if the given value is valid to form this token.
     * @param {string} tokenValue
     * @returns {boolean}
     */
    isValidFor(tokenValue) {
        return this.symbol === tokenValue
    }
}

