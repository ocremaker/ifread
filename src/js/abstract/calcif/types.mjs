/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { containsFlag, flags } from "#utils"

@flags
export class Type {
    static accessor DECIMAL
    static accessor INTEGER
    static accessor BOOLEAN
}

Type.NUMBER = Type.INTEGER | Type.DECIMAL

const HUMAN_READABLE_TYPE = new Map([
    [Type.DECIMAL, "decimal number"],
    [Type.INTEGER, "integer"],
    [Type.BOOLEAN, "condition"],
    [Type.NUMBER, "number"],
])

/**
 * Generates a human readable string of the type.
 * @param {CalcIfValueType} type
 * @param {boolean} plural
 * @return {string}
 */
export function getHumanReadableOfType(type, plural = false) {
    const value = []
    if(HUMAN_READABLE_TYPE.has(type))
        value.push(HUMAN_READABLE_TYPE.get(type))
    else
        for(const [k,v] of HUMAN_READABLE_TYPE.entries())
            if(containsFlag(type, k))
                value.push(v + (plural ? "s" : ""))
    return value.join(" or ")
}

/**
 * @typedef {number} CalcIfValueType
 * @type {Type.DECIMAL|Type.INTEGER|Type.BOOLEAN|Type.NUMBER}
 */