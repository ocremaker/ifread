/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { BaseElement } from "../../element.mjs"
import { ICONS } from "./icons.mjs"
import { parseHTML } from "#utils"

import { html } from "lit"
import { customElement } from "lit/decorators.js"
import Quill from "quill"

import style from "#css/shadows/popup/richtexteditor.scss"
import { generatePlaceholder } from "./placeholders.mjs"


@customElement("rich-text-editor")
export class RichTextEditor extends BaseElement {
    static styles = [style]

    #contents = null

    initialize() {
        // Keep the quill elements out of shadow dom.
        this.editor = parseHTML(`<div class="rt-editor"></div>`)
        this.toolbar = parseHTML(this.toolbarHtml)
        // console.log("Toolbar", this.toolbar, this.editor)
        this.appendChild(this.toolbar)
        this.appendChild(this.editor)
        // Initialize keep.
        const options = {
            debug: 'warn',
            bounds: this,
            modules: {
                toolbar: this.toolbar,
                history: {
                    delay: 2000,
                    maxStack: 500,
                    userOnly: true
                },
            },
            placeholder: generatePlaceholder(),
            theme: 'snow'
        }
        this.quill = new Quill(this.editor, options)
        console.log("Loading", this.#contents)
        this.quill.setContents(this.#contents ?? [])
    }

    /**
     * Contents of the Quill editor.
     * @throws {Error} if called before the rich text editor is bound to a body.
     * @returns {Delta}
     */
    get contents() {
        return this.quill.getContents()
    }

    /**
     * Sets contents of the Quill editor.
     * @throws {Error} if called before the rich text editor is bound to a body.
     * @param {Delta} val
     */
    set contents(val) {
        return this.#contents = val
    }

    get toolbarHtml() {
        return `
            <div class="toolbar">
                <select class="ql-paragraph-format">
                    <option selected>Normal</option>
                    <option value="high-voice">High Voice</option>
                    <option value="verdict">Verdict</option>
                    <option value="vampire">Vampire</option>
                </select>
                <span class="ql-formats">
                    <button class="ql-bold"></button>
                    <button class="ql-italic"></button>
                    <button class="ql-underline"></button>
                    <button class="ql-strike"></button>
                    <button class="ql-script" value="sub"></button>
                    <button class="ql-script" value="super"></button>
                </span>
                <span class="ql-formats">
                    <button class="ql-blockquote"></button>
                    <button class="ql-music">${ICONS.music}</button>
                    <button class="ql-link"></button>
                    <button class="ql-tbo"></button>
                </span>
                <span class="ql-formats">
                    <button class="ql-button"></button>
                    <button class="ql-if"></button>
                </span>
                <span class="ql-formats">
                    <button class="ql-undo">${ICONS.undo}</button>
                    <button class="ql-redo">${ICONS.redo}</button>
                </span>
            </div>
        `
    }

    render() {
        return html`
            <div class="main-editor">
                <slot></slot>
            </div>
        `
    }
}