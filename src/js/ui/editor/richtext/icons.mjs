/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export const ICONS = {
    'music': `<svg viewBox="0 0 18 18">
                <path d="m12 12 2-1v-8l-8 3v8l-2 1" class="ql-stroke"/>
              </svg>`,
    // From https://github.com/gtgalone/react-quilljs/issues/35
    'undo': `<svg viewbox="0 0 18 18">
                <polygon class="ql-fill ql-stroke" points="6 10 4 12 2 10 6 10"></polygon>
                <path class="ql-stroke" d="M8.09,13.91A4.6,4.6,0,0,0,9,14,5,5,0,1,0,4,9"></path>
             </svg>`,
    'redo': `<svg viewbox="0 0 18 18">
                <polygon class="ql-fill ql-stroke" points="12 10 14 12 16 10 12 10"></polygon>
                <path class="ql-stroke" d="M9.91,13.91A4.6,4.6,0,0,1,9,14a5,5,0,1,1,5-5"></path>
             </svg>`
}