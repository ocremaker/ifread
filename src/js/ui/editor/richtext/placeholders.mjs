/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


const RANDOM_WORDS_PLACEHOLDER = "%RANDOM_WORD_PLACEHOLDER%"
let WORDS = []

function getRandomWords(n) {
    WORDS.sort(() => .5 - Math.random())
    return WORDS.slice(0, n)
}


async function fetchWords() {
    // Filtered from https://github.com/ScriptSmith/topwords/, words are under CC-BY-SA-4.0

    const req = await fetch("recommended_words.txt")
    if(req.ok) {
        WORDS = (await req.text()).split("\n")
    }
}

fetchWords().then(() => console.log("Recommended words fetched."))

const PLACEHOLDERS = [
    RANDOM_WORDS_PLACEHOLDER,
    RANDOM_WORDS_PLACEHOLDER,
    RANDOM_WORDS_PLACEHOLDER,
    RANDOM_WORDS_PLACEHOLDER,
    RANDOM_WORDS_PLACEHOLDER,
    RANDOM_WORDS_PLACEHOLDER,
    "Your epic composition starts here...",
    "Your epic composition starts here...",
    "Your epic composition starts here...",
    "Your new plot twist starts here...",
    "Your new plot twist starts here...",
    "Your new plot twist starts here...",
    "Your new adventure starts here...",
    "Your new adventure starts here...",
    "Your new adventure starts here...",
    "Your new adventure starts now...",
    "Your new adventure starts now...",
    "Your new adventure starts now...",
    "Your writing starts here...",
    "Your writing starts here...",
    "Your writing starts here...",
    "Brevity might be the soul of wit, but this empty box awaits your writing...",
    "Getting started is always the hardest part.\nIf you feel uninspired, don't be afraid of taking a break, or to try a different activity.",
    "Take a deep breath, close your eyes.\nCome on.\nReally.\nI swear...",
    "Stanley sits down at his computer, and begins writing the next section of his chapter...",
    "You chose to inspect the bottom of this empty box, only to find it empty... Fill the box? Y/N",
    `You find a business card at the bottom of this box:\n\n"STAN'S PREVIOUSLY OWNED VESSELS - "I won't shut up until I've made you a deal!" --Stan"\n\nYou wonder how it got here.`,
    "Within this box, the cat is neither dead, or alive. Why?\nBecause, at the moment, it doesn't exist! It's up to you to solve that before the universe collapses!",
    "Remember to stay hydrated!",
    "If you ever feel as though nobody will ever truly appreciate what you're writing here, remember that the Trails series localizes spent who knows how long making up a thousand of witty 'empty chest' quotes to the point where one thought to end it all.\n\nNow, they're the staple of that series. Nothing you do is ever useless!",
    "HIT DVORAK PLATE WITH YOUR HEAD TO MAKE A HOLE.",
    "If you're bored or uninspired enough to read those, maybe it's time to take a break.",
    "BOX ADVERTISEMENT SPACE FOR SALE.",
    "HELP ME! I'M BEING KEPT IN THE BASEMENT AND THEY'RE FORCING ME TO WRITE ALL THESE--",
    "This box sincerely apologizes for stealing all your work and inspiration.",
    "I feel as though I am repeating myself, but this box is empty. Please fill it? It's lonely.",
    "There is so much empty space here a cat might jump in and take it all for itself 🐈.",
    "Quest Start: Fill this empty text box with illuminating and enlightening content. Accept? [YES] [NO]",
    "*blush* Please stop staring while I'm all bare.",
    "Today is this box's birthday. Please write the name of the present you want to give it.",
    "There are so many incredible and interesting things I could be doing right now, but instead I'm spending it writing stupid placeholder messages.",
    "If you're reading this message, that means I finally managed to get an MVP for Ifread. The development started on April 9th, 2024-2025.\nIf I could do it, you can finish this work too! I believe in you!"
]

PLACEHOLDERS.push(`Fun fact: there are ${(new Set(PLACEHOLDERS)).size + 5} different quotes that can appear as placeholder. How many have you seen already?`)


function generateRandomWordsPlaceholder() {
    const WORD_PLACEHOLDERS = [
        `You want some inspiration? Here are four words on the house:\n\n${getRandomWords(4).join(", ")}.\n\nGood luck!`,
        `You want some inspiration? Fine. Here are four words for you:\n\n${getRandomWords(4).join(", ")}.\n\nUh, best of luck. You look like going to need it.`,
        `You want some inspiration? Okay. Here are four random words:\n\n${getRandomWords(4).join(", ")}.\n\nWhat? Not good enough? Fine then. Here are ten more:\n\n${getRandomWords(10).join(", ")}.`,
        `You want some inspiration? Okay, let's do this:\n\n${getRandomWords(4).join(", ")}-- Wait what do you mean you already had an idea and I ruined it?`,
    ]
    return WORD_PLACEHOLDERS[WORD_PLACEHOLDERS.length * Math.random() | 0]
}

export function generatePlaceholder() {
    let placeholder = PLACEHOLDERS[PLACEHOLDERS.length * Math.random() | 0]
    if(placeholder === RANDOM_WORDS_PLACEHOLDER)
        placeholder = generateRandomWordsPlaceholder()
    return placeholder
}