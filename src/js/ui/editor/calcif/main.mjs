/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { customElement, property } from "lit/decorators.js"
import { html } from "lit"
import { when } from "lit/directives/when.js"

import { BaseElement } from "../../element.mjs"
import { htmlEntities, parseHTML } from "#utils"
import { CalcIf, Variable } from "#abstract"

import style from "#css/shadows/popup/calcif.scss"


const FORBIDDEN_KEYS = [" ", "{", "}", "Enter", "ArrowUp", "ArrowDown"]
const IGNORE_KEYS = ["ArrowLeft", "ArrowRight", "Shift", "Tab"]


const GLOSSARY = [
    {
        name: "Numeric operators",
        lines: [
            {
                items: ["+", "-", "*", "/", "^"],
                description: "Common operators"
            },
            {
                items: ["%", "mod"],
                description: "Modulo (Remainder)"
            },
            {
                items: ["=", "!=", ">", "<", ">=", "<="],
                description: "Comparison operators"
            }
        ]
    },
    {
        name: "Condition (boolean) operators",
        lines: [
            {
                items: ["and", "&&", "or", "||"],
                description: "Common operators"
            },
            {
                items: ["=", "==", "!="],
                description: "Comparison operators"
            },
            {
                items: ["nor", "nand", "xor", "nor"],
                description: "Additional operators"
            }
        ]
    },
    {
        name: "Checking conditions (if-else)",
        lines: [
            {
                items: ["<condition> ? <value if true> : <value if false>"],
                description: "First syntax"
            },
            {
                items: ["<value if true> if <condition> else <value if false>"],
                description: "Second syntax"
            }
        ]
    },
    {
        name: "Functions",
        lines: [
            {
                items: ["round", "floor", "ceil"],
                description: "Rounding functions"
            },
            {
                items: ["!", "not"],
                description: "Boolean functions"
            },
            {
                items: ["sin", "cos", "tan"],
                description: "Trigonometric functions"
            }
        ]
    },
    {
        name: "Other",
        lines: [
            {
                items: ["true", "false", "pi", "e"],
                description:
                    "Constants"
            }
        ]
    }
]

@customElement("calc-if-editor")
export class CalcIfEditor extends BaseElement {
    static styles = [style]

    /** @type {CalcIf.Token[]} */
    #tokens = null

    /** @type {number} */
    #errorTimeoutId = null

    /** @type {CalcIf.TokenType[]} */
    #tokenTypes = CalcIf.DEFAULT_TOKEN_TYPES

    /** @type {CalcIfValueType} */
    #expectedOutputType = CalcIf.Type.NUMBER | CalcIf.Type.BOOLEAN

    /** @type {{startAt: number, endAt: number}} */
    #lastSelection = { startAt: 0, endAt: 0 }

    /** @type {VariableDefinition[]} */
    @property({ attribute: false })
    accessor variables = []

    /** @type {CalcIf.TokensError[]} */
    @property({ attribute: false })
    accessor errors = []

    /**
     * Returns the tokens of the edited expression.
     * @returns {CalcIf.Token[]}
     */
    get tokens() {
        return this.#tokens
    }

    /**
     * Sets tokens of the edited expression.
     * @param {CalcIf.Token[]} val
     */
    set tokens(val) {
        return this.#tokens = val
    }

    /**
     * Calc If value type that should be returned by the expression.
     * @returns {CalcIfValueType}
     */
    get expectedOutputType() {
        return this.#expectedOutputType
    }

    /**
     * Sets the Calc If value type that should be returned by the expression.
     * @param {CalcIfValueType} val
     */
    set expectedOutputType(val) {
        return this.#expectedOutputType = val
    }

    initialize() {
        // Keep the editor elements out of shadow dom.
        this.editor = parseHTML(`<div class="ci-editor" contenteditable="true" placeholder="Enter your expression here..."></div>`)
        this.appendChild(this.editor)
        this.#refreshTokensInHTML()
        this.editor.addEventListener("keydown", (e) => this.#handleInput(e))
        this.#findErrors()
    }

    /**
     * Adds the 'data-error-id' attribute to blocks in the editor matching the index of errors.
     */
    #applyErrorsToEditor() {
        const offset = this.editor.children[0]?.classList.contains("cie-skip") ? 1 : 0
        for(let i = 0; i < this.editor.children.length; i++) {
            const tokenBlock = this.editor.children[i + offset]
            if(tokenBlock?.hasAttribute("data-error-id"))
                tokenBlock.removeAttribute("data-error-id")
            for(let j = 0; j < this.errors.length; j++)
                if(this.errors[j].overTokens.includes(i)) {
                    tokenBlock.setAttribute("data-error-id", j % 5)
                    break
                }
        }
    }

    /**
     * Checks for errors in tokens and sets them to the 'errors' list.
     */
    #findErrors() {
        this.errors = CalcIf.identifyErrors({
            tokens: this.#tokens,
            tokenTypes: this.#tokenTypes,
            expectedType: this.#expectedOutputType,
            objectValues: Variable.convertDefinitionsToObjectValues(this.variables)
        })
        this.#applyErrorsToEditor()
    }

    /**
     * Returns the calc if expression as plain text.
     * @return {string}
     */
    #queryContentAsText() {
        return this.editor.textContent.replace(/[ \n\s]/g, "")
    }

    /**
     * Returns the 'startAt' and 'endAt' text index positions which are selected in the editor.
     * @param {Selection} selection
     * @return {{endAt: number, startAt: number}}
     */
    #getAbsoluteSelectionRangeInEditor(selection) {
        let startAt = 0
        let endAt = 0
        if(selection.rangeCount > 0) {
            const range = selection.getRangeAt(0)
            let start = range.startContainer.parentElement
            let end = range.endContainer.parentElement
            let parent = range.commonAncestorContainer
            if(parent instanceof Text)
                parent = parent.parentElement
            if(parent.closest(".ci-editor") === this.editor) {
                // Currently focused on editor.
                if(start === this && this.editor.children.length > 0) {
                    // Just deleted an element, start and end offsets match index of child.
                    start = this.editor.children[range.startOffset - 1] ?? this.editor
                    end = this.editor.children[range.endOffset - 1] ?? this.editor
                    startAt = start.textContent.length
                    endAt = end.textContent.length
                } else {
                    // Start and end are blocks.
                    startAt = range.startOffset
                    endAt = range.endOffset
                }
                while(start.previousElementSibling !== null) {
                    start = start.previousElementSibling
                    if(!start.classList.contains("cie-skip"))
                        startAt += start.textContent.trim().length
                }
                while(end.previousElementSibling !== null) {
                    end = end.previousElementSibling
                    if(!end.classList.contains("cie-skip"))
                        endAt += end.textContent.trim().length
                }
            }
        }
        return { startAt, endAt }
    }

    /**
     * Reselects the saved absolute selection range from the editor.
     * @param {Selection} selection
     */
    #reapplyPreviousAbsoluteSelectionRange(selection) {
        const { startAt, endAt } = this.#lastSelection
        const targetRange = document.createRange()
        let previousLength = 0
        for(const child of this.editor.children) {
            if(child.classList.contains("cie-skip")) // Internal dom fix.
                continue
            const newLength = previousLength + child.textContent.trim().length
            if(child.hasAttribute("contenteditable")) {
                // Uneditable block
                const nextTextNode = child.nextElementSibling?.childNodes[0] ?? this.editor
                const idx = 0
                if(newLength >= startAt && startAt > previousLength) {
                    targetRange.setStart(nextTextNode, idx)
                }
                if(newLength >= endAt && endAt > previousLength) {
                    targetRange.setEnd(nextTextNode, idx)
                }
            } else {
                if(newLength >= startAt && startAt > previousLength) {
                    targetRange.setStart(child.childNodes[0], startAt - previousLength)
                }
                if(newLength >= endAt && endAt > previousLength) {
                    targetRange.setEnd(child.childNodes[0], endAt - previousLength)
                }
            }
            previousLength = newLength
        }
        const firstChild = this.editor.children[0]
        const lastChild = this.editor.children[this.editor.children.length - 1]
        if(targetRange.startContainer === document) {
            // No start has been applied
            if(startAt === 0)
                targetRange.setStart(firstChild?.childNodes[0] ?? firstChild ?? this.editor, 0)
            else
                targetRange.setStart(lastChild?.childNodes[0] ?? lastChild ?? this.editor, lastChild?.textContent.length ?? 0)
        }
        if(targetRange.endContainer === document) {
            // No start has been applied
            if(endAt === 0)
                targetRange.setEnd(firstChild?.childNodes[0] ?? firstChild ?? this.editor, 0)
            else
                targetRange.setEnd(lastChild?.childNodes[0] ?? lastChild ?? this.editor, lastChild?.textContent.length ?? 0)
        }
        selection.removeAllRanges()
        selection.addRange(targetRange)
        return targetRange
    }

    /**
     * Replaces current selection by given plain text.
     * @param {string} text
     */
    #replaceSelectionBy(text) {
        const { startAt, endAt } = this.#lastSelection
        let contents = this.#queryContentAsText()
        contents = contents.substring(0, startAt) + text + contents.substring(endAt, contents.length)
        this.#lastSelection = {
            startAt: startAt + text.length,
            endAt: endAt + text.length
        }
        this.#tokens = CalcIf.parse(contents, this.#tokenTypes)
        this.#refreshTokensInHTML()
        this.#findErrors()
    }

    /**
     * Creates an HTML token block from a token.
     * @param {CalcIf.Token} token
     * @return {HTMLElement}
     */
    #createBlock(token) {
        let block
        if(token.tokenType instanceof CalcIf.ObjectTypeTokenType) {
            const internalValue = CalcIf.ObjectTypeTokenType.getInternalValue(token.value)
            block = parseHTML(`<span class="cie-token cie-object cie-${token.type}" contenteditable="false">
                    <span class="cie-object-internal">{${htmlEntities(token.tokenType.tokenName)}:${htmlEntities(internalValue)}}</span>
                </span>`)
            block.setAttribute("data-value", token.value)
            if(token.type.startsWith("variable-")) {
                // Set variable data
                const variable = this.variables.find(v => v.uuid === internalValue)
                block.classList.add("has-icon", token.type)
                block.setAttribute("data-name", variable?.name ?? "<undefined>")
                block.setAttribute("data-defined", variable !== undefined)
            }
        } else {
            block = parseHTML(`<span class="cie-${token.type} cie-token">${htmlEntities(token.value)}</span>`)
            block.setAttribute("data-value", token.value)
        }
        return block
    }

    /**
     * Parses tokens into the editor, and moves the selection objects accordingly.
     */
    #refreshTokensInHTML() {
        const selection = window.getSelection()
        this.#lastSelection = this.#getAbsoluteSelectionRangeInEditor(selection)
        this.editor.innerHTML = null
        for(const token of this.#tokens) {
            const block = this.#createBlock(token)
            this.editor.appendChild(block)
        }
        if(this.editor.children.length > 0) {
            // Just in case last element is not selectable. Will be removed at parsing.
            if(this.editor.firstElementChild.hasAttribute("contenteditable"))
                this.editor.insertBefore(parseHTML("<span class=\"cie-skip\">&nbsp;</span>"), this.editor.firstChild)
            if(this.editor.lastElementChild.hasAttribute("contenteditable"))
                this.editor.appendChild(parseHTML("<span class=\"cie-skip\">&nbsp;</span>"))
        }
        this.#reapplyPreviousAbsoluteSelectionRange(selection)
    }

    /**
     *
     * @param {KeyboardEvent} e
     */
    #handleInput(e) {
        clearTimeout(this.#errorTimeoutId)
        this.errors = []
        if(FORBIDDEN_KEYS.includes(e.key))
            e.preventDefault()
        if(!IGNORE_KEYS.includes(e.key) && (!e.ctrlKey || e.key === "v")) {
            setTimeout(() => this.parseTokens(), 1)
            this.#errorTimeoutId = setTimeout(() => this.#findErrors(), 500)
        } else if(e.key !== "Tab") {
            // Save selection anyway.
            setTimeout(() => {
                this.#lastSelection = this.#getAbsoluteSelectionRangeInEditor(window.getSelection())
            }, 1)
        }
    }

    /**
     * Reparses the tokens and outputs the result to the HTML.
     */
    parseTokens() {
        const contents = this.#queryContentAsText()
        console.log("Contents:", JSON.stringify(contents))
        this.#tokens = CalcIf.parse(contents, this.#tokenTypes)
        this.#refreshTokensInHTML()
    }

    /**
     * Adds the clicked variable to the editor.
     * @param {UIEvent} e
     */
    addVariable(e) {
        const varUUID = e.target.getAttribute("data-uuid")
        const variable = this.variables.find(v => v.uuid === varUUID)
        if(variable) {
            const varType = variable.varType === Variable.Type.INT ? "variable-int" : "variable-bool"
            this.#replaceSelectionBy(`{${varType}:${varUUID}}`)
        }
    }

    render() {
        return html`
            <div class="main-editor">
                <slot></slot>
            </div>
            <details class="errors" data-count="${this.errors.length}" open>
                <summary>Errors</summary>
                <ul>
                    ${this.errors.map(e => {
                        const [preUnderline, pastStart] = e.message.split("<b>")
                        const [underline, pastUnderline] = pastStart.split("</b>")
                        return html`
                            <li>${preUnderline}<b>${underline}</b>${pastUnderline}</li>`
                    })}
                </ul>
            </details>
            <details class="variables" data-count="${this.variables.length}" open>
                <summary>Variables</summary>
                <ul>
                    ${this.variables.map(v => {
                        return html`
                            <li>
                                <button class="variable-name has-icon variable-${v.varType}" data-uuid="${v.uuid}"
                                        @click="${this.addVariable}">
                                    ${v.name}
                                </button>
                                <span class="details">
                                    ${when(v.description !== "", () =>
                                            html`<span class="description">${v.description}<br>`
                                    )}
                                    ${when(v.varType === Variable.Type.INT, () =>
                                            html`<span class="maximum">Max: ${v.maximum}<br>`
                                    )}
                                </span>
                            </li>`
                    })}
                </ul>
                <p>Hint: You can click a variable to insert it into the editor.</p>
            </details>
            <details class="glossary" open>
                <summary>Glossary</summary>
                ${GLOSSARY.map(list =>
                        html`
                            <details>
                                <summary>${list.name}</summary>
                                <ul>
                                    ${list.lines.map(entry => {
                                        return html`
                                            <li>
                                                <span class="list">
                                                    ${entry.items.map(x => html`<span class="character">${x}</span>`)}
                                                </span>: ${entry.description}
                                            </li>`
                                    })}
                                </ul>
                            </details>`
                )}
            </details>
        `
    }
}