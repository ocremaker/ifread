/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * Opens a popup with an inner diagram editor.
 * @param {string} innerGraphTitle
 * @param {string} id - ID of the diagram to open.
 * @param {string} nodeTypesListTitle - List of nodes which can be created within the graph.
 */
export function openInnerDiagram(innerGraphTitle, id, nodeTypesListTitle) {
    /** @type {BasePopup} */
    const popup = document.createElement("base-popup")
    popup.popupTitle = innerGraphTitle
    popup.hasCloseButton = true
    popup.hasDrawer = true
    popup.classList.add("popup-diagram")
    const builder = document.createElement("logic-builder-canvas")
    builder.builderId = id
    builder.nodeTypesList = nodeTypesListTitle
    popup.appendChild(builder)
    popup.buttons = []
    popup.addEventListener("close", () => {
        builder.stage.container(document.createElement("div"))
    })
    popup.addEventListener("toggleDrawer", () => {
        builder.drawerOpen = !builder.drawerOpen
    })
    document.body.appendChild(popup)
    setTimeout(() => popup.focus(), 100)
}