/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseElement } from "../element"
import { ButtonDeclarator } from "#utils"
import { html } from "lit"
import { customElement, property } from "lit/decorators.js"

import style from "#css/shadows/popup/popup.scss"
import { when } from "lit/directives/when.js"

@customElement("base-popup")
export class BasePopup extends BaseElement {
    static styles = [style]

    @property({ attribute: "popup-title" })
    accessor popupTitle

    @property({ attribute: "has-close-button", type: Boolean })
    accessor hasCloseButton = false

    @property({ attribute: "has-drawer", type: Boolean })
    accessor hasDrawer = false

    @property()
    accessor buttons = [
        new ButtonDeclarator({
            text: "Accept",
            callback: () => {}
        })
    ]

    clickedButton(e) {
        let btnId = parseInt(e.target.getAttribute("data-id"))
        this.buttons[btnId].callback()
        this.remove()
    }

    initialize() {
        this.tabIndex = 1
        if(this.hasCloseButton)
            this.addEventListener("keydown", (e) => {
                if(e.key === "Escape")
                    this.close()
            })
    }

    close() {
        this.dispatchEvent(new CustomEvent("close"))
        this.remove()
    }

    toggleDrawer(e) {
        this.dispatchEvent(new CustomEvent("toggleDrawer"))
        e.target.classList.toggle("menu")
        e.target.classList.toggle("menu-unfold")
    }

    render() {
        return html`
            <div class="popup-container">
                <h2 class="popup-title">
                    ${when(this.hasDrawer, () => {
                        return html`
                            <button class="has-icon menu" title="Show/hide drawer" @click="${this.toggleDrawer}"></button>
                        `
                    })}
                    <span class="popup-title-text">${this.popupTitle}</span>
                    ${when(this.hasCloseButton, () => {
                        return html`
                            <button class="has-icon close" title="Close" @click="${this.close}"></button>
                        `
                    })}
                </h2>
                <div class="popup-body">
                    <slot>[EMPTY BODY]</slot>
                </div>
                <div class="popup-buttons">
                    ${this.buttons.map((btn, i) => {
                        return html`
                            <button data-id="${i}" class="${btn.classMap}" tabindex="0" @click=${this.clickedButton}>${btn.text}</button>`
                    })}
                </div>
            </div>`
    }
}