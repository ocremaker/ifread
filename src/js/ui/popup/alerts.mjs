/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ButtonDeclarator, parseHTML, parseSimpleMarkdown } from "#utils"

let autoConfirm = []

/**
 * Opens an asynchronous confirm popup.
 * @param {string} id
 * @param {string} title
 * @param {string} contents
 * @param {boolean} allowAlwaysConfirm
 * @returns {Promise<>}
 */
export function alert(id, title, contents, allowAlwaysConfirm = false) {
    return new Promise((resolve, reject) => {
        if(autoConfirm.includes(id))
            resolve()
        else {
            /** @type {BasePopup} */
            let popup = parseHTML(`
            <base-popup popup-title="${title}" allows-escape="true" class="popup-alert">
                <div>
                    ${parseSimpleMarkdown(contents)}
                    ${allowAlwaysConfirm ? `<br><br><input type="checkbox" id="always-confirm" /><label for="always-confirm">Do not show this popup again</label>` : ""}
                </div>
            </base-popup>
            `)
            popup.buttons = [
                new ButtonDeclarator({
                    text: "Ok",
                    callback: () => {
                        if(allowAlwaysConfirm && popup.querySelector("#always-confirm").checked)
                            autoConfirm.push(id)
                        resolve()
                    }
                })
            ]
            document.body.appendChild(popup)
            setTimeout(() => popup.focus(), 100)
        }
    })
}

/**
 * Opens an asynchronous confirm popup.
 * @param {string} id
 * @param {string} title
 * @param {string} contents
 * @returns {Promise<>}
 */
export function confirm(id, title, contents) {
    return new Promise((resolve, reject) => {
        if(autoConfirm.includes(id))
            resolve()
        else {
            /** @type {BasePopup} */
            let popup = parseHTML(`
            <base-popup popup-title="${title}" allows-escape="true" class="popup-alert">
                <div>
                    ${parseSimpleMarkdown(contents)}<br><br>
                    <input type="checkbox" id="always-confirm" />
                    <label for="always-confirm">Do not show this popup again</label>
                </div>
            </base-popup>
            `)
            popup.buttons = [
                new ButtonDeclarator({
                    text: "Cancel",
                    callback: () => {
                    }
                }),
                new ButtonDeclarator({
                    text: "Confirm",
                    callback: () => {
                        if(popup.querySelector("#always-confirm").checked)
                            autoConfirm.push(id)
                        resolve()
                    }
                })
            ]
            document.body.appendChild(popup)
            setTimeout(() => popup.focus(), 100)
        }
    })
}
