/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { ButtonDeclarator } from "#utils"
import Delta from 'quill-delta'
import {CalcIf, Variable} from "#abstract"

/**
 * Opens a popup with an rich text editor.
 * @param {string} title
 * @param {string} textUUID - UUID pointing to the Quill Delta data to open in the editor.
 * @returns {Promise}
 */
export function openTextEditor(title, textUUID) {
    return new Promise((resolve, reject) => {
        let popup = document.createElement("base-popup")
        popup.popupTitle = title
        popup.hasCloseButton = true
        popup.classList.add("popup-editor")
        /** @type {RichTextEditor} */
        let rte = document.createElement("rich-text-editor")
        rte.contents = new Delta(window.globals.texts[textUUID])
        popup.appendChild(rte)
        popup.buttons = [
            new ButtonDeclarator({
                text: "Cancel",
                callback: () => {}
            }),
            new ButtonDeclarator({
                text: "Save",
                callback: () => {
                    window.globals.texts[textUUID] = rte.contents.ops
                    resolve()
                }
            })
        ]
        popup.addEventListener("close", () => resolve())
        document.body.appendChild(popup)
        setTimeout(() => popup.focus(), 100)
    })
}


/**
 * Opens a popup with an calc/if expression editor.
 * @param {string} title
 * @param {CalcIf.Token[]} data - Calc If data to open in the editor.
 * @param {VariableDefinition[]} variables - List of variable definitions usable in this calc if expression.
 * @param {CalcIfValueType} expectedOutputType
 */
export function openCalcIfEditor(title, data, variables, expectedOutputType) {
    return new Promise((resolve, reject) => {
        let popup = document.createElement("base-popup")
        popup.popupTitle = title
        popup.hasCloseButton = true
        popup.classList.add("popup-calcifeditor")
        /** @type {CalcIfEditor} */
        let cie = document.createElement("calc-if-editor")
        cie.tokens = data
        cie.variables = variables
        cie.expectedOutputType = expectedOutputType
        popup.appendChild(cie)
        popup.buttons = [
            new ButtonDeclarator({
                text: "Cancel",
                callback: () => {}
            }),
            new ButtonDeclarator({
                text: "Save",
                callback: () => {
                    resolve(cie.tokens)
                }
            })
        ]
        popup.addEventListener("close", () => resolve(cie.tokens))
        document.body.appendChild(popup)
        setTimeout(() => popup.focus(), 100)
    })
}