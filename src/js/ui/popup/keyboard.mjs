/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { htmlEntities, parseHTML } from "#utils"
import { ITEMS } from "../menu/menus"
import { ButtonDeclarator } from "#utils"

const medKeys = ["tab", "enter", "backspace"]
const lgKeys = ["space"]
const replacements = {
    "arrowdown": "↓",
    "arrowup": "↑",
    "arrowleft": "←",
    "arrowright": "→",
    "tab": "↹ Tab",
    "enter": "↲ Enter",
    "delete": "Del."
}

export function keyboardShortcuts() {
    /** @type {BasePopup} */
    let popup = parseHTML(`
    <base-popup popup-title="Keyboard shortcuts" has-close-button="true" class="popup-shortcuts">
        ${ITEMS.filter(mi => mi.shortcut !== null).map(mi => {
        let s = mi.shortcut
        let k = mi.shortcut.key
        return `
            <div class="shortcut-row">
                <p class="description">
                    <b class="title">${mi.text.replace('&', '')}</b>
                    <span class="text">${htmlEntities(mi.description)}</span>
                </p>
                <div class="shortcut">
                    ${s.ctrl ? `<div class="key md">Ctrl</div>` : ""}
                    ${s.shift ? `<div class="key lg">⇑ Shift</div>` : ""}
                    ${s.alt ? `<div class="key sm">Alt</div>` : ""}
                    <div class="key ${lgKeys.includes(k) ? `lg` : (medKeys.includes(k) ? `md` : `sm`)}">
                        ${mi.shortcut.key in replacements ?
                            replacements[mi.shortcut.key] :
                            (mi.shortcut.key[0].toUpperCase() + mi.shortcut.key.substring(1))}
                    </div>
                </div>
            </div>`.trim()
    }).join("")}
    </base-popup>
    `)
    popup.buttons = [
        new ButtonDeclarator({
            text: "Close",
            callback: () => {}
        })
    ]
    document.body.appendChild(popup)
    setTimeout(() => popup.focus(), 100)

}