/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {LitElement} from 'lit';


export class BaseElement extends LitElement {
    static properties = {}

    /**
     * Forwards a custom event
     * @param {CustomEvent} e
     */
    forwardCustomEvent(e) {
        this.dispatchEvent(new CustomEvent(e.type, {
            detail: e.detail,
        }))
        this.requestUpdate()
    }

    /**
     * Called when the shadow dom has been initialized.
     */
    initialize() {
        throw new Error("Not implemented.");
    }

    firstUpdated(args) {
        super.firstUpdated(args)
        this.initialize()
    }
}