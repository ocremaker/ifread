/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseElement } from "../element.mjs"
import { BLOCK_CATEGORIES, BaseBlock } from "#logic-builder/blocks"
import { ZonePerBuilderDelegate } from "#logic-builder/zones"

import { html } from "lit"
import { query, customElement, property } from "lit/decorators.js"
import { classMap } from "lit/directives/class-map.js"
import { styleMap } from "lit/directives/style-map.js"
import { when } from "lit/directives/when.js"

import style from "#css/shadows/canvas.scss"

@customElement("logic-builder-canvas")
export class GraphCanvas extends BaseElement {
    static styles = [style]

    @property({ attribute: "node-types-list" })
    accessor nodeTypesList

    @property({ attribute: "builder-id" })
    accessor builderId

    @property({ attribute: "drawer-open", type: Boolean })
    accessor drawerOpen = true

    @property({ attribute: false })
    accessor currentlyChangingRatio = false

    @property({ attribute: false })
    accessor topDrawerCoverRatio = .5

    @property({ attribute: false })
    accessor stage = null

    @property({ attribute: false })
    accessor selectedNode = null

    @property({ attribute: false })
    accessor selectedZone = null

    @property({ attribute: false })
    accessor selectedDrawerTab = "blocks"

    @query(".canvas-container")
    accessor canvasContainer

    /**
     * @type {DrawerPanelCreate}
     */
    @query("drawer-panel-create")
    accessor drawerPanelCreate

    /**
     * @type {DrawerPanelZones}
     */
    @query("drawer-panel-zones")
    accessor drawerPanelZones

    constructor() {
        super()
    }

    initialize() {
        let container = this.canvasContainer
        this.focusClickTimeout = -1
        this.stage = window.globals.builders[this.builderId]
        this.stage.container(container)
        this.stage.width(container.offsetWidth)
        this.stage.height(container.offsetHeight)
        this.recenterStage()
        // Adding events for selection
        container.addEventListener("selected", (e) => {
            if(e.detail instanceof ZonePerBuilderDelegate) {
                this.selectedNode = null
                this.selectedZone = e.detail.zone
                this.selectedDrawerTab = "zones"
            } else {
                this.selectedNode = e.detail
                this.selectedZone = null
                this.selectedDrawerTab = "blocks"
            }
        })
        // Tab events
        container.addEventListener("focus", (e) => {
            this.focusClickTimeout = setTimeout(() => {
                if(this.selectedNode === null && this.selectedZone === null) {
                    let blocks = this.stage.listFocusCaptors()
                    if(blocks.length > 0)
                        this.stage.focus(blocks[0])
                }
            }, 200)
        })
        container.addEventListener("click", (e) => {
            clearTimeout(this.focusClickTimeout)
        })
        container.addEventListener("keydown", (e) => {
            // e.stopPropagation()
            let eProps = { key: e.key, shift: e.shiftKey, alt: e.altKey, ctrl: e.ctrlKey, evt: e }
            if(e.key === "Tab") {
                // Focus next element
                this.handleTab(e.shiftKey, e)
            } else if(this.selectedNode !== null)
                this.selectedNode.fire("keydown", eProps)
            else
                this.stage.fire("keydown", eProps)
        })
        // Observe resizing of the canvas
        this.canvasResizer = new ResizeObserver((info) => {
            if(info.length > 0) {
                let { width, height } = info[0].contentRect
                if(width !== this.stage.width())
                    this.stage.width(width)
                if(height !== this.stage.height())
                    this.stage.height(height)
                this.stage.grid.redrawGrid()
            }
        })
        this.canvasResizer.observe(container)
    }

    /**
     * Handling of drag and drop of objects.
     */

    canvasDragOver(e) {
        e.preventDefault()
    }

    canvasDrop(e) {
        e.preventDefault()
        let data = e.dataTransfer.getData("x-ifread/node")
        let nodeType = BLOCK_CATEGORIES[this.nodeTypesList].find(n => n.name === data)
        if(nodeType !== undefined) {
            this.stage.setPointersPositions(e)
            this.stage.addNewBlock(nodeType, this.stage.getOffsetPointerPosition())
        }
    }

    recenterStage() {
        this.stage.x(400)
        this.stage.y(this.canvasContainer.offsetHeight / 2)
        this.stage.scale({ x: 1, y: 1 })
        this.stage.grid.redrawGrid()
    }

    /**
     * @param {boolean} shift
     * @param {Event} e
     */
    handleTab(shift, e) {
        let focusable = this.stage.listFocusCaptors()
        if(focusable.length > 0) {
            let currentSelectedId = focusable.indexOf(this.selectedNode)
            if(currentSelectedId === -1) currentSelectedId = 0
            if(shift) {
                // Focus previous element.
                if(currentSelectedId > 0) {
                    e.preventDefault()
                    this.stage.focus(focusable[currentSelectedId - 1])
                } else {
                    this.stage.focus(null)
                    this.stage.currentFocusedElement = null
                }
            } else {
                // Focus next element
                if(currentSelectedId < focusable.length - 1) {
                    e.preventDefault()
                    this.stage.focus(focusable[currentSelectedId + 1])
                } else {
                    this.stage.focus(null)
                    this.stage.currentFocusedElement = null
                }
            }

        }
    }

    /**
     * Called when one of the tab button has been clicked.
     * @param {UIEvent} e
     */
    changeTab(e) {
        let tab = e.target.getAttribute("tab-id") ?? "blocks"
        if(tab !== this.selectedDrawerTab) {
            this.drawerPanelZones.selectTool("move")
            this.selectedDrawerTab = tab
        }
    }

    changeTool(e) {
        this.stage.fire("toolChanged", { tool: e.detail.newTool })
    }

    selectZone(e) {
        const delegate = e.detail.zone.delegates.get(this.builderId)
        this.stage.focus(delegate)
    }

    propertyChanged() {
        this.drawerPanelZones.requestUpdate()
    }

    startChangingPanelRatio() {
        this.currentlyChangingRatio = true
    }

    stopChangingPanelRatio() {
        this.currentlyChangingRatio = false
    }

    /**
     * Called when the panel separator is moved.
     * @param {PointerEvent} e
     */
    changePanelRatio(e) {
        if(this.currentlyChangingRatio && e.buttons === 1) {
            const thisRect = this.getBoundingClientRect()
            const y = e.clientY - thisRect.top
            this.topDrawerCoverRatio = y / thisRect.height
        }
    }

    render() {
        const topHeight = this.topDrawerCoverRatio*this.clientHeight - 10
        const bottomHeight = (1 - this.topDrawerCoverRatio)*this.clientHeight - 10
        return html`
            <div id="drawer" class="${classMap({ closed: !this.drawerOpen })}"
                 @pointermove="${this.changePanelRatio}" @pointerup="${this.stopChangingPanelRatio}">
                <div class="panels-list" style="${styleMap({ minHeight: topHeight + "px", maxHeight: topHeight + "px" })}">
                    <div role="tablist">
                        <button id="tab-button-blocks" role="tab"
                                tabindex="${this.selectedDrawerTab === "zones" ? 0 : -1}"
                                tab-id="blocks" @click=${this.changeTab}
                                aria-controls="blocks-panel" aria-selected="${this.selectedDrawerTab === "blocks"}">
                            Blocks
                        </button>
                        <button id="tab-button-zones" role="tab"
                                tabindex="${this.selectedDrawerTab === "blocks" ? 0 : -1}"
                                tab-id="zones" @click=${this.changeTab}
                                aria-controls="zones-panel" aria-selected="${this.selectedDrawerTab === "zones"}">
                            Zones
                        </button>
                    </div>
                    <drawer-panel-create role="tabpanel" tabindex="0" aria-labelledby="tab-button-blocks"
                                         class="${classMap({
                                             "drawer-panel": true,
                                             "hidden": this.selectedDrawerTab !== "blocks"
                                         })}"
                                         .nodeTypesList=${this.nodeTypesList}></drawer-panel-create>
                    <drawer-panel-zones role="tabpanel" tabindex="0" aria-labelledby="tab-button-zones"
                                        class="${classMap({
                                            "drawer-panel": true,
                                            "hidden": this.selectedDrawerTab !== "zones"
                                        })}"
                                        .stage=${this.stage} .selectedZone=${this.selectedZone}
                                        @toolChanged=${this.changeTool}
                                        @zoneSelected=${this.selectZone}></drawer-panel-zones>
                </div>
                ${when(this.selectedNode ?? this.selectedZone !== null, () => {
                    return html`
                        <div class="panel-separator" @pointerdown="${this.startChangingPanelRatio}"></div>
                        <drawer-panel-properties id="zone-properties"
                                                 style="${styleMap({ minHeight: bottomHeight + "px", maxHeight: bottomHeight + "px" })}"
                                                 .associatedObject=${this.selectedNode ?? this.selectedZone}
                                                 @propertyChanged=${this.propertyChanged}></drawer-panel-properties>`
                })}

            </div>
            <div class="canvas-container cursor-grab" tabindex="0" role="application"
                 aria-label="Interactive Logic Builder"
                 aria-roledescription="Use the Tab key to navigate through blocks, Space or Enter to open either the inner diagram or text prompt,
                 Ctrl+P to edit the block's properties, the Arrow Keys to move them, Escape to unfocus it, and Delete to delete them."
                 style="${styleMap({ backgroundColor: this.stage?.defaultZone?.blockData.color })}"
                 @dragover=${this.canvasDragOver} @drop=${this.canvasDrop}></div>
            <button class="reset-position" @click=${this.recenterStage}>Reset position</button>`
    }
}