/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { html } from "lit"
import { customElement, property, query } from "lit/decorators.js"
import { repeat } from "lit/directives/repeat.js"
import { when } from "lit/directives/when.js"
import { classMap } from "lit/directives/class-map.js"
import { BaseElement, alert } from "#ui"
import { SelectOption } from "#utils"
import { Variable } from "#abstract"

import style from "#css/shadows/variable/definitions.scss"

export const descriptions = {
    [Variable.Type.BOOL]: "Boolean",
    [Variable.Type.INT]: "Integer",
}

const TYPE_SELECT = Variable.Type.map(
    varType => new SelectOption({
        value: varType,
        text: descriptions[varType],
        icon: `variable-${varType}`
    })
)

@customElement("variable-definitions")
export class DefineVariablesProperty extends BaseElement {
    static styles = [style]

    /** @type {Set<VariableDefinition>} */
    @property()
    accessor definitions

    /** @type {Zone} */
    @property()
    accessor zone

    @property({ type: String })
    accessor showingTypePicker

    @query(".row.question")
    accessor complexityBtn


    initialize() {

    }

    /**
     * Returns the definition by which the given event target is associated with.
     * @param {Event} e
     * @return {VariableDefinition}
     */
    findTargetDefinition(e) {
        const uuid = e.target.closest("[data-uuid]").getAttribute("data-uuid")
        return this.definitions.find(d => d.uuid === uuid)
    }

    /**
     * Shows the popup with information about complexity.
     */
    showComplexity() {
        alert("info-complexity", "About complexity", `Complexity represents the number of possible different states with the defined variables.

Having too high a complexity (~50) can cause performance issues on AO3.`).then(() => this.complexityBtn.focus())
    }

    /**
     * Shows the type picker for the associated definition.
     * @param e
     */
    showTypePicker(e) {
        const def = this.findTargetDefinition(e)
        this.showingTypePicker = def.uuid
    }

    /**
     * Changes the type of the definition.
     * @param {CustomEvent} e
     */
    changeTypeTo(e) {
        const def = this.findTargetDefinition(e)
        def.varType = e.detail
        this.showingTypePicker = null
        // this.requestUpdate()
        this.shadowRoot.querySelector(`[data-uuid="${def.uuid}"]`).focus()
    }

    /**
     * Updates the name of the variable.
     * @param {Event} e
     */
    updateName(e) {
        const def = this.findTargetDefinition(e)
        const name = e.target.value
        if(name.length > 0)
            def.name = name
        this.dispatchEvent(new CustomEvent("updated"))
    }

    /**
     * Deletes the associated definition from the list.
     * @param {Event} e
     */
    deleteDefinition(e) {
        const def = this.findTargetDefinition(e)
        this.definitions.delete(def)
        this.requestUpdate()
        this.dispatchEvent(new CustomEvent("updated"))
    }

    /**
     * Folds the description into one line.
     * @param {Event} e
     */
    foldDescription(e) {
        e.target.style.removeProperty("height")
    }

    /**
     * Unfolds the description to show everything.
     * @param {Event} e
     */
    unfoldDescription(e) {
        e.target.style.height = "1px"
        e.target.style.height = (e.target.scrollHeight) + "px"
    }

    /**
     * Updates the value of the description into the definition, and updates the textarea's height.
     * @param {Event} e
     */
    updateDescription(e) {
        const def = this.findTargetDefinition(e)
        def.description = e.target.value
        // Updating element's height.
        this.unfoldDescription(e)
        this.dispatchEvent(new CustomEvent("updated"))


    }

    /**
     * Updates the maximum of the integer variable.
     * @param {Event} e
     */
    updateMaximum(e) {
        const def = this.findTargetDefinition(e)
        const value = Math.abs(parseInt(e.target.value, 10)) || 1
        def.maximum = value
        e.target.value = value
        this.requestUpdate()
        this.dispatchEvent(new CustomEvent("updated"))
    }

    /**
     * Adds a new variable for the definitions list.
     */
    addDefinition() {
        this.definitions.add(new Variable.Definition({
            name: "Flag",
            varType: Variable.Type.BOOL
        }))
        this.requestUpdate()
        this.dispatchEvent(new CustomEvent("updated"))
    }

    render() {
        const previousDefinitions = this.zone.listAncestors().reverse().flatMap(z => Array.from(z.blockData.variables))
        const definitions = this.definitions
        const complexity = [...previousDefinitions, ...definitions].reduce((acc, v) => acc * v.complexity, 1)
        const complexityClass = {
            calm: complexity < 20,
            warn: complexity >= 20 && complexity < 50,
            panic: complexity >= 50
        }
        return html`
            <ol>
                <li>
                    <button class="row question" title="Click for more information."
                            @click="${this.showComplexity}">
                        <span>Complexity:</span>
                        <span class="complexity ${classMap(complexityClass)}">${complexity}</span>
                    </button>
                </li>
                ${repeat(previousDefinitions, d => d.uuid, d =>
                        html`
                            <li data-uuid="${d.uuid}" class="parent-definition"
                                title="This variable cannot be modified, as it has been defined in one of the parent zones.">
                                <div class="row">
                                    <button class="type has-icon variable-${d.varType}"
                                            title="${descriptions[d.varType]}"
                                            aria-label="${descriptions[d.varType]}"></button>
                                    <span class="title">${d.name}</span>
                                </div>
                                <div class="row">
                                    <span class="description">
                                        ${d.description}
                                    </span>
                                </div>
                                ${when(d.varType === Variable.Type.INT, () => {
                                    return html`
                                        <div class="row">
                                            <span>Maximum: ${d.maximum}</span>
                                        </div>`
                                })}
                            </li>
                        `
                )}
                ${repeat(definitions, d => d.uuid, d =>
                        html`
                            <li data-uuid="${d.uuid}">
                                ${when(this.showingTypePicker === d.uuid, () =>
                                        html`
                                            <select-menu id="choose-type" .options="${TYPE_SELECT}"
                                                         @selected="${this.changeTypeTo}"></select-menu>`
                                )}
                                <div class="row">
                                    <button class="type has-icon variable-${d.varType}"
                                            title="${descriptions[d.varType]}"
                                            aria-label="${descriptions[d.varType]}"
                                            @click="${this.showTypePicker}"></button>
                                    <input type="text" title="Name" aria-label="Name"
                                           class="title" value="${d.name}"
                                           pattern=".+" @change="${this.updateName}">
                                    <button class="has-icon trash" title="Delete" aria-label="Delete"
                                            @click="${this.deleteDefinition}"></button>
                                </div>
                                <div class="row">
                                <textarea title="Description" aria-label="Description" class="description"
                                          placeholder="Describe your variable here&hellip;"
                                          @focus="${this.unfoldDescription}" @blur="${this.foldDescription}"
                                          @keyup="${this.updateDescription}">${d.description}</textarea>
                                </div>
                                ${when(d.varType === Variable.Type.INT, () => {
                                    return html`
                                        <div class="row">
                                            <label for="maximum-${d.uuid}">Maximum: </label>
                                            <input type="number" title="Maximum of the number"
                                                   aria-label="Maximum of the number"
                                                   id="maximum-${d.uuid}" class="maximum"
                                                   min="1" step="1" value="${d.maximum}"
                                                   @change="${this.updateMaximum}">
                                        </div>`
                                })}
                            </li>`
                )}
                <li class="create">
                    <button class="has-icon add" @click="${this.addDefinition}">Add new variable</button>
                </li>
            </ol>
        `
    }
}