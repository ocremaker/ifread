/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { html } from "lit"
import { customElement, property, query } from "lit/decorators.js"
import { ifDefined } from "lit/directives/if-defined.js"
import { when } from "lit/directives/when.js"
import { classMap } from "lit/directives/class-map.js"
import { BaseElement } from "../../element.mjs"
import { CalcIf, Variable } from "#abstract"

import style from "#css/shadows/variable/condition.scss"
import { alert, openCalcIfEditor } from "../../popup/index.mjs"

@customElement("variable-condition")
export class TestVariablesProperty extends BaseElement {
    static styles = [style]

    /** @type {string} */
    @property()
    accessor localizedPropName

    /** @type {VariableCondition|null} */
    @property()
    accessor condition

    /** @type {Zone} */
    @property()
    accessor fromZone

    @query(".has-icon.errors")
    accessor errorsButton

    initialize() {

    }

    /**
     * Adds or remove the condition.
     */
    toggleDefined() {
        if(this.condition === null) {
            this.condition = new Variable.Condition({
                calcIfExpression: CalcIf.parse("true", CalcIf.DEFAULT_TOKEN_TYPES),
                expressionHasErrors: false
            })
        } else {
            this.condition = null
        }
        this.dispatchEvent(new CustomEvent("updated", { detail: this.condition }))
        this.requestUpdate()
    }


    /**
     * Returns a list of TokensError for the condition.
     * @returns {TokensError[]}
     */
    listVariableErrors() {
        const vars = this.fromZone?.listAllDefinedVariables() ?? []
        const objectValues = Variable.convertDefinitionsToObjectValues(vars)
        const errors = CalcIf.identifyErrors({
            tokens: this.condition.calcIfExpression,
            tokenTypes: CalcIf.DEFAULT_TOKEN_TYPES,
            expectedType: CalcIf.Type.BOOLEAN,
            objectValues
        })
        console.log("Errors:", errors)
        return errors
    }

    /**
     * Open an editor to edit the condition.
     */
    editCondition() {
        const usableDefs = this.fromZone?.listAllDefinedVariables() ?? []
        openCalcIfEditor(`Edit ${this.localizedPropName}:`, this.condition.calcIfExpression, usableDefs, CalcIf.Type.BOOLEAN).then((results) => {
            this.condition.calcIfExpression = results
            this.condition.expressionHasErrors = this.listVariableErrors().length !== 0
            this.dispatchEvent(new CustomEvent("updated", { detail: this.condition }))
            this.requestUpdate()
        })
    }

    /**
     * Shows the error message as an alert.
     */
    showErrorAlert() {
        const errors = this.listVariableErrors()
        const errorsMd = errors.map(e => e.message.replace(/<\/?b>/g, "**"))
        const msg = `${errors.length} errors were found in the condition.\n\n- ${errorsMd.join("\n- ")}`
        alert("condition-error", "Errors in the condition", msg).then(() => this.errorsButton.focus())
    }

    render() {
        const vars = this.fromZone?.listAllDefinedVariables() ?? []
        return html`
            <div class="line">
                <label for="#setting" class="property-name">${this.localizedPropName}</label>
                <input id="setting" type="checkbox" checked=${ifDefined(this.condition !== null ? 1 : undefined)}
                       @change=${this.toggleDefined}/>
            </div>
            ${when(this.condition !== null, () => html`
                <ol>
                    <li class="${classMap({ red: this.condition.expressionHasErrors })}">
                        <div class="row">
                            ${when(this.condition.expressionHasErrors, () => {
                                return html`
                                    <button class="has-icon error" title="Condition has errors"
                                            aria-label="Condition has errors"
                                            @click="${this.showErrorAlert}"></button>
                                `
                            })}
                            <button class="cie-display" @click="${this.editCondition}">
                                ${this.condition.calcIfExpression.map(t => {
                                    if(t.type.startsWith("variable-")) {
                                        const varUUID = CalcIf.ObjectTypeTokenType.getInternalValue(t.value)
                                        const varDef = vars.find(v => v.uuid === varUUID)
                                        return html`<span
                                                class="cie-token cie-object cie-${t.type} has-icon ${t.type} icon-medium"
                                                data-name="${varDef?.name}" data-undefined="${varDef !== undefined}"></span>`
                                    } else {
                                        return html`<span class="cie-${t.type} cie-token"
                                                          data-value="${t.value}">${t.value}</span>`
                                    }
                                })}
                                <span class="has-icon edit">Edit</span>
                            </button>
                        </div>
                    </li>
                </ol>
            `)}`
    }
}