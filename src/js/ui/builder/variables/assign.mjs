/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import { customElement, property, query } from "lit/decorators.js"
import { BaseElement } from "../../element.mjs"
import { alert, openCalcIfEditor } from "../../popup/index.mjs"
import { html } from "lit"
import { classMap } from "lit/directives/class-map.js"
import { repeat } from "lit/directives/repeat.js"
import { when } from "lit/directives/when.js"

import { Variable, CalcIf } from "#abstract"
import { compareUUIDs, SelectOption } from "#utils"


import style from "#css/shadows/variable/assignments.scss"
import { ObjectTypeTokenType } from "../../../abstract/calcif/index.mjs"

@customElement("variable-assignments")
export class AssignVariablesProperty extends BaseElement {
    static styles = [style]

    /** @type {Set<VariableAssignment>} */
    @property()
    accessor assignments

    /** @type {Zone} */
    @property()
    accessor fromZone

    /** @type {Zone} */
    @property()
    accessor targetZone

    @property({ type: String })
    accessor showingAssignmentPicker

    @property({ type: String })
    accessor showingBehaviorOnOverflowPicker


    initialize() {

    }

    /**
     * Returns the definition by which the given event target is associated with.
     * @param {Event} e
     * @return {VariableAssignment}
     */
    findTargetAssignment(e) {
        const uuid = e.currentTarget.closest("[data-uuid]").getAttribute("data-uuid")
        return this.assignments.find(d => d.uuid === uuid)
    }

    /**
     * Returns a list of TokensError for the given assignment.
     * @param {VariableAssignment} assignment
     * @returns {TokensError[]}
     */
    listVariableErrors(assignment) {
        const variable = assignment.getTargetDefinitionWithin(this.targetZone?.listAllDefinedVariables() ?? [])

        let errors = []
        if(variable === undefined)
            errors.push(new CalcIf.TokensError("Variable is <b>undefined</b>. Tip: Click to switch it.", []))
        else {
            const varType = Variable.TYPE_TO_CALCIFTYPE.get(variable.varType)
            const vars = this.fromZone?.listAllDefinedVariables() ?? []
            const objectValues = Variable.convertDefinitionsToObjectValues(vars)
            errors = CalcIf.identifyErrors({
                tokens: assignment.calcIfExpression,
                tokenTypes: CalcIf.DEFAULT_TOKEN_TYPES,
                expectedType: varType,
                objectValues
            })
        }
        console.log("Errors:", errors)
        return errors
    }

    /**
     * Creates the assignment when the user picks a variable to be assigned to.
     * @param {VariableDefinition} varDef
     * @param {string?} uuid - Unique identifier of the variable.
     * @return {VariableAssignment}
     */
    createNewAssignment(varDef, uuid = null) {
        let assign = null
        if(varDef instanceof Variable.Definition)
            switch(varDef.varType) {
                case Variable.Type.INT:
                    assign = new Variable.Assignment({
                        targetUUID: varDef.uuid,
                        calcIfExpression: CalcIf.parse("0", CalcIf.DEFAULT_TOKEN_TYPES),
                        expressionHasErrors: false,
                        uuid
                    })
                    break
                case Variable.Type.BOOL:
                    assign = new Variable.Assignment({
                        targetUUID: varDef.uuid,
                        calcIfExpression: CalcIf.parse("true", CalcIf.DEFAULT_TOKEN_TYPES),
                        expressionHasErrors: false,
                        uuid
                    })
                    break
                default:
                    throw new Error(`Unknown variable type ${JSON.stringify(varDef.varType)}.`)
            }
        return assign
    }

    /**
     * Adds a new assignment to the list to the selected variable.
     * @param {CustomEvent} e
     */
    addNewAssignment(e) {
        const targetVarDefs = this.targetZone.listAllDefinedVariables()
        const varDef = targetVarDefs.find(d => d.uuid === e.detail)
        this.assignments.add(this.createNewAssignment(varDef))
        console.log("Added new assignment", this.assignments)
        this.showingAssignmentPicker = null
        this.dispatchEvent(new CustomEvent("updated"))
        this.requestUpdate()
    }

    /**
     * Shows the error message as an alert.
     * @param {TokensError[]} errors
     * @param {HTMLElement} target
     */
    showErrorAlert(errors, target) {
        const errorsMd = errors.map(e => e.message.replace(/<\/?b>/g, "**"))
        const msg = `${errors.length} errors were found in the assignment.\n\n- ${errorsMd.join("\n- ")}`
        alert("assignment-error", "Errors in the assignment", msg).then(() => target.focus())
    }

    /**
     * Changes assignment variable to a new one based on the newly selected one.
     * @param {CustomEvent} e
     */
    switchVariable(e) {
        const targetVarDefs = this.targetZone.listAllDefinedVariables()
        const varDef = targetVarDefs.find(d => d.uuid === e.detail)
        const assignment = this.findTargetAssignment(e)
        this.assignments.delete(assignment)
        this.assignments.add(this.createNewAssignment(varDef, assignment.uuid))
        this.showingAssignmentPicker = null
        this.dispatchEvent(new CustomEvent("updated"))
        this.requestUpdate()
    }

    /**
     * Deletes the associated assignment from the list.
     * @param {Event} e
     */
    deleteAssignment(e) {
        const assignment = this.findTargetAssignment(e)
        this.assignments.delete(assignment)
        this.dispatchEvent(new CustomEvent("updated"))
        this.requestUpdate()
    }

    /**
     * Open an editor to edit the associated assignment.
     * @param {Event} e
     */
    editAssignment(e) {
        const assignment = this.findTargetAssignment(e)
        const usableDefs = this.fromZone?.listAllDefinedVariables() ?? []
        const targetVarDefs = this.targetZone?.listAllDefinedVariables() ?? []
        const varDef = targetVarDefs.find(d => d.uuid === assignment.targetUUID)
        if(varDef !== undefined) { // Do NOT open if the target variable is not defined.
            const targetType = Variable.TYPE_TO_CALCIFTYPE.get(varDef.varType)
            openCalcIfEditor(`Set variable ${varDef.name} to:`, assignment.calcIfExpression, usableDefs, targetType).then((results) => {
                assignment.calcIfExpression = results
                assignment.expressionHasErrors = this.listVariableErrors(assignment).length !== 0
                this.dispatchEvent(new CustomEvent("updated"))
                this.requestUpdate()
            })
        }
    }

    /**
     * Shows the popup with information about behavior on overflow.
     */
    showBehaviorOnOverflowInfo(e) {
        const btn = e.target.querySelector("#info-behavior-on-overflow")
        const msg = `Overflows (and underflows) are events that occur when the integer value of the variable goes above the maximum (or under zero). Setting the behavior when such events occur will change how certain variables will behave in unexpected conditions.

- **Clamp:** (default) This behavior will ensure the value will no longer increase when it hits its maximum (or decrease when it hits zero)
- **Modulo:** This behavior will calculate the module of the over/underflown value compared to its maximum (e.g. -1 will become maximum, maximum + 2 will become 1…).

If you're unsure of which one you should use, **clamp** will ensure your variable won't reset, and will thus be probably safer for your usecase.

Note that this parameter is set *per assignment*, which means a single variable can have *different* behaviors on overflow depending on the assignment.`
        alert("info-behavior-on-overflow", "About behavior on overflow/underflow.", msg).then(() => btn.focus())
    }

    /**
     * Shows the select menu to pick a new behavior or overflow for our current assignment.
     */
    showBehaviorOnOverflowPicker(e) {
        const assignment = this.findTargetAssignment(e)
        this.showingBehaviorOnOverflowPicker = assignment.uuid
        this.requestUpdate()
    }

    /**
     * The user has selected a new behavior on overflow for this assignment.
     * @param {CustomEvent} e
     */
    switchBehaviorOnOverflow(e) {
        const assignment = this.findTargetAssignment(e)
        assignment.behaviorOnOverflow = e.detail
        this.showingBehaviorOnOverflowPicker = false
        this.dispatchEvent(new CustomEvent("updated"))
        this.requestUpdate()
    }

    /**
     * Shows the variable picker to add a new assignment.
     * @param {Event} e
     */
    showVariableSwitchPicker(e) {
        if(this.assignments.size < this.targetZone.listAllDefinedVariables().length) {
            const assignment = this.findTargetAssignment(e)
            this.showingAssignmentPicker = assignment.uuid
            this.requestUpdate()
        }
    }

    /**
     * Shows the variable picker to add a new assignment.
     */
    showNewAssignmentPicker() {
        this.showingAssignmentPicker = "-1"
    }

    render() {
        const assignments = Array.from(this.assignments).sort((a, b) => compareUUIDs(a.uuid, b.uuid))
        const targetVarDefs = this.targetZone?.listAllDefinedVariables() ?? []
        const assignedUUIDs = assignments.map(a => a.targetUUID)
        const availableTargetVarDefs = targetVarDefs.filter(d => !assignedUUIDs.includes(d.uuid))
        return html`
            <ol>
                ${repeat(assignments, a => a.uuid, a => this.renderAssignment(a, targetVarDefs, availableTargetVarDefs))}
                <li class="create">
                    ${when(this.showingAssignmentPicker === "-1", () => {
                        const selects = availableTargetVarDefs.map(v => new SelectOption({
                            value: v.uuid,
                            text: v.name,
                            icon: `variable-${v.varType}`
                        }))
                        return html`
                            <select-menu id="choose-type" .options="${selects}"
                                         @selected="${this.addNewAssignment}"></select-menu>`
                    })}
                    ${when(availableTargetVarDefs.length > 0, () => {
                        return html`
                            <button class="has-icon add" @click="${this.showNewAssignmentPicker}">
                                Add new assignment
                            </button>`
                    })}
                    ${when(availableTargetVarDefs.length === 0 && targetVarDefs.length > 0, () => {
                        const txt = "All variables from the exit block's zone have been set."
                        return html`<span class="info">${txt}</span>`
                    })}
                    ${when(targetVarDefs.length === 0, () => {
                        const txt = this.targetZone === null ? "Exit block is not within a zone." : "Exit block's zone does not have any defined variable."
                        return html`<span class="info">${txt}</span>`
                    })}

                </li>
            </ol>
        `
    }

    /**
     *
     * @param {VariableAssignment} a
     * @param {VariableDefinition[]} targetVarDefs
     * @param {VariableDefinition[]} availableTargetVarDefs
     * @return {TemplateResult<1>}
     */
    renderAssignment(a, targetVarDefs, availableTargetVarDefs) {
        const target = targetVarDefs.find(d => d.uuid === a.targetUUID)
        const containsVariables = a.calcIfExpression.find(t => t.type.startsWith("variable-"))
        let maximum = Infinity
        if(!containsVariables && !a.expressionHasErrors)
            maximum = CalcIf.buildOperationStreamAST(new CalcIf.TokenStream(a.calcIfExpression))
                            .evaluate(new Map())
        const usableVariables = this.fromZone?.listAllDefinedVariables() ?? []
        const canBeSuperiorToMaximum = containsVariables || (maximum > target?.maximum)
        const needsToAskAboutBehaviorOnOverflow = target?.varType === Variable.Type.INT && canBeSuperiorToMaximum
        return html`
            <li data-uuid="${a.uuid}" class="${classMap({ red: target === undefined || a.expressionHasErrors })}">
                ${when(this.showingAssignmentPicker === a.uuid, () => {
                    const options = availableTargetVarDefs.map(v => new SelectOption({
                        value: v.uuid,
                        text: v.name,
                        icon: `variable-${v.varType}`
                    }))
                    return html`
                        <select-menu id="choose-var" .options="${options}"
                                     @selected="${this.switchVariable}"></select-menu>`
                })}
                <div class="row management">
                    <div class="row assignment">
                        ${when(target === undefined || a.expressionHasErrors, () => {
                            const errors = this.listVariableErrors(a)
                            return html`
                                <button class="has-icon error" title="Assignment has errors"
                                        aria-label="Assignment has errors"
                                        @click="${{ handleEvent: (e) => this.showErrorAlert(errors, e.target) }}"></button>
                            `
                        })}
                        <button class="variable-full has-icon icon-medium variable-${target?.varType}"
                                title="Switch variable"
                                aria-label="Switch variable"
                                @click="${this.showVariableSwitchPicker}">
                            ${target?.name ?? "<undefined>"}
                        </button>
                        <span class="equal">:=</span>
                        <button class="cie-display" @click="${this.editAssignment}">
                            ${a.calcIfExpression.map(t => {
                                if(t.type.startsWith("variable-")) {
                                    const varUUID = CalcIf.ObjectTypeTokenType.getInternalValue(t.value)
                                    const varDef = usableVariables.find(v => v.uuid === varUUID)
                                    return html`<span
                                            class="cie-token cie-object cie-${t.type} has-icon ${t.type} icon-medium"
                                            data-name="${varDef?.name}" data-undefined="${varDef !== undefined}"></span>`
                                } else {
                                    return html`<span class="cie-${t.type} cie-token"
                                                      data-value="${t.value}">${t.value}</span>`
                                }
                            })}
                            <span class="has-icon edit">Edit</span>
                        </button>
                    </div>
                    <button class="has-icon icon-medium trash" title="Delete assignment" aria-label="Delete assignment"
                            @click="${this.deleteAssignment}"></button>
                </div>
                ${when(needsToAskAboutBehaviorOnOverflow, () => {
                    const max = target.maximum
                    const options = [
                        new SelectOption({
                            value: Variable.BehaviorOnOverflow.CLAMP,
                            text: `Clamp (-1 → 0 and ${max + 1} → ${max})`
                        }),
                        new SelectOption({
                            value: Variable.BehaviorOnOverflow.MODULO,
                            text: `Modulo (-1 → ${max} and ${max + 1} → 0)`
                        })
                    ]
                    const text = options.find(o => o.value === a.behaviorOnOverflow).text
                    return html`
                        <div class="row behavior-overflow">
                            <button id="info-behavior-on-overflow-${a.uuid}"
                                    aria-labelledby="behavior-on-overflow-${a.uuid}"
                                    class="has-icon icon-medium manual"
                                    @click="${this.showBehaviorOnOverflowInfo}"></button>
                            <label id="behavior-on-overflow-${a.uuid}" for="info-behavior-on-overflow-${a.uuid}">
                                Behavior on overflow:
                            </label>
                        </div>
                        <button class="row" @click="${this.showBehaviorOnOverflowPicker}">
                            ${when(this.showingBehaviorOnOverflowPicker === a.uuid, () => {
                                return html`
                                    <select-menu id="choose-behavior-on-overflow" .options="${options}"
                                                 @selected="${this.switchBehaviorOnOverflow}"></select-menu>`
                            })}
                            ${text}
                        </button>
                    `
                })}
            </li>
        `
    }
}