/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseElement } from "../../../element.mjs"
import { confirm } from "../../../popup/index.mjs"
import { html } from "lit"
import { query, customElement, property } from "lit/decorators.js"
import { classMap } from "lit/directives/class-map.js"
import { ifDefined } from "lit/directives/if-defined.js"
import { Zone } from "#logic-builder/zones/"
import { AbstractZone } from "#abstract/structures"
import { getRandomColor } from "#utils"

import style from "#css/shadows/panel/zones.scss"

@customElement("zones-list")
export class ZonesList extends BaseElement {
    static styles = [style]

    /**
     * @type {Zone|null}
     */
    @property({ attribute: false })
    accessor parentZone = null

    /**
     * @type {Zone|null}
     */
    @property({ attribute: false })
    accessor selectedZone = null

    initialize() {

    }

    requestUpdate(name, oldValue, options) {
        super.requestUpdate(name, oldValue, options)
    }

    /**
     * Returns the Zone associated to the event.
     * @param {Event} e
     * @returns {Zone}
     */
    targetZoneFromEvent(e) {
        let targetElement = e.target.closest("details")
        let targetId = parseInt(targetElement.getAttribute("data-zone-id"))
        return Zone.listChildrenOf(this.parentZone).find(z => z._id === targetId)
    }


    toggled(e) {
        e.stopPropagation()
        let target = this.targetZoneFromEvent(e)
        this.dispatchEvent(new CustomEvent("zoneSelected", {
            detail: {
                zone: target
            }
        }))
    }

    /**
     * Clicked the button to add a new subzone.
     * @param e
     */
    addZone(e) {
        let target = this.targetZoneFromEvent(e)
        let zonesList = e.target.closest("details").querySelector("zones-list")
        Zone.register(new Zone({
            blockData: new AbstractZone({
                parentZone: target.uuid,
                color: getRandomColor(true),
            })
        }))
        zonesList.requestUpdate()
    }

    /**
     * Clicked the button to delete the zone.
     * @param e
     */
    deleteZone(e) {
        let target = this.targetZoneFromEvent(e)
        let msg = "This action will completely remove this zone AND ALL OF ITS CHILDREN from ALL logic diagrams. THIS OPERATION IS NOT REVERSIBLE! Continue?"
        confirm("delete-zone", "Delete Zone?", msg).then(() => {
            target.destroy()
            this.requestUpdate()
        })
    }

    render() {
        let zones = Zone.listChildrenOf(this.parentZone) ?? []
        let childZoneCount = zones.map(z => Zone.listChildrenOf(z).length)
        return html`${zones.map((z, i) => html`
            <details style="--color: ${z.blockData.color}" data-zone-id="${z._id}" @click=${this.toggled}
                     open="${ifDefined(childZoneCount[i] !== 0 ? "true" : undefined)}">
                <summary class="${classMap({
                    // "hide-marker": childZoneCount[i] === 0,
                    "selected": this.selectedZone === z
                })}">
                    <div class="summary--inner">
                        <div class="title">${z.blockData.title}</div>
                        <button class="has-icon add" title="Add new sub-zone" @click=${this.addZone}></button>
                        <button class="has-icon trash" title="Delete zone" @click=${this.deleteZone}></button>
                        <div class="plus">${childZoneCount[i] === 0 ? "" : `+${childZoneCount[i]}`}</div>
                    </div>
                </summary>
                <div class="child-zones">
                    <zones-list .parentZone=${z} .selectedZone=${this.selectedZone}
                                @zoneSelected=${this.forwardCustomEvent}></zones-list>
                </div>
            </details>
        `)}`
    }
}
