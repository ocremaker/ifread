/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


export * from "./list"

import { BaseElement } from "../../../element.mjs"
import { customElement, property, query } from "lit/decorators.js"
import { html } from "lit"
import { Zone } from "#logic-builder/zones/"

import style from "#css/shadows/panel/base.scss"

@customElement("drawer-panel-zones")
export class DrawerPanelZones extends BaseElement {
    static styles = [style]

    @property({ attribute: false })
    accessor selectedTool = "move"

    @property({ attribute: false })
    accessor stage

    @property({ attribute: false })
    accessor selectedZone

    @query("zones-list")
    accessor zonesList

    initialize() {
    }

    /**
     * @param {UIEvent} e
     */
    toolClicked(e) {
        this.selectTool(e.target.getAttribute("tool-name"))
    }

    selectTool(toolName) {
        this.selectedTool = toolName
        this.dispatchEvent(new CustomEvent("toolChanged", {
            detail: {
                newTool: this.selectedTool
            }
        }))
    }

    addZone() {
        Zone.register(new Zone({}))
        console.log("Registered new zone")
        this.zonesList.requestUpdate()
    }

    render() {
        return html`
            <div class="zone-tools" role="toolbar">
                <button class="tool has-icon tool-move" tool-name="move" aria-selected="${this.selectedTool === "move"}" @click=${this.toolClicked}>
                    Move
                </button>
                <button class="tool has-icon tool-paint" tool-name="paint" aria-selected="${this.selectedTool === "paint"}" @click=${this.toolClicked}>
                    Paint
                </button>
                <button class="tool has-icon tool-erase" tool-name="erase" aria-selected="${this.selectedTool === "erase"}" @click=${this.toolClicked}>
                    Erase
                </button>
                <button class="tool has-icon tool-square-fill" tool-name="fill" aria-selected="${this.selectedTool === "fill"}" @click=${this.toolClicked}>
                    Fill
                </button>
                <button class="tool has-icon tool-square-empty" tool-name="empty" aria-selected="${this.selectedTool === "empty"}" @click=${this.toolClicked}>
                    Empty
                </button>
            </div>
            <h2 class="drawer-title">Zones</h2>
            <div id="zones-list" class="drawer-scroll-container">
                <zones-list .stage=${this.stage} .selectedZone=${this.selectedZone} @zoneSelected=${this.forwardCustomEvent}></zones-list>
                <button class="add-zone" @click=${this.addZone}>Add new zone</button>
            </div>`

    }
}