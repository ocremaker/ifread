/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { html } from "lit"
import { customElement, property, query } from "lit/decorators.js"
import { ifDefined } from "lit/directives/if-defined.js"
import { BaseElement } from "#ui/element.mjs"
import { DataContainer, Variable } from "#abstract"
import { Color } from "#abstract/types"

import style from "#css/shadows/panel/property.scss"

@customElement("property-value")
export class PropertyValue extends BaseElement {
    static styles = [style]

    @property()
    accessor associatedObject

    @property()
    accessor property

    @query("input")
    accessor input

    initialize() {
    }

    applyChange(newValue) {
        const oldValue = this.associatedObject.blockData[this.property.name]
        this.associatedObject.blockData[this.property.name] = newValue
        this.associatedObject.fire(`${this.property.name}Changed`, { oldValue, newValue })
        this.dispatchEvent(new CustomEvent("propertyChanged", {}))
    }

    validateStringInput(e) {
        if(e.key === "Enter")
            if(this.input.validity.valid) {
                let value = null
                switch(this.property.type) {
                    case String:
                        value = this.input.value
                        break
                    case Number:
                        value = parseFloat(this.input.value) || null
                        break
                }
                if(value !== null)
                    this.applyChange(value)
            }
    }

    toggleBoolean() {
        this.applyChange(this.input.checked)
    }

    setColor() {
        this.applyChange(this.input.value)
    }

    updateSet(property, value) {
        console.log("Updating set", property)
        this.associatedObject.fire(`${property}Changed`, { oldValue: value, newValue: value })
    }

    render() {
        let value = this.associatedObject.blockData[this.property.name]
        const p = this.property
        const localizedName = (p.name[0].toUpperCase() + p.name.substring(1))
        if(p.type === String && p.container === DataContainer.NONE) {
            return html`
                <div class="line">
                    <label for="#prop-${p.name}" class="property-name">${localizedName}</label>
                    <input id="prop-${p.name}" type="text" value="${value}" @keydown=${this.validateStringInput}
                           enterkeyhint="enter"/>
                </div>`
        } else if(p.type === Number && p.container === DataContainer.NONE) {
            return html`
                <div class="line">
                    <label for="#prop-${p.name}" class="property-name">${localizedName}</label>
                    <input id="prop-${p.name}" type="number" value="${value}" @keydown=${this.validateStringInput}
                           enterkeyhint="enter"/>
                </div>`
        } else if(p.type === Boolean && p.container === DataContainer.NONE) {
            return html`
                <div class="line">
                    <label for="#prop-${p.name}" class="property-name">${localizedName}</label>
                    <input id="prop-${p.name}" type="checkbox" checked=${ifDefined(value ? 1 : undefined)}
                           @change=${this.toggleBoolean}/>
                </div>`
        } else if(p.type === Color && p.container === DataContainer.NONE) {
            return html`
                <div class="line">
                    <label for="#prop-${p.name}" class="property-name">${localizedName}</label>
                    <input id="prop-${p.name}" type="color" value="${value}" @input=${this.setColor}/>
                </div>`
        } else if(p.type === Variable.Definition && p.container === DataContainer.SET) {
            return html`
                <div class="column">
                    <label class="property-name">${localizedName}</label>
                    <variable-definitions .definitions="${value}" .zone="${this.associatedObject}"
                                          @updated="${{handleEvent: () => this.updateSet(p.name, value)}}">
                    </variable-definitions>
                </div>`
        } else if(p.type === Variable.Assignment && p.container === DataContainer.SET) {
            const fromBlock = this.associatedObject.fromSocket.findAncestor(".Block")
            const toBlock = this.associatedObject.toSocket.findAncestor(".Block")
            const fromZone = fromBlock.zone()
            const targetZone = toBlock.zone()
            return html`
                <div class="column">
                    <label class="property-name">${localizedName}</label>
                    <variable-assignments .fromZone="${fromZone}" .targetZone="${targetZone}" .assignments="${value}"
                                         @updated="${{handleEvent: () => this.updateSet(p.name, value)}}">
                    </variable-assignments>
                </div>`
        } else if(p.type === Variable.Condition && p.container === DataContainer.NONE) {
            const fromBlock = this.associatedObject.fromSocket.findAncestor(".Block")
            const toBlock = this.associatedObject.toSocket.findAncestor(".Block")
            const fromZone = fromBlock.zone()
            const targetZone = toBlock.zone()
            return html`
                <div class="column">
                    <variable-condition .fromZone="${fromZone}" .targetZone="${targetZone}"
                                        .localizedPropName="${localizedName}" .condition="${value}"
                                        @updated="${{handleEvent: (e) => this.applyChange(e.detail)}}">
                    </variable-condition>
                </div>`
        } else {
            throw new Error(`Cannot modify ${p.type.name} with container ${p.container}.`)
        }
    }
}
