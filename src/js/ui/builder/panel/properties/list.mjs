
import { Node } from "konva"
import { customElement, property, query } from "lit/decorators.js"
import { html } from "lit"
import { when } from "lit/directives/when.js"
import { repeat } from "lit/directives/repeat.js"
import { BaseBlock } from "#logic-builder/blocks/"
import { BaseElement } from "../../../element.mjs"

import style from "#css/shadows/panel/properties.scss"

@customElement("drawer-panel-properties")
export class DrawerPanelProperties extends BaseElement {
    static styles = [style]

    /**
     * @type {BaseBlock|Note|Zone}
     */
    @property()
    accessor associatedObject

    @query("property-value")
    accessor firstProperty

    initialize() {
    }

    buttonClicked() {
        let btn = this.associatedObject.container.findOne(".Button")
        btn.click()
    }

    focus() {
        // Focus first property.
        this.firstProperty.input.focus()
    }

    render() {
        const obj = this.associatedObject
        const btn = obj.findOne?.call(obj, ".Button") ?? null
        const selectedTitle = obj.blockData.title === "" || !obj.blockData.title ? obj.constructor.displayName : obj.blockData.title
        const selectTitleClass = obj.constructor.name.toLowerCase()
        const properties = obj.blockData.constructor.properties
                              .filter(p => p.userEditable instanceof Function ? p.userEditable(obj) : !!p.userEditable)
        return html`
            <h2 class="drawer-title">Properties of
                ${when(!(obj instanceof BaseBlock),
            () => html`${selectedTitle}`,
            () => html`
                            <span class="block-bg ${selectTitleClass}">
                                <span class="block-title">${selectedTitle}</span>
                            </span>`
        )}
            </h2>
            <div id="properties-list" class="drawer-scroll-container">
                ${repeat(properties, p => p.name, prop => {
                    return html`
                        <property-value id="prop-${prop.name}" .property=${prop}
                                        .associatedObject=${obj} @propertyChanged=${this.forwardCustomEvent}>`
                })}
            </div>
            ${when(btn !== null,
            () => {
                return html`<div class="selected-action" style="background: ${this.associatedObject.titleNode?.background.fill()}">
                                <button @click=${this.buttonClicked}>
                                    ${btn.text()}
                                </button>
                            </div>`
            }
        )}`

    }
}