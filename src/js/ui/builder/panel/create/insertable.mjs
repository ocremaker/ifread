/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseElement } from "../../../element.mjs"
import { Socket } from "#logic-builder/shapes"
import { Anchor } from "#logic-builder/utils"
import { COLORS } from "#logic-builder/link.mjs"
import { Drag } from "#utils"

import { html } from "lit"
import { customElement, property, query } from "lit/decorators.js"

import style from "#css/shadows/panel/insertable.scss"

const POSITION_CLASSES = new Map([
    [Anchor.TOP | Anchor.LEFT, "position-top-left"],
    [Anchor.TOP, "position-top"],
    [Anchor.TOP | Anchor.RIGHT, "position-top-right"],
    [Anchor.LEFT, "position-left"],
    [Anchor.RIGHT, "position-right"],
    [Anchor.BOTTOM | Anchor.LEFT, "position-bottom-left"],
    [Anchor.BOTTOM, "position-bottom"],
    [Anchor.BOTTOM | Anchor.RIGHT, "position-bottom-right"],
])

@customElement("insertable-block")
export class InsertableBlock extends BaseElement {
    static styles = [style]

    @property()
    accessor associatedType

    // @query("[draggable]")
    // accessor block

    initialize() {
        console.log(`Initialized block with type ${this.associatedType.name}`)
        this.setAttribute("grab", "supported")
        this.setAttribute("draggable", "true")
        Drag.listenStart(this, (e) => this.dragStart(e))
    }

    dragStart(e) {
        e.dataTransfer.setData("x-ifread/node", this.associatedType.name)
        this.setAttribute("grab", "true")
    }

    cloneNode(deep) {
        const clone = super.cloneNode(deep)
        clone.associatedType = this.associatedType
        return clone
    }

    render() {
        return html`
            <div class="${this.associatedType.name.toLowerCase()} block">
                ${this.associatedType.socketModels?.map(model => {
                    return html`
                        <div class="socket ${POSITION_CLASSES.get(model.anchor)}"
                             style="background: ${COLORS.get(model.linkType)}"></div>`
                })}
                ${this.associatedType.displayName}
            </div>`
    }
}