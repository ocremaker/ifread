/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export * from "./insertable"

import { customElement, property } from "lit/decorators.js"
import { html } from "lit"
import { BaseElement } from "../../../element.mjs"
import { BLOCK_CATEGORIES } from "#logic-builder/blocks/"

import style from "#css/shadows/panel/base.scss"

@customElement("drawer-panel-create")
export class DrawerPanelCreate extends BaseElement {
    static styles = [style]

    @property()
    accessor nodeTypesList

    initialize() {}

    render() {
        return html`
            <h2 class="drawer-title">Create</h2>
            <div id="create-list" class="drawer-scroll-container">
                ${BLOCK_CATEGORIES[this.nodeTypesList].map(nodeType =>
                    html`<insertable-block .associatedType="${nodeType}"></insertable-block>`
                )}
            </div>`
    }
}