/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { MenuItem, SEPARATOR } from "#utils"
import {
    createNewFile,
    exportCurrentDiagram,
    loadFile,
    saveChapterVersionToOpenedFile
} from "#io"
import { DataContainer, serialize, unserialize } from "#abstract"
import { createFlatBlockRow, IfreadSaveFile } from "#abstract/structures"
import { keyboardShortcuts } from "../popup/"

/**
 *
 * @type {Object.<string, (MenuItem|symbol)[]>}}
 */
export const MENUS = {
    "&File": [
        new MenuItem({
            text: "&New",
            icon: "new",
            shortcut: "Ctrl+Alt+N",
            description: "Discard current chapter and creates a new blank chapter",
            callback: () => createNewFile()
        }),
        new MenuItem({
            text: "&Open",
            icon: "open",
            shortcut: "Ctrl+O",
            description: "Opens a previously saved chapter from an Ifread file",
            callback: () => {
                // TODO: Choose file interface when we have more.
                window.globals.save.interface.showOpenDialog().then(file => {
                    file.text().then(text => {
                        /** @type {IfreadSaveFile} */
                        const parsedData = unserialize(JSON.parse(text), IfreadSaveFile, DataContainer.NONE)
                        loadFile(parsedData)
                        console.log("Open!")
                    })
                })
            }
        }),
        SEPARATOR,
        new MenuItem({
            text: "&Save",
            icon: "save",
            shortcut: "Ctrl+S",
            description: "Saves the current chapter into an Ifread file",
            callback: () => {
                saveChapterVersionToOpenedFile()
                const fileData = serialize(window.globals.save.fileData, IfreadSaveFile, DataContainer.NONE)
                window.globals.save.interface.save(JSON.stringify(fileData)).then(() => {
                    console.log("Save!", window.globals.save.fileData)
                    console.log("Serialized!", fileData)
                })
            }
        }),
        new MenuItem({
            text: "E&xport",
            icon: "export",
            shortcut: "Ctrl+E",
            description: "Exports the chapter into an interactive readable format",
            callback: () => {
                console.log("Export!", createFlatBlockRow(exportCurrentDiagram()))
            }
        }),
        SEPARATOR,
        new MenuItem({
            text: "&Properties",
            icon: "settings",
            shortcut: "Ctrl+Alt+P",
            description: "Checks and modifies the properties of the chapter",
            callback: () => console.log("Properties!")
        })
    ],
    "&Edit": [
        new MenuItem({
            text: "&Copy",
            icon: "copy",
            shortcut: "Ctrl+C",
            description: "Copies the selected block",
            callback: () => console.log("Copy!")
        }),
        new MenuItem({
            text: "&Paste",
            icon: "paste",
            shortcut: "Ctrl+V",
            description: "Pasts the copied block",
            callback: () => console.log("Paste!")
        }),
        new MenuItem({
            text: "&Duplicate",
            icon: "duplicate",
            shortcut: "Ctrl+D",
            description: "Duplicates the selected block",
            callback: () => console.log("Duplicate!")
        })
    ],
    "&Help": [
        new MenuItem({
            text: "&Keyboard shortcuts",
            icon: "info",
            callback: () => keyboardShortcuts()
        }),
        new MenuItem({
            text: "&About Ifread",
            icon: "info",
            callback: () => console.log("Info!")
        })
    ]
}

export const ITEMS = Object.values(MENUS).flat().filter(m => m !== SEPARATOR)