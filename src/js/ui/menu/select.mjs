/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { customElement, property, query } from "lit/decorators.js"
import { BaseElement } from "../element"
import { html } from "lit"
import { SEPARATOR } from "#utils"
import { unsafeHTML } from "lit/directives/unsafe-html.js"

import style from "#css/shadows/menu.scss"

@customElement("select-menu")
export class SelectMenu extends BaseElement {
    static styles = [style]

    @property()
    accessor options

    @query("button[data-id='0']")
    accessor firstBtn

    initialize() {
        this.firstBtn.focus()
    }

    clickedButton(e) {
        let btnId = parseInt(e.target.closest("button").getAttribute("data-id"))
        this.dispatchEvent(new CustomEvent("selected", {
            detail: this.options[btnId].value
        }))
    }

    render() {
        return html`
            <div class="menu-root" role="menu">
                ${this.options.map((btn, i) =>
                        html`
                            <button data-id="${i}" class="${btn.classMap}" @click=${this.clickedButton} role="menuitem">
                                <span class="text">${btn.text}</span>
                            </button>`
                )}
            </div>`
    }
}