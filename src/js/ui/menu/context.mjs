/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { customElement, property } from "lit/decorators.js"
import { BaseElement } from "../element"
import { html } from "lit"
import { SEPARATOR } from "#utils"
import { unsafeHTML } from "lit/directives/unsafe-html.js"

import style from "#css/shadows/menu.scss"

@customElement("block-context-menu")
export class BlockContextMenu extends BaseElement {
    static styles = [style]

    @property()
    accessor associatedFocusCaptor

    initialize() {
        if(this.associatedFocusCaptor.titleNode)
            this.associatedFocusCaptor.titleNode.edit.lock = true
        let menuAlts = this.buttons.filter(e => e !== SEPARATOR).map(mi => mi.altShortcut)
        this.addEventListener("keydown", (e) => {
            for(let menuShortcut of menuAlts)
                if(menuShortcut.checkMatchAndRun(e)) {
                    // console.log("Matched context menu")
                    this.remove()
                    return
                }
        })
    }

    get buttons() {
        return this.associatedFocusCaptor.contextMenu || []
    }

    clickedButton(e) {
        if(this.associatedFocusCaptor.titleNode)
            this.associatedFocusCaptor.titleNode.edit.lock = false
        let btnId = parseInt(e.target.closest("button").getAttribute("data-id"))
        this.buttons[btnId].callback()
        this.remove()
    }

    remove() {
        if(this.associatedFocusCaptor.titleNode)
            this.associatedFocusCaptor.titleNode.edit.lock = false
        super.remove()
    }

    render() {
        return html`
            <div class="menu-root" role="menu">
                ${this.buttons.map((btn, i) =>
                    html`
                        <button data-id="${i}" class="${btn.classMap}" @click=${this.clickedButton} role="menuitem">
                            <span class="text">${unsafeHTML(btn.parsedText)}</span><span class="shortcut">${btn.shortcut.toString()}</span>
                        </button>`
                )}
            </div>`
    }
}