/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { BaseElement } from "../element"
import { MENUS } from "./menus"
import { SEPARATOR, altNameToHTML, altShortcutFromString } from "#utils"

import { html } from "lit"
import { query, customElement, property } from "lit/decorators.js"
import { unsafeHTML } from "lit/directives/unsafe-html.js"
import { when } from "lit/directives/when.js"

import styleMain from "#css/shadows/menubar.scss"


@customElement("menu-bar")
export class MenuBar extends BaseElement {
    static styles = [styleMain]

    @query(".menu-titles")
    accessor menuTitles

    @property({ type: Number })
    accessor openMenu = -1

    constructor() {
        super()
    }

    initialize() {
        // Initialize shortcuts
        let menuOpenShortcuts = Object.keys(MENUS).map((title, i) => {
            let short = altShortcutFromString(title)
            short.callback = () => {
                this.openMenu = i
            }
            return short
        })
        let allShortcuts = Object.values(MENUS).map((m) =>
            m.filter(e => e !== SEPARATOR).map(mi => mi.shortcut)
        ).flat()
        let menusAlts = Object.values(MENUS).map((m) =>
            m.filter(e => e !== SEPARATOR).map(mi => mi.altShortcut)
        )
        window.addEventListener("keydown", (e) => {
            let t = e.target
            let popupEditor = document.querySelector("base-popup.popup-editor")
            if(popupEditor !== null || t?.closest('[contenteditable]') !== null || t?.tagName === "INPUT")
                return
            let shortcuts = [
                ...menuOpenShortcuts,
                ...allShortcuts,
                this.openMenu > -1 ? menusAlts[this.openMenu] : null
            ].filter(f => f != null && f != undefined)
            // Check each shortcut
            for(let menuShortcut of shortcuts)
                if(menuShortcut !== null && menuShortcut.checkMatchAndRun(e)) {
                    e.preventDefault()
                    if(!menuOpenShortcuts.includes(menuShortcut))
                        this.openMenu = -1
                    break
                }
        })
        // Add blur
        this.addEventListener("blur", () => this.openMenu = -1)
    }

    /**
     * Triggered when a menu title is clicked.
     * @param {Event} e
     */
    menuClick(e) {
        let target = e.target.closest("button")
        let clickedId = parseInt(target.getAttribute("data-id"))
        if(clickedId !== this.openMenu)
            this.openMenu = clickedId
        else
            this.openMenu = -1
    }

    /**
     * Triggered when a menu title is clicked.
     * @param {Event} e
     */
    menuHover(e) {
        if(this.openMenu !== -1)
            this.menuClick(e)
    }

    /**
     * Triggered when a button in the menu is clicked.
     * @param {Event} e
     */
    clickedButton(e) {
        let target = e.target.closest("button")
        let clickedId = parseInt(target.getAttribute("data-id"))
        let menu = Object.values(MENUS)[this.openMenu]
        menu[clickedId].callback()
    }


    render() {
        return html`
            <div class="menu-titles" role="menubar">${Object.keys(MENUS).map((m, i) =>
                    html`
                        <button class="menu-title" data-id="${i}" @mouseenter=${this.menuHover} @click=${this.menuClick} role="menuitem">
                            ${unsafeHTML(altNameToHTML(m))}
                        </button>`
            )}
            </div>
            ${when(this.openMenu === -1,
                    () => html``,
                    () => {
                        let menuLeft = this.menuTitles?.children[this.openMenu]?.getBoundingClientRect().left ?? 0
                        let items = Object.values(MENUS)[this.openMenu]
                        return html`
                            <div class="menu-root" style="left: ${menuLeft}px;" role="menu">
                                ${items.map((btn, i) =>
                                        html`${when(btn === SEPARATOR,
                                                () => html`
                                                    <div class="menu-separator" role="separator"></div>`,
                                                () => html`
                                                    <button data-id="${i}" class="${btn.classMap}"
                                                            @click=${this.clickedButton} role="menuitem">
                                                        <span class="text">${unsafeHTML(btn.parsedText)}</span><span class="shortcut">${btn.shortcut?.toString() ?? ""}</span>
                                                    </button>`
                                        )}`
                                )}
                            </div>`
                    }
            )}`
    }
}