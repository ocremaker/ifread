/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { classMap } from "lit/directives/class-map.js"
import { altNameToHTML, altShortcutFromString, Shortcut } from "./shortcut"

export const SEPARATOR = Symbol("SEPARATOR")

export class ButtonDeclarator {
    /**
     *
     * @param {string} text
     * @param {function()} callback
     * @param {string[]} classes - List of HTML classes for the button.
     * @param {string} icon
     */
    constructor({ text, callback, classes = [], icon = null }) {
        this.text = text
        this.callback = callback
        this.classes = classes
        if(icon != null)
            classes.push(icon, "has-icon")
        let classesObj = Object.fromEntries(classes.map(c => [c, true]))
        this.classMap = classMap(classesObj)
    }
}

export class MenuItem extends ButtonDeclarator {
    /**
     * @param {string|null} shortcut
     * @param {string} description
     * @param {{ text: string, callback: function, classes: Array?, icon: string }} options
     */
    constructor({ shortcut = null, description = "No description", ...options }) {
        super(options)
        this.parsedText = altNameToHTML(this.text)
        this.description = description
        this.shortcut = shortcut == null ? null : new Shortcut(shortcut, this.callback)
        this.altShortcut = altShortcutFromString(this.text)
        this.altShortcut.callback = options.callback
    }
}

export class SelectOption extends ButtonDeclarator {
    /**
     * @param {any} value
     * @param {{ text: string, classes: Array?, icon: string }} options
     */
    constructor({ value, ...options }) {
        super({ callback: () => value, ...options })
        this.value = value
    }
}