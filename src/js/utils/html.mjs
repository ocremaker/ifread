/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { encode } from "he";
import { html } from "lit"

/**
 * Parses an html string and returns the parsed nodes.
 * @param {string} html
 * @returns {ChildNode[]}
 */
export function parseHTMLNodes(html) {
    let div = document.createElement('div')
    div.innerHTML = html.trim()
    return Array.from(div.childNodes)
}

/**
 * Parses an html string and returns the first child element
 * @param {string} html
 * @returns {HTMLElement}
 */
export function parseHTML(html) {
    let div = document.createElement('div')
    div.innerHTML = html.trim()
    return div.children[0]
}

/**
 * Strips of all 'space' nodes between actual content.
 * @param {HTMLElement} htmlElement
 * @returns HTMLElement
 */
export function stripSpacesNodes(htmlElement) {
    for(const child of htmlElement.childNodes) {
        if(child.nodeType === Node.TEXT_NODE) {
            if(child.textContent.trim() === "")
                htmlElement.removeChild(child)
        } else if(child instanceof HTMLElement)
            stripSpacesNodes(child)
    }
    return htmlElement
}

/**
 * Parses a string into an safe string that can be inserted into HTML without causing issues.
 * @param {string} string
 * @returns {string}
 */
export function htmlEntities(string) {
    return encode(string)
}

function _splitToHTMLTags(string, separator, open, close) {
    let isOpen = false
    let parts = string.split(separator).map(s => {
        isOpen = !isOpen
        return s + (isOpen ? open : close)
    })
    let fused = parts.join("")
    if(isOpen)
        fused = fused.substring(0, fused.length - open.length)
    else
        fused = fused.substring(0, fused.length - close.length)
    return fused
}

/**
 * Parses markdown string and escapes the html entities, but parses some of the basic markdown elements.
 * @param {string} md
 * @return {string}
 */
export function parseSimpleMarkdown(md) {
    md = encode(md)
    md = "<p>" + md.replace(/\n\n/g, "</p><p>") + "</p>"
    // Parse lists
    const liSearch = /(^|\n|<p>)- ([^\n<]+)(\n|<\/p>|$)/g
    while(liSearch.test(md))
        md = md.replace(liSearch, "$1<li>$2</li>$3")
    md = md.replace(/<p><li>/g, "<ul><li>")
    md = md.replace(/<\/li><\/p>/g, "</li></ul>")
    // Parse linebreaks.
    md = md.replace(/\n/g, "<br>")
    md = md.replace(/!\[([a-z-]+)\]/g, `<span class="has-icon $1"></span>`)
    md = _splitToHTMLTags(md, "**", "<strong>", "</strong>")
    md = _splitToHTMLTags(md, "*", "<em>", "</em>")
    return md
}