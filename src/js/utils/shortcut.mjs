/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export class Shortcut {
    constructor(shortcutInfo = null, onTrigger = () => {}) {
        if(shortcutInfo === null)
            throw new Error("No shortcut info provided")
        let info = shortcutInfo.toLowerCase().split("+")
        this.ctrl = info.includes("ctrl")
        this.alt = info.includes("alt")
        this.shift = info.includes("shift")
        let keys = info.filter(i => !(["shift", "ctrl", "alt"].includes(i)))
        if(keys.length !== 1)
            throw new Error("More than one key provided.")
        this.key = keys[0].toLowerCase()
        this.callback = onTrigger
    }

    /**
     * Checks whether this shortcut is triggered in a given KeyboardEvent, and if so, runs the callback.
     * @param {KeyboardEvent} e
     * @returns {boolean}
     */
    checkMatchAndRun(e) {
        let isValid = this.key === e.key.toLowerCase()
        if(isValid) {
            isValid &&= !this.shift || e.shiftKey
            isValid &&= !this.alt || e.altKey
            isValid &&= !this.ctrl || (e.ctrlKey || e.metaKey) // For macOS
            if(isValid)
                this.callback()
        }
        return isValid
    }

    toString() {
        let ctrl = this.ctrl ? "Ctrl+" : ""
        let alt = this.alt ? "Alt+" : ""
        let shift = this.shift ? "Shift+" : ""
        let key = this.key[0].toUpperCase() + this.key.substring(1)
        return `${ctrl}${alt}${shift}${key}`
    }
}

/**
 * Parses a string with an "&" character, and returns the matching Alt shortcut.
 * @param {string} titleString
 * @returns Shortcut
 * @throws Error when no or an invalid & is provided in the string.
 */
export function altShortcutFromString(titleString) {
    let idx = titleString.lastIndexOf("&")
    if(idx === -1) throw new Error("No & provided in the string.")
    if(idx === titleString.length - 1) throw new Error("& provided at the end of the string.")
    let char = titleString[idx + 1].toLowerCase()
    return new Shortcut(`Alt+${char}`)
}

/**
 * Changes the & character to underline.
 * @param {string} titleString
 * @returns {string}
 */
export function altNameToHTML(titleString) {
    return titleString.replace(/&(\w)/g, "<u>$1</u>")
}