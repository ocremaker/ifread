/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Maps a channel (r, g, or b) number to an hex stirng.
 * @param {number} channel
 */
export function channelToHex(channel) {
    let value = Math.min(255, Math.max(0, channel)) // Clamp value
    value = value.toString(16)
    return value.length === 1 ? `0${value}` : value
}

/**
 * Darkens a given hex color by a percentage between 0 and 1.
 * @param {string} hex
 * @param {number} percentage
 * @returns {string}
 */
export function darkenColor(hex, percentage) {
    let channels = [hex.substring(1, 3), hex.substring(3, 5), hex.substring(5, 7)];
    channels = channels.map(c => Math.ceil(parseInt(c, 16)*(1-percentage)))
    channels = channels.map(c => channelToHex(c))
    return `#${channels.join("")}`
}

/**
 * Generates a random color
 * @param {boolean} light - If true, the generated color will be of the light variation, else it will be of the dark variation.
 */
export function getRandomColor(light = true) {
    let channels = [~~(Math.random()*64) , ~~(Math.random()*64) , ~~(Math.random()*64) ]
    if(light)
        channels = channels.map(c => c+164)
    channels = channels.map(c => channelToHex(c))
    return `#${channels.join("")}`
}