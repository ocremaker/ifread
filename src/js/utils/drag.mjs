/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const cachedTransferData = new Map()


/**
 *
 * @param {TouchEvent} e
 * @param {HTMLElement} clone
 * @param {DataTransfer} dataTransfer
 */
function moveTemporaryElement(e, clone, dataTransfer) {
    const pointer = e.targetTouches[0]
    clone.style.left = (pointer.clientX - clone.clientWidth/2) + 'px'
    clone.style.top = (pointer.clientY - clone.clientHeight/2) + 'px'
    const event = new DragEvent('dragmove', {
        dataTransfer, bubbles: true,
        ctrlKey: e.ctrlKey,
        metaKey: e.metaKey,
        shiftKey: e.ctrlKey,
        clientX: pointer.x, clientY: pointer.y,
        screenX: pointer.x, screenY: pointer.y
    })
    let target = document.elementFromPoint(pointer.clientX, pointer.clientY)
    while(target.shadowRoot)
        target = target.shadowRoot.elementFromPoint(pointer.clientX, pointer.clientY)
    target?.dispatchEvent(event)
}

/**
 *
 * @param {HTMLElement} originalTarget
 * @param {HTMLElement} clone
 * @param {function} moveListener
 * @param {DataTransfer} dataTransfer
 */
function dropTemporaryElement(originalTarget, clone, moveListener, dataTransfer) {
    const pointer = clone.getBoundingClientRect()
    originalTarget.removeEventListener('touchmove', moveListener)
    let target = document.elementFromPoint(pointer.x, pointer.y)
    while(target.shadowRoot)
        target = target.shadowRoot.elementFromPoint(pointer.x, pointer.y)
    const event = new DragEvent('drop', {
        dataTransfer, bubbles: true,
        clientX: pointer.x, clientY: pointer.y,
        screenX: pointer.x, screenY: pointer.y
    })
    target?.dispatchEvent(event)
    document.body.removeChild(clone)
}

/**
 * @param {HTMLElement} element
 * @param {function(DragEvent)} listener
 */
export function listenStart(element, listener) {
    element.addEventListener('dragstart', listener)
    element.addEventListener('touchstart', (e) => {
        const dataTransfer = new DataTransfer()
        const pointer = e.targetTouches[0]
        const clone = e.target.cloneNode(true)
        const move = (e) => moveTemporaryElement(e, clone, dataTransfer)
        e.target.addEventListener("touchmove", move)
        e.target.addEventListener("touchend", (e) => dropTemporaryElement(e.target, clone, move, dataTransfer), { once: true })
        clone.classList.add("touch-drag")
        clone.style.left = pointer.clientX + 'px'
        clone.style.top = pointer.clientY + 'px'
        document.body.appendChild(clone)
        listener(new DragEvent('dragstart', {
            bubbles: true,
            button: 0, buttons: 1,
            cancelable: false,
            clientX: pointer.clientX, clientY: pointer.clientY,
            ctrlKey: e.ctrlKey,
            metaKey: e.metaKey,
            shiftKey: e.ctrlKey,
            dataTransfer,
            screenX: pointer.screenX, screenY: pointer.screenY,
        }))
    })
}