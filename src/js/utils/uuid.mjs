/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * Generates a random and unique string.
 * Sorting several UUIDs will give them by their order of
 * @returns {string}
 */
export function generateUUID() {
    return Date.now().toString(36) + Math.random().toString(16).slice(4)
}

/**
 * Compares the two UUIDs to see which was created first.
 * @param {string} uuid1
 * @param {string} uuid2
 */
export function compareUUIDs(uuid1, uuid2) {
    const int1 = parseInt(uuid1.substring(0,8), 36)
    const int2 = parseInt(uuid2.substring(0,8), 36)
    return int1 - int2
}