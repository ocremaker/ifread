/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Clamps the value between two numbers.
 * @param {number} min
 * @param {number} max
 * @param {number} value
 * @return {number}
 */
Math.clamp = function(min, max, value) {
    return Math.max(Math.min(min, max), Math.min(max, value))
}

/**
 * Fuzzy matches this string against a term and returns a number <= 1 regarding how much they match.
 * (1 being the same string)
 * @param {string} compareTo
 * @return {number}
 */
String.prototype.fuzzyMatch = function(compareTo) {
    const term = compareTo.toLowerCase().split("")
    const str = this.toLowerCase().split("")
    let matchValue = 0
    for(let i = 0; i < term.length; i++) {
        const idx = str.map((x, j) => j)
                                .filter(j => str[j] === term[i])
        if(idx.length === 0)
            matchValue--
        else {
            idx.sort((a, b) => Math.abs(a-i)-Math.abs(b-i))
            const shortestDistance = Math.abs(idx[0]-i)
            matchValue += (1-shortestDistance/str.length)
        }
    }
    return matchValue/str.length
}

/**
 * Find an item within the set.
 * @param {function(any): boolean} cond
 * @return {any|null}
 */
Set.prototype.find = function(cond) {
    for(const obj of this)
        if(cond(obj))
            return obj
    return null
}