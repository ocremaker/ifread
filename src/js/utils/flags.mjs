/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Class decorator to make non defined static fields into flags.
 * @decorator
 * @param cls
 */
export function flags(cls) {
    let fields = Object.getOwnPropertyNames(cls).filter(f => {
        // Check undefined accessor
        try {
            cls[f]
            return false
        } catch(e) {
            return true
        }
    })
    for(let i = 0; i < fields.length; i++)
        Object.defineProperty(cls, fields[i], {
            get() {
                return 1 << i
            }
        })
}

/**
 * Checks whether the given flags contain a flag.
 * @param {number} flags
 * @param {number} flag
 */
export function containsFlag(flags, flag) {
    return (flags & flag) === flag
}