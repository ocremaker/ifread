/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const TEST_TEXT = "QWERTYUIOP"

let canvas = document.createElement("canvas")
let ctx = canvas.getContext("2d")
ctx.font = "normal 15px sans-serif"

const DEFAULT_WIDTH = ctx.measureText(TEST_TEXT).width

export const FontLoader = {
    CALLBACKS: {},
    LOADED: [],
    LOADING: [],

    /**
     * Measures the width a text produces with a font.
     * @param font
     * @param text
     * @returns {number}
     */
    measureWidth(font, text) {
        ctx.font = `normal 15px ${font}`
        return ctx.measureText(text).width
    },

    /**
     * Ensures a font is loaded and calls the callback if so.
     * @param {string} fontName
     * @param {function()} callback
     */
    ensureLoaded(fontName, callback) {
        if(this.LOADED.includes(fontName))
            callback()
        else if(this.LOADING.includes(fontName))
            this.CALLBACKS[fontName].push(callback)
        else {
            this.LOADING.push(fontName)
            this.CALLBACKS[fontName] = [callback]
        }
    },

    startLoadLoop() {
        setInterval(() => {
            for(let font of this.LOADING) {
                ctx.font = `normal 15px ${font}`
                if(DEFAULT_WIDTH !== ctx.measureText(TEST_TEXT).width) {
                    let cbs = this.CALLBACKS[font] ?? []
                    for(let cb of cbs)
                        cb()
                    delete this.CALLBACKS[font]
                    this.LOADED.push(font)
                    this.LOADING.splice(this.LOADING.indexOf(font), 1)
                }
            }
        }, 1000)
    }
}

FontLoader.startLoadLoop()
