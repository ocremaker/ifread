/*
 * Interactive Fictions for Restricted Environments All-in-one Designer
 * Copyright (C) 2024-2025  ocremaker <ocremaker@tutanota.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Utils to declare 'extension' class, to allow for multi inheritance


/**
 * 'Function' class to create class extensions.
 * @usage ```
 * const SubClass = new ExtensionClass(object, (C) =>
 *     /**
 *      * @class {SubClassDelegate}
 *      *\/
 *     class SubClassDelegate extends C {}
 * )
 * ```
 */
export class ExtensionClass extends Function {
    /**
     *
     * @param {typeof object} extendsType
     * @param {function(typeof object): typeof object} classDeclaration
     * @returns {function(typeof object): typeof object}
     */
    constructor(extendsType, classDeclaration) {
        super();
        this.extendsType = extendsType;
        this.classDeclaration = classDeclaration;
        this.cachedClassesMap = new Map()
        return new Proxy(this, {
            get: (target, prop) => target[prop],
            apply: (target, _, args) => target._defineClass(...args)
        })
    }

    /**
     * Checks whether the given object's class extends this extension.
     * @param obj
     * @returns {boolean}
     */
    isAncestorOf(obj) {
        let result = false;
        for(let delegate of this.cachedClassesMap.values())
            result |= obj instanceof delegate
        return result
    }

    /**
     * Creates the base class, returns the cached one if necessary.
     * The last part is necessary for
     * @param {typeof object} baseClass
     * @returns {T}
     * @private
     */
    _defineClass(baseClass) {
        if(baseClass == null || !(baseClass.prototype instanceof this.extendsType))
            throw new Error(`Class ${baseClass.name} is not extending '${this.extendsType.name}'.`);
        if(this.cachedClassesMap.has(baseClass))
            return this.cachedClassesMap.get(baseClass)
        else {
            let c = this.classDeclaration(baseClass)
            Object.defineProperty(c, "name", { value: `${c.name.split("<")[0]}<${baseClass.name}>`, writable: false })
            this.cachedClassesMap.set(baseClass, c)
            return c
        }
    }
}