Ifread
---
Interactive Fiction for Restricted Environments All-in-one Designer

Ifread is a webapplication that allows its users to structure, write and compile interactive fictions
for restricted environment targets like AO3 and the Skins function.

It features a nested visual logic tree builder, support for up to seven bits of memory to be used as booleans
and integers, as well as an integrated rich text writer which includes macros for interactive features.

# Status
**WIP**

- [x] Branding
- [ ] UI
  - [x] Create Blocks
  - [x] Fonts
  - [x] Drawer
  - [x] Main Diagram
  - [x] Popups
  - [x] Properties
  - [x] Inner Diagrams
  - [ ] Keyboard controls
    - Missing: Linking, block creating.
  - [x] Menu bar
  - [x] Zones
  - [x] Calc If Editor
  - [ ] Rich Text Editor
    - [x] Basic editor
    - [x] Styles
    - [x] Saving and restoring (Deltas)
    - [ ] Buttons
    - [ ] Music
    - [ ] Transform blocks
    - [ ] Ifs
    - [ ] Colors
    - [ ] Scratch pad
    - [ ] Macros
    - [ ] Images
    - [ ] Details
- [ ] Backend
  - [ ] Saving the diagram (including multi versions + multi chapters)
    - [ ] File format specing (including JSON-schema)
    - [ ] IO system
    - [ ] Saving
    - [ ] Loading
    - [ ] Auto save system
  - [ ] Loading the diagram
  - [ ] Exporters
    - [ ] AO3 Exporter
      - [ ] Per-chapter generator
      - [ ] CSS Skin generator (Only 1 if class for complex IF conditions, and dynamic generation for all the valid tests)
- [ ] Visual Logic Tree Builder
  - [x] Block design
  - [x] Layouts
  - [x] Context Menu
  - [x] Selecting
  - [ ] Help labels
  - [ ] Variables
    - [x] Zones
    - [ ] Implementation in blocks
    - [ ] Implementation in links
      - [x] Setting variables
      - [ ] Testing for variables
  - [x] Inner Diagram
  - [x] Sockets
  - [x] Controls
  - [ ] Blocks
    - [x] Detecting the zone within which the blocks are
    - [x] Start
    - [x] Note
    - [x] Merge
    - [x] Location
    - [ ] Ifs
      - [ ] Variable Condition
      - [ ] Output
      - [ ] And/Or
      - [ ] Not
    - [x] Choice
    - [x] Ghost World
      - [x] Core
        - [x] Trick
      - [ ] Talk
    - [ ] Debate
    - [ ] Investigation
    - [ ] Connect

# Licence

- Ifread is licenced under the AGPL 3.0. Please read LICENCE.md for more information.
- The icon (`assets/icons/ifread.svg`, `assets/static/ifread.png`) is licenced under Creative Commons 4.0 BY-NC-SA.
- Other icons in (`assets/icons`) are from [iconmonstr](https://iconmonstr.com/), and licenced under their [licence](https://iconmonstr.com/license/).
- The Allura font (`assets/fonts/allura`) is licenced under the [SIL OFL](http://scripts.sil.org/OFL).
- The Inter font (`assets/fonts/inter`) is licenced under the [SIL OFL](http://scripts.sil.org/OFL).
- The Recommended Words List (`assets/static/recommended_words.txt`) extracted from [topwords](https://github.com/ScriptSmith/topwords) is licenced under the [Creative Commons 4.0 BY-SA](https://creativecommons.org/licenses/by-sa/4.0/).
- The brush image (`assets/images/brush-stroke.png`) is licensed under Creative Commons 4.0 BY-NC (https://www.pngall.com/brush-stroke-png/download/46648)
