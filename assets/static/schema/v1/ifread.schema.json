{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://ocremaker.frama.io/ifread/schema/v1/ifread.schema.json",
  "title": "Ifread File Format",
  "description": "The file format used for ifread documents",
  "type": "object",
  "required": ["$schema", "title", "firstCreated", "lastModified", "lastExportFormatSettings", "chapters"],
  "properties": {
    "$schema": {
      "description": "References the schema this JSON document follows. Should point to this schema's $id.",
      "const": "https://ocremaker.frama.io/ifread/schema/v1/ifread.schema.json"
    },
    "title": {
      "description": "User-provided title of the document",
      "type": "string"
    },
    "firstCreated": {
      "description": "Unix timestamp of the creation date of the document (when a user first saved).",
      "type": "integer"
    },
    "lastModified": {
      "description": "Unix timestamp of the last modified date of the document (when a user saved last).",
      "type": "integer"
    },
    "lastExportFormatSettings": {
        "description": "List of settings used for last exports per format.",
        "type": "object",
        "properties": {
          "css": {
            "description": "List of settings used for last CSS export.",
            "type": "object",
            "required": ["selector"],
            "properties": {
              "selector": {
                "description": "The type of the selector used to affect other elements.",
                "default": "siblings",
                "enum": ["siblings", ":has"]
              }
            }
          }
        }
    },
    "chapters": {
      "title": "Chapter[]",
      "description": "The chapters contained in this file.",
      "type": "array",
      "minItems": 1,
      "uniqueItems": true,
      "items": {
        "title": "Chapter",
        "description": "A unique chapter that has multiple rollbackable versions (by possibly different authors).",
        "type": "object",
        "required": ["title", "uuid", "currentVersion", "versions"],
        "properties": {
          "title": {
            "description": "User-inputed title of the chapter.",
            "type": "string"
          },
          "uuid": {
            "description": "Unique identifier for this chapter (as they may be reorganized in different versions).",
            "$ref": "#/$defs/uuid"
          },
          "currentVersion": {
            "description": "UUID of the current chapter version.",
            "$ref": "#/$defs/uuid"
          },
          "versions": {
            "title": "ChapterVersion[]",
            "description": "The various stored versions of the chapter (in a tree)",
            "type": "array",
            "minItems": 1,
            "uniqueItems": true,
            "items": {
              "title": "ChapterVersion",
              "description": "A unique incremental version of a chapter.",
              "type": "object",
              "required": ["uuid", "author", "lastModified", "previousVersion", "texts", "zones"],
              "properties": {
                "uuid": {
                  "description": "UUID of this version",
                  "$ref": "#/$defs/uuid"
                },
                "childBuilders": {
                  "title": "ChildBuildersDiff",
                  "description": "Object that contains the changes of builder UUIDs dependant on this builder one between the previous version and this one.",
                  "type": "object",
                  "properties": {
                    "added": {
                      "title": "AddedChildBuilders",
                      "description": "UUID of the child builder.",
                      "type": "array",
                      "items": {
                        "$ref": "#/$defs/uuid"
                      }
                    },
                    "removed": {
                      "title": "RemovedChildBuilders",
                      "description": "The squares that were removed in this version.",
                      "type": "array",
                      "items": {
                        "$ref": "#/$defs/uuid"
                      }
                    }
                  }
                },
                "author": {
                  "description": "Name of the author who wrote this version",
                  "type": "string"
                },
                "lastModified": {
                  "description": "Unix timestamp of the last modified date of this chapter version.",
                  "type": "integer"
                },
                "previousVersion": {
                  "description": "UUID of the version this one is based of. Null if canonical version.",
                  "oneOf": [
                    { "$ref": "#/$defs/uuid" },
                    { "type": "null" }
                  ]
                },
                "texts": {
                  "title": "QuillDeltaWithID[]",
                  "description": "List of INCREMENTAL Quill Delta texts.",
                  "allOf": [{ "$ref": "#/$defs/VersionDiff" }],
                  "properties": {
                    "added": {
                      "items": { "$ref": "#/$defs/QuillDeltaWithID" }
                    },
                    "modified": {
                      "items": { "$ref": "#/$defs/QuillDeltaWithID" }
                    }
                  }
                },
                "zones": {
                  "description": "The list of Zones for this chapter.",
                  "allOf": [{ "$ref": "#/$defs/VersionDiff" }],
                  "properties": {
                    "added": {
                      "items": { "$ref": "#/$defs/Zone" }
                    },
                    "modified": {
                      "items": { "$ref": "#/$defs/Zone[Diff]" }
                    }
                  }
                },
                "builders": {
                  "title": "PartialAbstractLogicBuilder[]",
                  "description": "List of all logic builders (including 'main' and inner diagrams).",
                  "type": "array",
                  "minItems": 1,
                  "uniqueItems": true,
                  "items": {
                    "title": "LogicBuilder",
                    "description": "An instance of a logic builder.",
                    "type": "object",
                    "required": [
                      "uuid",
                      "zonesDelegates",
                      "blocks",
                      "links"
                    ],
                    "properties": {
                      "uuid": {
                        "description": "Unique identifier of the builder. Main builder is 'main'.",
                        "type": "string"
                      },
                      "zonesDelegates": {
                        "title": "ZoneDelegate[]",
                        "description": "List of each zone delegate used in this logic builder.",
                        "type": "array",
                        "minItems": 0,
                        "uniqueItems": true,
                        "items": {
                          "title": "ZoneDelegate",
                          "description": "An instance of a zone delegate in this logic builder.",
                          "type": "object",
                          "required": ["uuid", "squares"],
                          "properties": {
                            "uuid": {
                              "description": "Unique of the zone (matched in the 'zones' of the ChapterVersion)",
                              "$ref": "#/$defs/uuid"
                            },
                            "squares": {
                              "title": "ZoneSquaresDiff",
                              "description": "Object that contains the changes between the Zone Squares of the previous version and this one.",
                              "type": "object",
                              "properties": {
                                "added": {
                                  "title": "AddedSquares",
                                  "description": "The squares that were added in this version.",
                                  "type": "array",
                                  "items": {
                                    "$ref": "#/$defs/ZoneSquare"
                                  }
                                },
                                "removed": {
                                  "title": "RemovedSquares",
                                  "description": "The squares that were removed in this version.",
                                  "type": "array",
                                  "items": {
                                    "$ref": "#/$defs/ZoneSquare"
                                  }
                                }
                              }
                            }
                          }
                        }
                      },
                      "blocks": {
                        "title": "Block[]",
                        "description": "List of all blocks in this LogicBuilder, including StartBlock.",
                        "allOf": [{ "$ref": "#/$defs/VersionDiff" }],
                        "properties": {
                          "added": {
                            "items": { "$comment": "TODO: Document the blocks" }
                          },
                          "modified": {
                            "items": { "$comment": "TODO: Document the blocks" }
                          }
                        }
                      },
                      "links": {
                        "title": "Links",
                        "description": "List of links linking block sockets together.",
                        "allOf": [{ "$ref": "#/$defs/VersionDiff" }],
                        "properties": {
                          "added": {
                            "items": { "$ref": "#/$defs/Link" }
                          },
                          "modified": {
                            "items": { "$ref": "#/$defs/Link[Diff]" }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "$defs": {
    "uuid": {
      "type": "string",
      "pattern": "^[a-z0-9]{19}$"
    },
    "VersionDiff": {
      "title": "VersionDiff",
      "description": "Object that contains the changes between the previous version and this one.",
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "added": {
          "title": "Added",
          "description": "The elements that were added in this version.",
          "uniqueItems": true,
          "type": "array"
        },
        "modified": {
          "title": "Modified",
          "description": "The elements that were modified in this version.",
          "uniqueItems": true,
          "type": "array"
        },
        "removed": {
          "title": "Added",
          "description": "The elements that were removed in this version.",
          "type": "array",
          "uniqueItems": true,
          "items": {
            "$ref": "#/$defs/uuid"
          }
        }
      }
    },
    "QuillDeltaWithID": {
      "description": "A unique INCREMENTAL Quill Delta texts with a uuid. To be combined with previous versions of each text when they exist.",
      "type": "object",
      "required": ["uuid", "contents"],
      "properties": {
        "uuid": {
          "description": "Unique identifier for this Delta.",
          "$ref": "#/$defs/uuid"
        },
        "contents": {
          "$ref": "#/$defs/QuillDeltaOperation"
        }
      }
    },
    "QuillDeltaOperation": {
      "description": "The incremental Quill Delta data operations",
      "type": "array",
      "items": {
        "description": "A singular Quill delta operation",
        "type": "object",
        "oneOf": [
          {"required": ["insert"]},
          {"required": ["delete"]},
          {"required": ["retain"]}
        ],
        "properties": {
          "attributes": {
            "description": "Attributes to be applied to the content parsed by the operation",
            "type": "object"
          },
          "insert": {
            "description": "Inserts something at the current index.",
            "oneOf": [
              {"type": "string"},
              {"type": "object"}
            ]
          },
          "delete": {
            "description": "Deletes a number of chars at the current index.",
            "type": "integer"
          },
          "retain": {
            "description": "Skips a number of chars at the current index.",
            "type": "integer"
          }
        }
      }
    },
    "CalcIfExpression": {
      "title": "CalcIfToken[]",
      "type": "array",
      "uniqueItems": false,
      "minItems": 0,
      "items": {
        "title": "CalcIfToken",
        "type": "object",
        "required": ["type", "value"],
        "properties": {
          "type": {
            "type": "string"
          },
          "value": {
            "type": "string"
          }
        }
      }
    },
    "Socket": {
      "title": "Block Socket",
      "type": "object",
      "required": ["ref", "block"],
      "properties": {
        "ref": {
          "description": "Unique identifiable name of the socket within the block.",
          "type": "string"
        },
        "block": {
          "description": "UUID of the block this socket is in.",
          "$ref": "#/$defs/uuid"
        }
      }
    },
    "ZoneSquare": {
      "title": "Zone Square",
      "type": "object",
      "required": ["x", "y"],
      "properties": {
        "x": {
          "type": "integer",
          "description": "X position of the square of the ZoneDelegate."
        },
        "y": {
          "type": "integer",
          "description": "Y position of the square of the ZoneDelegate."
        }
      }
    },
    "Link[Diff]": {
      "title": "Link[Diff]",
      "description": "Partial (diffed) instance of a Link.",
      "type": "object",
      "required": ["uuid"],
      "properties": {
        "uuid": {
          "description": "Unique identifier of the Link.",
          "$ref": "#/$defs/uuid"
        },
        "title": {
          "description": "User provided title for this link",
          "examples": ["Link"],
          "type": "string"
        },
        "linkType": {
          "description": "Type of link this one represents",
          "enum": ["text", "link", "branch", "condition"]
        },
        "allowBidirectional": {
          "description": "If true, makes this link bidirectional.",
          "type": "boolean"
        },
        "from": {
          "description": "The socket from which this link starts",
          "$ref": "#/$defs/Socket"
        },
        "to": {
          "description": "The socket at which this link ends",
          "$ref": "#/$defs/Socket"
        },
        "condition": {
          "title": "VariableCondition",
          "description": "Condition required to be fulfilled to display this link/the data after this link.",
          "anyOf": [
            { "type": "null" },
            {
              "type": "object",
              "required": ["expressionHasErrors", "calcIfExpression"],
              "properties": {
                "expressionHasErrors": {
                  "type": "boolean",
                  "description": "Stores whether the expression had errors when it was last set."
                },
                "calcIfExpression": {
                  "$ref": "#/$defs/CalcIfExpression"
                }
              }
            }
          ]
        },
        "assignments": {
          "title": "VariableAssignment[]",
          "description": "List of assignments that will be executed when this link is passed.",
          "type": "array",
          "minItems": 0,
          "uniqueItems": true,
          "items": {
            "title": "VariableAssignment",
            "type": "object",
            "required": ["uuid", "targetUUID", "expressionHasErrors", "behaviorOnOverflow", "calcIfExpression"],
            "properties": {
              "uuid": {
                "$ref": "#/$defs/uuid",
                "description": "UUID of the assignment."
              },
              "targetUUID": {
                "$ref": "#/$defs/uuid",
                "description": "UUID of the target variable definition to be assigned."
              },
              "expressionHasErrors": {
                "type": "boolean",
                "description": "Stores whether the expression had errors when it was last set."
              },
              "behaviorOnOverflow": {
                "enum": ["clamp", "modulo"],
                "description": "What algorithm to use when the value of the integer overflows."
              },
              "calcIfExpression": {
                "$ref": "#/$defs/CalcIfExpression"
              }
            }
          }
        }
      }
    },
    "Link": {
      "title": "Link[Complete]",
      "description": "An instance of a Link linking block sockets together.",
      "allOf": [{ "$ref":  "#/$defs/Link[Diff]" }],
      "required": ["uuid", "title", "linkType", "allowBidirectional", "from", "to", "assignments"]
    },
    "Zone[Diff]": {
      "title": "Zone[Diff]",
      "description": "Partial (diffed) instance of a Zone.",
      "type": "object",
      "required": ["uuid"],
      "properties": {
        "uuid": {
          "description": "Unique identifier for this Zone.",
          "$ref": "#/$defs/uuid"
        },
        "parentZone": {
          "description": "Unique identifier for the parent of this zone.",
          "oneOf": [
            { "$ref": "#/$defs/uuid" },
            { "type": "null" }
          ]
        },
        "title": {
          "description": "User-provided title for this zone.",
          "examples": ["Untitled Zone"],
          "type": "string"
        },
        "color": {
          "description": "User-provided background color for this zone.",
          "type": "string",
          "pattern": "^#[a-fA-F0-9]{6}$"
        },
        "variables": {
          "description": "A list of variable definitions.",
          "type": "array",
          "minItems": 0,
          "uniqueItems": true,
          "items": {
            "description": "A singular variable definition.",
            "type": "object",
            "required": ["name", "varType", "description", "maximum", "uuid"],
            "properties": {
              "uuid": {
                "description": "Unique identifier for this variable definition.",
                "$ref": "#/$defs/uuid"
              },
              "name": {
                "description": "User-provided name of the variable (no restrictions on characters).",
                "type": "string"
              },
              "varType": {
                "description": "The type of the variable being defined.",
                "enum": ["bool", "int"]
              },
              "description": {
                "description": "User-provided multi-line description of the variable.",
                "type": "string"
              },
              "maximum": {
                "description": "Maximum value that the integer variable can have.",
                "type": "integer",
                "minimum": 1
              }
            }
          }
        }
      }
    },
    "Zone": {
      "title": "Zone[Complete]",
      "description": "A unique Zone in the background that define variables and their scopes for this chapter.",
      "allOf": [{ "$ref":  "#/$defs/Zone[Diff]" }],
      "required": ["uuid", "title", "color", "variables"]
    }
  }
}
