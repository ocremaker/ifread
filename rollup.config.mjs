import { nodeResolve } from "@rollup/plugin-node-resolve"
import litCss from "rollup-plugin-lit-css"
import { babel } from "@rollup/plugin-babel"
import commonjs from "@rollup/plugin-commonjs"
import alias from "@rollup/plugin-alias";
import terser from "@rollup/plugin-terser"
import { compile } from "sass"

import { dirname, resolve } from 'path';
import { fileURLToPath } from 'url';

const projectRootDir = resolve(dirname(fileURLToPath(import.meta.url)))

export default {
    input: "src/js/main",
    output: {
        dir: "public/jstmp",
        sourcemap: true,
        compact: true,
        format: "umd"
    },
    plugins: [
        commonjs({ sourceMap: false }),
        alias({
            entries: [
                { find: "#abstract", replacement: `${projectRootDir}/src/js/abstract/` },
                { find: "#io", replacement: `${projectRootDir}/src/js/io/` },
                { find: "#logic-builder", replacement: `${projectRootDir}/src/js/logic-builder/` },
                { find: "#utils", replacement: `${projectRootDir}/src/js/utils/` },
                { find: "#ui", replacement: `${projectRootDir}/src/js/ui/` },
                { find: "#css", replacement: `${projectRootDir}/src/css` },
            ],
        }),
        nodeResolve({ browser: true }),
        litCss({
            include: "src/css/shadows/**/*.scss",
            transform: (data, { filePath }) => {
                try {
                    let css = compile(filePath, { style: "compressed" }).css
                    css = css.replaceAll("../assets/", `./assets/`)
                    return css
                } catch(e) {
                    console.log(filePath)
                    console.error(e)
                    throw e
                }
            }
        }),
        litCss({
            include: "node_modules/quill/dist/quill.*.css",
        }),
        babel({
            babelHelpers: "bundled"
        })
        // terser()
    ]
}